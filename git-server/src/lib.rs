use std::path::{Path, PathBuf};
use std::sync::Arc;

use axum::body::Body;
use axum::http::Request;
use axum::middleware::from_fn;
use axum::routing::{self, MethodFilter};
use axum::{routing::get, Extension, Router};
use tokio::sync::Mutex;
use tower_http::trace::{DefaultOnResponse, TraceLayer};

mod handler;
mod middleware;
pub use handler::Error;
use tracing::{Level, Span};

pub type AuthFunc = Arc<Mutex<Box<dyn Fn(&str, &str) -> bool + Send + Sync>>>;

#[derive(Default, Clone)]
struct Configuration {
    pub root: PathBuf,
    pub address: String,
    pub enable_push: bool,
    pub auth_required: bool,
    pub auth_fn: Option<AuthFunc>,
}

/// Lightweight server that wraps git-http-backend to provide access to
/// git repositories over HTTP. The primary use case of this package is for
/// use in Lorry integration tests. It may, however, be generally useful.
#[derive(Default)]
pub struct Server {
    config: Configuration,
}

impl Server {
    pub fn with_address(mut self, addr: &str) -> Self {
        self.config.address = addr.to_string();
        self
    }

    pub fn with_workdir(mut self, workdir: &Path) -> Self {
        self.config.root = workdir.to_path_buf();
        self
    }

    pub fn with_auth_required(mut self, required: bool) -> Self {
        self.config.auth_required = required;
        self
    }

    pub fn with_auth<F>(mut self, func: F) -> Self
    where
        F: Fn(&str, &str) -> bool + Send + Sync + 'static,
    {
        self.config.auth_fn = Some(Arc::new(Mutex::new(Box::new(func))));
        self
    }

    pub fn enable_push(mut self, enabled: bool) -> Self {
        self.config.enable_push = enabled;
        self
    }

    /// return the configured address
    pub fn address(&self) -> String {
        self.config.address.clone()
    }

    pub async fn serve(&self) -> Result<(), std::io::Error> {
        tracing::info!("Serving repositories from: {:?}", self.config.root);
        let app = Router::new()
            // git smart http clone
            // /(HEAD|info/refs|objects/info/.*|git-upload-pack).*$
            .route("/:name/HEAD", get(handler::handler))
            .route("/:name/info/refs", get(handler::handler))
            .route("/:name/objects/info/:id", get(handler::handler))
            .route(
                "/:name/git-upload-pack",
                routing::on(MethodFilter::GET.or(MethodFilter::POST), handler::handler),
            )
            // Allows push access which needs to be authenticated
            .route(
                "/:name/git-receive-pack",
                routing::on(MethodFilter::GET.or(MethodFilter::POST), handler::handler),
            )
            .layer(
                TraceLayer::new_for_http()
                    .on_request(|request: &Request<Body>, _span: &Span| {
                        tracing::info!("started {} {}", request.method(), request.uri().path())
                    })
                    .on_response(DefaultOnResponse::new().level(Level::INFO)),
            )
            .layer(from_fn(middleware::authorization))
            .layer(Extension(Arc::new(self.config.clone())));
        // run it
        let listener = tokio::net::TcpListener::bind(&self.config.address)
            .await
            .unwrap();
        println!("listening on {}", listener.local_addr().unwrap());
        axum::serve(listener, app).await
    }
}

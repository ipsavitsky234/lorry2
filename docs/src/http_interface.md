# HTTP Interface

Lorry has a web interface as well as a REST API for configuration and management.

Once Lorry has been launched by default it will be available at 
[http://localhost:3000](http://localhost:3000).

## Web Interface 

![Lorry Logo](images/screenshot.png)

The main page will list mirror status of each job and there is also a debug
page which can display which active jobs are running on which thread. The 
config page will show each currently configured Lorry.

## Endpoints

The following endpoints are available for users to interact with the controller.

### Health Check

`GET /1.0/health-check`

### List Jobs

`GET /1.0/list-jobs`

Lists of all the jobs that have been given to workers.

Example output:

```json
[
  {
    "id": 1,
    "path": "lorry-mirrors/github/octocat/hello-world",
    "exit_status": {
      "Finished": {
        "exit_code": 0,
        "disk_usage": 311296
      }
    },
    "host": "worker-0"
  }
]
```

### List Lorries

`GET /1.0/list-lorries`

Lists all the lorries configured on the controller.

Example Output:

```json
[
  {
    "path": "lorry-mirrors/github/octocat/hello-world",
    "name": "lorry-mirrors/github/octocat/hello-world",
    "spec": {
      "type": "git",
      "url": "https://github.com/octocat/Hello-World.git",
      "check-certificates": true,
      "refspecs": null
    },
    "running_job": null,
    "last_run": 1696523383,
    "interval": "PT60S",
    "lorry_timeout": "PT60S",
    "last_run_results": {
      "Finished": {
        "exit_code": 0,
        "disk_usage": 311296
      }
    },
    "last_run_output": "...",
    "purge_before": 1696512967
  }
]
```

### Metrics

`GET /1.0/metrics`

Returns information of jobs and lorries exported as metrics for 
[Prometheus](https://prometheus.io):

Example Output:

```text
# HELP lorry2_total_lorries The total amount of lorries.
# TYPE lorry2_total_lorries gauge
lorry2_total_lorries 2
# HELP lorry2_total_lorries_degraded The total amount of lorries partially failed.
# TYPE lorry2_total_lorries_degraded gauge
lorry2_total_lorries_degraded 0
# HELP lorry2_total_lorries_errors The total amount of lorries in a failed state.
# TYPE lorry2_total_lorries_errors gauge
lorry2_total_lorries_errors 0
# HELP lorry2_total_lorries_successful The total amount of successful mirrors.
# TYPE lorry2_total_lorries_successful gauge
lorry2_total_lorries_successful 2
# HELP lorry2_total_lorries_degraded_namespaced The total amount of lorries partially failed.
# TYPE lorry2_total_lorries_degraded_namespaced gauge
# HELP lorry2_total_lorries_errors_namespaced The total amount of lorries in a failed state.
# TYPE lorry2_total_lorries_errors_namespaced gauge
# HELP lorry2_total_lorries_successful_namespaced The total amount of successful mirrors.
# TYPE lorry2_total_lorries_successful_namespaced gauge
lorry2_total_lorries_successful_namespaced{namespace="lorry-mirrors/lorry"} 1
lorry2_total_lorries_successful_namespaced{namespace="raw-assets/lorry-assets"} 1
# EOF
```

#### Alerting & Metric Namespacing

Lorry configures mirrors with a concatenated path name such as `sources/nvidia`
or `sources/github/google`. Lorry can expose the status of mirrors such as 
number of errors or warnings based on these names. With this approach it is 
possible to use Prometheus's 
[alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/) to
send notifications to groups of users who are responsible for maintaining
a particular set of mirrors from an organization.

It's possible to configure the "depth", i.e. number of sub directories that
are exposed by Lorry. The default number of mirror namespaces that will be 
exposed is `4`.

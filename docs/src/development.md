# Development

## Developing Lorry

The Lorry repository contains everything you need to start hacking on the
codebase right away. First ensure that you've followed the instructions for
[installing Lorry locally](/installation.html#from-source).

Although not strictly required installing [cargo-watch](https://watchexec.github.io/#cargo-watch) is suggested since it makes the development workflow easier. Install
it either by your package manager or via `cargo install watch`. Additionally
you should ensure that you have [podman](podman.io) installed which is the only
supported container platform for running Lorry.

This example uses Gitlab which is the primary [software forge](https://en.wikipedia.org/wiki/Forge_(software)) supported by Lorry.

```sh
# In a seperate terminal pane you can launch Gitlab. This will take a few minutes.
scripts/run_gitlab.sh
# Request an authentication token from Gitlab. Note that you need to wait about
# five minutes before running this since Gitlab will not immediately be ready.
source scripts/request_gitlab_token.sh
# Launch the watch script which will restart Lorry on code changes.
scripts/watch.sh
```

## Writing Documentation

Documentation is managaed with [mdbook](https://rust-lang.github.io/mdBook/).

Ensure you have that installed and then run `scripts/docs.sh` to launch the
server. Documentation content is located under `docs/content/`.

## API Documentation

Lorry is not yet available on crates.io however its bleeding edge API 
documentation can be found [here](/api/workerlib/index.html).

## Development Containers

Lorry uses several containers during it's CI process. To update the version
of Rust for example that is used to compile Lorry you should modify the
appropriate variable in `scripts/setup_build_containers.sh` and then execute
that script (assuming you have appropriate permissions to access Lorry's
Gitlab container registry).

## Changing the version of Rust in CI / Containers

To change the version of Rust bump the version number defined in 
`scripts/setup_build_containers.sh` and then run the script to mirror the new
version. Additionally set the version defined in `.gitlab-ci.yml` to match the
container / tag you mirrored in the previous step.

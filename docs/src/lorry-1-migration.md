# Migration from Lorry 1

The biggest difference between Lorry 1 and Lorry 2 are the configuration files
as well as the confgit repo. Thankfully as the controller configuration for 
Lorry 2 includes a branch, this can be used to make migration easier.

## Confgit Migration

The first step will be to migrate the confgit configuration. For this, if your individual lorries are in yaml format,
then nothing is going to need to be changed. The biggest change is to the root `lorry-configuration.conf` where several
options have changed. You may also need to change/drop certain mirror files themselves.

1. Convert any JSON based lorry configs to YAML.
2. Convert the `lorry-configuration.conf` file:

* `type` stays the same (the value `lorries`)
* `interval` convert to ISO8601 Duration format
* `lorry-timeout` convert to ISO8601 Duration format and rename key `timeout`. This is now a required key.
* `prefix` stays the same
* `globs` stays the same
* any other keys should be discarded (but will be ignored if left)

3. Convert mirror files to be lorry2 compatible by running:

  * The `git` type is supported as the same usage
  * The `raw-file` type is supported as the same usage
  * The `tarball` type is no longer supported, so repos with `type: tarball` need to be dropped.
  * Mercurial upstream is no longer supported so you may need to drop mirrors with
    `type: hg` or find a `git` host of them.
  * Bazaar upstream is no longer supported so you may need to drop mirrors with
    `type: bzr` or find a `git` host of them.
  * Subversion upstream is no longer supported so you may need to drop mirrors with
    `type: svn` or find a `git` host of them.
  * CVS upstream is no longer supported so you may need to drop mirrors with
    `type: cvs` or find a `git` host of them.
  * ZIP upstream is no longer supported so you may need to drop mirrors with `type: zip`
  * GZIP upstream is no longer supported so you may need to drop mirrors with `type: gzip`

For `type: tarball`, `type: zip` and `type: gzip`, it might be possible
to use `type: raw-file` instead for these purpose. But bear in mind that
`type: tarball`, `type: zip` and `type: gzip` used to first expand the
compressed file and then commit the extracted content while `raw-file` just
simply pushes the compressed file.

To automate step 3 we internally use a script [`convert_to_lorry2.py`](./scripts/convert_to_lorry2.py).
This may be useful for your own conversion. Its usage is:

    python convert_to_lorry2.py mirror_config_repo

`mirror_config_repo` should be the root directory of the mirroring-config repo. This script will
automatically drop all repos with the unsupported types as listed above.

This can all be done in a separate branch to your main configuration, allowing for quick rollback in case of an issue.

# Installation

## System Requirements

Lorry is designed to run on a Linux distribution and should have at least
two cores, 512mb of memory or more and adequate disk space to hold the 
contents of each configured mirror and raw file remote.

### Runtime Dependencies

Note that only the Git binary need to be installed on your system, the other
components are embedded in Lorry's crate dependencies.

| Dependency                         | Description
|------------------------------------|-----------------------------------------|
| [git](https://git-scm.com/)        | Used for non libgit2 cloning operations |
| [libgit2](https://libgit2.org/)    | Used for various repository management  |
| [sqlite3](https://www.sqlite.org/) | Stateful repository metadata            |

## From Helm

A Helm chart for running Lorry on Kubernetes is available [here](https://gitlab.com/CodethinkLabs/lorry/lorry2/-/tree/main/helm/lorry2)

## From your Package Manager

### Arch Linux

A distribution package of Lorry is available on the [AUR](https://aur.archlinux.org/packages/lorry).

### Other Operating Systems

If you're Linux distribution doesn't have a system package please consider
creating one, merge requests are most welcome in this regard. Please open up
an issue to track your distributions progress 
[here](https://gitlab.com/CodethinkLabs/lorry/lorry2/-/issues/).

## From Source

To install Lorry from source you simply need to clone its Git repository
and compile it with the standard Rust tool chain.

### Build Dependencies

Ensure you have `sqlx` available on your PATH, typically it can be installed
either by your system's package manager or by running `cargo install sqlx-cli`.

| Dependency                                     | Description
|------------------------------------------------|--------------------------------------------------|
| [sqlx](https://github.com/launchbadge/sqlx/)   | Used for compile time verficiation of sql queries|

```sh
git clone https://gitlab.com/CodethinkLabs/lorry/lorry2.git
cd lorry2
# Initialize Lorry's database in the repository
scripts/init_db_if_missing.sh
# Compile the Lorry binary
cargo build --release
# Test the compiled binary 
target/release/lorry --help
```

From this point you can modify the lorry.example.toml file to your liking
pointing it at your gitlab instance.



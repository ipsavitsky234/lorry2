//! /// Helper command builder for pushing mirrors downstream
//!

use crate::execute::CommandBuilder;
use crate::PushRefs;
use fancy_regex::Regex;
use std::{path::Path, sync::OnceLock};
use tokio::process::Command;

/// Push the contents of a mirror directory into the downstream
pub struct Push<'a> {
    pub url: &'a url::Url,

    // Git ref that should be pushed
    pub ref_names: Vec<&'a str>,

    // Path to the global git configuration file
    pub config_path: &'a Path,
}

impl Push<'_> {
    /// Parse the output of `stdout` from the `git push` command to determine
    /// which branches succeeded and which ones failed
    ///
    /// Example output:
    /// ```txt
    /// To http://127.0.0.1:9999/root/repo-a.git
    /// =       refs/heads/a:refs/heads/a       [up to date]
    /// =       refs/heads/asdf:refs/heads/asdf [up to date]
    /// !       refs/heads/main:refs/heads/main [rejected] (non-fast-forward)
    /// Done
    /// ```
    ///
    /// `git push --porcelain <flag>` documentation (see `man git-push`)
    ///
    /// ```txt
    /// A single character indicating the status of the ref:
    ///       (space)
    ///           for a successfully pushed fast-forward;
    ///       +
    ///           for a successful forced update;
    ///       -
    ///           for a successfully deleted ref;
    ///       *
    ///           for a successfully pushed new ref;
    ///       !
    ///           for a ref that was rejected or failed to push; and
    ///       =
    ///           for a ref that was up to date and did not need pushing.
    /// ```
    pub fn parse_output(output: &str) -> Result<PushRefs, String> {
        tracing::debug!("Parsing push output: {}", output);
        output
            .split('\n')
            .try_fold(PushRefs(vec![], vec![]), |mut accm, line| {
                if line.starts_with("To") {
                    return Ok(accm);
                }
                if line.starts_with("Done") {
                    return Ok(accm);
                }
                if line.is_empty() {
                    return Ok(accm);
                }
                let refname = match get_match_regex().captures(line).ok() {
                    Some(Some(group)) => group.get(1).map(|matched| matched.as_str().to_string()),
                    Some(None) => None,
                    None => None,
                };

                if let Some(refname) =
                    refname.and_then(|refname| refname.split(':').next().map(|s| s.to_string()))
                {
                    if line.starts_with('*') || line.starts_with('=') {
                        // success
                        accm.0.push((refname.clone(), line.to_string()))
                    } else if line.starts_with('!') {
                        // failed
                        accm.1.push((refname.clone(), line.to_string()))
                    } else if line.starts_with(' ') {
                        // fast-forward
                        accm.0.push((refname.clone(), line.to_string()))
                    } else {
                        return Err(format!("Unknown line format: {}", line));
                    };
                    Ok(accm)
                } else {
                    Err(format!("Bad line: {}", line))
                }
            })
    }
}

impl CommandBuilder for Push<'_> {
    fn build(&self, current_dir: &Path) -> Command {
        let mut args = vec!["--bare", "push", "--porcelain", self.url.as_str()];
        args.extend(self.ref_names.clone());
        tracing::debug!("Invoking git command with args: {:?}", args);
        let mut cmd = Command::new("git");
        cmd.current_dir(current_dir);
        cmd.envs([(
            crate::git_config::GIT_CONFIG_GLOBAL,
            self.config_path.to_string_lossy().as_ref(),
        )]);
        cmd.args(args);
        cmd
    }
}

fn get_match_regex() -> &'static Regex {
    static EXPRESSIONS: OnceLock<Regex> = OnceLock::new();
    EXPRESSIONS.get_or_init(|| Regex::new(r##"(?<=\s)(\S+)"##).unwrap())
}

#[cfg(test)]
mod test {
    use super::*;

    const STDOUT_TEST_CASE_1: &str = r#"
To http://127.0.0.1:9999/root/repo-a.git
=       refs/heads/a:refs/heads/a       [up to date]
=       refs/heads/asdf:refs/heads/asdf [up to date]
!       refs/heads/main:refs/heads/main [rejected] (non-fast-forward)
Done
"#;

    const STDOUT_TEST_CASE_2: &str = r#"
To http://127.0.0.1:9999/root/repo-a.git
*       refs/heads/a:refs/heads/a       [new branch]
*       refs/heads/asdf:refs/heads/asdf [new branch]
!       refs/heads/main:refs/heads/main [rejected] (non-fast-forward)
Done
"#;

    const STDOUT_TEST_CASE_3: &str = r#"
To http://127.0.0.1:9999/root/repo-a.git
*       refs/heads/asdf:refs/heads/asdf [new branch]
haha yeah no
!       refs/heads/main:refs/heads/main [rejected] (non-fast-forward)
Done
"#;

    const STDOUT_TEST_CASE_4: &str = r#"
To http://127.0.0.1:20000/repo-a
        refs/heads/asdf:refs/heads/asdf 7f96764..953422f
Done
"#;

    #[test]
    fn test_push_stdout_parsing_case_1() {
        let results = Push::parse_output(STDOUT_TEST_CASE_1).unwrap();
        assert!(results.0.len() == 2);
        assert!(results.1.len() == 1);
        assert!(results.0.first().unwrap().0 == "refs/heads/a");
        assert!(results.0.get(1).unwrap().0 == "refs/heads/asdf");
        assert!(results.1.first().unwrap().0 == "refs/heads/main");
    }

    #[test]
    fn test_push_stdout_parsing_case_2() {
        let results = Push::parse_output(STDOUT_TEST_CASE_2).unwrap();
        assert!(results.0.len() == 2);
        assert!(results.1.len() == 1);
        assert!(results.0.first().unwrap().0 == "refs/heads/a");
        assert!(results.0.get(1).unwrap().0 == "refs/heads/asdf");
        assert!(results.1.first().unwrap().0 == "refs/heads/main");
    }

    #[test]
    fn test_push_stdout_parsing_case_3() {
        let results = Push::parse_output(STDOUT_TEST_CASE_3);
        assert!(results.is_err_and(|err| err == "Unknown line format: haha yeah no"))
    }

    #[test]
    fn test_push_stdout_parsing_case_4() {
        let results = Push::parse_output(STDOUT_TEST_CASE_4).unwrap();
        assert!(results.0.len() == 1);
        assert!(results.1.is_empty());
        assert!(results.0.first().unwrap().0 == "refs/heads/asdf");
    }
}

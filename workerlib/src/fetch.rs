//! Fetch git sources into a workspace
//!

use crate::execute::{execute, Command, CommandBuilder, Error as ExecutionError};
use git2::{Error as GitError, FetchOptions, FetchPrune, Repository};
use std::path::Path;
use utils::redact::redact;

/// Fetch all refs from the an upstream source into a file system directory
struct FetchCommand<'a> {
    pub url: &'a url::Url,

    // Path to the global git configuration file
    pub config_path: &'a Path,
}

impl CommandBuilder for FetchCommand<'_> {
    fn build(&self, current_dir: &Path) -> Command {
        let mut cmd = Command::new("git");
        cmd.current_dir(current_dir);
        cmd.envs([(
            crate::git_config::GIT_CONFIG_GLOBAL,
            self.config_path.to_string_lossy().as_ref(),
        )]);
        cmd.args([
            "--bare",
            "fetch",
            "--prune",
            self.url.as_ref(),
            "+refs/heads/*:refs/heads/*",
            "+refs/tags/*:refs/tags/*",
        ]);
        cmd
    }
}

/// An error occurred while fetching from the upstream repository
#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Error fetching with the Git binary: {0}")]
    Command(#[from] ExecutionError),

    #[error("Error fetching with Libgit2: {0}")]
    Git(#[from] GitError),
}

/// Mirrors a git repo by pulling down the upstream. Depending on the
/// configuration it will use libgit2 or the git binary
pub struct Fetch<'a> {
    pub git_repo: &'a crate::workspace::Workspace,

    // URL of the server being fetched from, must be a git compliant URL
    pub target_url: &'a url::Url,

    pub use_git_binary: Option<bool>,
    pub git_config_path: &'a Path,
}

impl Fetch<'_> {
    async fn fetch_with_git(&self) -> Result<(), Error> {
        execute(
            &FetchCommand {
                url: self.target_url,
                config_path: self.git_config_path,
            },
            &self.git_repo.repository_path(),
        )
        .await?;
        Ok(())
    }

    async fn fetch_with_libgit2(&self) -> Result<(), Error> {
        let git_repo_thread = self
            .git_repo
            .repository_path()
            .to_string_lossy()
            .to_string();
        let upstream_url_thread = self.target_url.to_string();
        let handle = tokio::task::spawn_blocking(move || {
            let repository = Repository::open(git_repo_thread).unwrap();
            let mut remote = repository.remote_anonymous(&upstream_url_thread).unwrap();
            remote.fetch(
                &["+refs/heads/*:refs/heads/*", "+refs/tags/*:refs/tags/*"],
                Some(
                    FetchOptions::new()
                        .prune(FetchPrune::On)
                        .custom_headers(&[crate::LORRY_VERSION_HEADER]),
                ),
                None,
            )
        });
        handle.await.unwrap()?;
        Ok(())
    }

    pub async fn fetch(&self) -> Result<(), Error> {
        if self
            .use_git_binary
            .is_some_and(|use_git_binary| use_git_binary)
        {
            tracing::info!(
                "fetching repository {} with the git binary into {}",
                redact(&self.target_url),
                self.git_repo.repository_path().to_string_lossy(),
            );
            self.fetch_with_git().await
        } else {
            tracing::info!(
                "fetching repository {} with libgit2 into {}",
                redact(&self.target_url),
                self.git_repo.repository_path().to_string_lossy(),
            );
            self.fetch_with_libgit2().await
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::git_config::Config;
    use crate::test_server::{spawn_test_server, TestRepo, EXAMPLE_COMMIT};
    use crate::workspace::Workspace;
    use tempfile::tempdir;

    #[tokio::test]
    async fn test_fetch_git_binary() {
        let test_repo = TestRepo((String::from("hello.git"), vec![EXAMPLE_COMMIT.to_string()]));
        let test_dir = tempdir().unwrap();
        let git_config_path = test_dir.path().join("gitconfig");
        let git_config = Config(git_config_path.to_path_buf());
        git_config
            .setup("hello@example.org", 1, false, Path::new("/dev/null"), None)
            .unwrap();
        let repos_dir = test_dir.path().join("repos");
        let test_workspace =
            Workspace::new(test_dir.path().join("workspace").as_path(), "test_fetch");
        test_workspace.init_if_missing(false).unwrap();
        let address = spawn_test_server(repos_dir.as_path(), &[test_repo.clone()])
            .await
            .unwrap();
        let fetch = Fetch {
            git_repo: &test_workspace,
            target_url: &test_repo.address(&address),
            use_git_binary: Some(true),
            git_config_path: git_config_path.as_path(),
        };
        fetch.fetch().await.unwrap();
    }

    #[tokio::test]
    async fn test_fetch_libgit2() {
        let test_repo = TestRepo((String::from("hello.git"), vec![EXAMPLE_COMMIT.to_string()]));
        let test_dir = tempdir().unwrap();
        let git_config_path = test_dir.path().join("gitconfig");
        let git_config = Config(git_config_path.to_path_buf());
        git_config
            .setup("hello@example.org", 1, false, Path::new("/dev/null"), None)
            .unwrap();
        let repos_dir = test_dir.path().join("repos");
        let test_workspace =
            Workspace::new(test_dir.path().join("workspace").as_path(), "test_fetch");
        test_workspace.init_if_missing(false).unwrap();
        let address = spawn_test_server(repos_dir.as_path(), &[test_repo.clone()])
            .await
            .unwrap();
        let fetch = Fetch {
            git_repo: &test_workspace,
            target_url: &test_repo.address(&address),
            use_git_binary: Some(false),
            git_config_path: git_config_path.as_path(),
        };
        fetch.fetch().await.unwrap();
    }
}

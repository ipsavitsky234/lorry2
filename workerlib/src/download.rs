//! Module used to download raw files from the internet and mirror them into a
//! working directory.
//!

use crate::Arguments;
use std::{
    fs::File,
    io::Write,
    path::{Path, PathBuf},
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum FileDownloadError {
    #[error("Could not open the file")]
    CouldNotOpenForWriting {
        source: std::io::Error,
        target: PathBuf,
    },

    #[error("Error occurred while setting up CURL")]
    CouldNotSetUpDownload { source: curl::Error, url: String },

    #[error("There was an error during the download itself")]
    DownloadFailed { source: curl::Error, url: String },

    #[error("An unexpected status code was returned from the server")]
    InvalidStatusCode { source: curl::Error, url: String },
}

/// Download the file from `url` to `file_destination`.
/// FIXME: SHA verification is urgently needed here
pub(crate) async fn download_file(
    file_destination: &Path,
    url: &url::Url,
    check_certs: bool,
    arguments: &Arguments,
) -> Result<(), FileDownloadError> {
    let mut file_to_write_to =
        File::create(file_destination).map_err(|e| FileDownloadError::CouldNotOpenForWriting {
            source: e,
            target: file_destination.to_path_buf(),
        })?;

    let mut handle = curl::easy::Easy::new();
    let mut send_headers = curl::easy::List::new();
    let mut last_modified_recv_header = None;
    handle
        .url(url.as_str())
        .and_then(|_| {
            send_headers.append("User-Agent: Lorry <TODO PUT VERSION AND REPO INFO HERE>")
            // FIXME
        })
        .and_then(|_| handle.http_headers(send_headers))
        .and_then(|_| handle.fail_on_error(true))
        .and_then(|_| handle.max_redirections(arguments.maximum_redirects))
        .and_then(|_| handle.follow_location(arguments.maximum_redirects > 0))
        .and_then(|_| handle.ssl_verify_peer(arguments.check_ssl_certificates && check_certs))
        .and_then(|_| handle.ssl_verify_host(arguments.check_ssl_certificates && check_certs))
        .and_then(|_| {
            handle.write_function(
                // This causes the function to signal an error becuase 0 bytes have been written.
                move |data| file_to_write_to.write(data).or(Ok(0)),
            )
        })
        .map_err(|e| FileDownloadError::CouldNotSetUpDownload {
            source: e,
            url: url.to_string(),
        })?;

    {
        let mut transfer = handle.transfer();

        transfer
            .header_function(|header| {
                let header_line = match std::str::from_utf8(header) {
                    Ok(s) => s,
                    Err(_) => return false,
                };
                if let Some(s) = header_line.to_lowercase().strip_prefix("last-modified: ") {
                    last_modified_recv_header = Some(s.to_owned());
                }
                true
            })
            .map_err(|e| FileDownloadError::CouldNotSetUpDownload {
                source: e,
                url: url.to_string(),
            })?;

        transfer
            .perform()
            .map_err(|e| FileDownloadError::DownloadFailed {
                source: e,
                url: url.to_string(),
            })?;
    }

    if let Ok(code) = handle.response_code() {
        // Any non-200 status code results in the whole operation failing
        // BUG: because of the way this library is designed we use curl to move
        // local files around and thus it is valid to encounter a zero status
        // code. Without accepting zero here we will fail several tests.
        if code != 200 && code != 0 {
            return Err(FileDownloadError::InvalidStatusCode {
                source: curl::Error::new(code),
                url: url.to_string(),
            });
        }
    }

    let file_destination_as_string = file_destination.to_string_lossy();

    // Get the last modified time as UTC
    let url_date = match last_modified_recv_header {
        Some(s) => match chrono::DateTime::parse_from_rfc2822(s.trim()) {
            Ok(dt) => Some(dt),
            Err(e) => {
                tracing::debug!("Was unable to parse the Last-Modified header from {url} into a timestamp. This is not a critical error but will result in incorrect access time metadata for {file_destination_as_string}. Error: {e}
                    the header was : {s}");
                    None
            },
        },
        None => None,
    }.map(chrono::DateTime::<chrono::Utc>::from);

    // If we could extract the remote file's last modified date, apply it to the local file
    match url_date {
        None => Ok(()),
        Some(dt) => {
            async {
                let t =
                    filetime::FileTime::from_unix_time(dt.timestamp(), dt.timestamp_subsec_nanos());
                match filetime::set_file_times(file_destination, t, t) {
                    Err(e) => {
                        tracing::debug!("Was unable to set the last modified time from {url} to to file at {file_destination_as_string}. This is not a critical error but will result in incorrect access time metadata. Error: {e}");
                        Ok(())
                    } // Discard an error here; it doesn't matter.

                    Ok(()) => Ok(()),
                }
            }.await
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use mockito::Server;
    use std::path::Path;
    use tempfile::TempDir;
    use url::Url;

    fn setup(tmp_path: &Path, maximum_redirects: u32) -> Arguments {
        Arguments {
            working_area: tmp_path.join("working-area"),
            maximum_redirects,
            ..Default::default()
        }
    }

    #[tokio::test]
    async fn test_redirection() {
        let mut server = Server::new_async().await;
        // Return HTTP 301
        server
            .mock("GET", "/redirect")
            .with_status(301)
            .with_header("Location", "/target")
            .create_async()
            .await;
        server
            .mock("GET", "/target")
            .with_body("abcd")
            .create_async()
            .await;
        let target_url = server.url() + "/redirect";
        let target = Url::parse(&target_url).unwrap();
        let tmp = TempDir::new().unwrap();
        let args = setup(tmp.path(), 0);
        let ret = download_file(&tmp.path().join("test-file"), &target, false, &args).await;
        // the server returns a redirect but we don't allow for them
        assert!(ret.is_err());
        let err = ret.err().unwrap();
        println!("ERROR: {:?}", err);
        assert!(matches!(err, FileDownloadError::InvalidStatusCode { .. }));
        let args = setup(tmp.path(), 1);
        let ret = download_file(&tmp.path().join("test-file"), &target, false, &args).await;
        // the server follows the redirect and works as expected
        assert!(ret.is_ok());
        let content = std::fs::read_to_string(tmp.path().join("test-file")).unwrap();
        assert!(content == "abcd")
    }
}

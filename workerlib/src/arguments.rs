//! Arguments used across all worker operations
//!

use std::path::{Path, PathBuf};

/// Options specifying how to carry out the mirroring operations
#[derive(Debug, Clone)]
pub struct Arguments {
    /// Directory for holding intermediate git repos and other internal data
    pub working_area: PathBuf,

    /// Validate SSL/TLS server certifications.
    pub check_ssl_certificates: bool,

    ///  The maximum number of redirections that should be followed by the client
    pub maximum_redirects: u32,

    /// If the git binary should be preferred or if libgit2 should be used.
    ///
    /// Libgit2 has stronger security properties guarantee while the git
    /// binary has better performance. Note that raw-file mirrors only use
    /// the git binary due to git's integration with git-lfs.
    pub use_git_binary: Option<bool>,

    /// Number of threads to use for cloning.
    ///
    /// This setting only effects the operation when using the git binary
    pub pack_threads: Option<usize>,

    /// Path to the global git configuration for use with git commands, must
    /// already be configured.
    pub git_config_path: PathBuf,

    /// If sha256sums are required for all raw files.
    ///
    /// NOTE: This will default to true in a future release.
    pub sha256sum_required: bool,
}

impl Default for Arguments {
    fn default() -> Self {
        Self {
            working_area: PathBuf::from("workd"),
            check_ssl_certificates: true,
            maximum_redirects: 0,
            use_git_binary: None,
            pack_threads: None,
            git_config_path: Path::new(crate::git_config::DEFAULT_GIT_CONFIG_PATH).to_path_buf(),
            sha256sum_required: false,
        }
    }
}

//! Representation of `.lorry` specification files
//!

use relative_path::RelativePathBuf;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

/// The contents of a single .lorry configuration file
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
#[serde(tag = "type")]
pub enum LorrySpec {
    #[serde(rename = "git")]
    Git(SingleLorry),

    #[serde(rename = "raw-file")]
    RawFiles(MultipleRawFiles),
}

#[derive(Clone, Default, Serialize, Deserialize, Debug, PartialEq)]
pub struct GCOptions {
    pub enabled: bool,
    pub window_size: Option<String>,
}

#[derive(Clone, Default, Serialize, Deserialize, Debug, PartialEq)]
pub struct SingleLorry {
    pub url: String,

    #[serde(rename = "check-certificates", default = "trivially_true")]
    pub check_ssl_certificates: bool,

    #[deprecated]
    pub refspecs: Option<Vec<String>>,

    pub ref_patterns: Option<Vec<String>>,
    pub ignore_patterns: Option<Vec<String>>,
    pub lfs: Option<bool>,
    pub gc: Option<GCOptions>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Default)]
pub struct MultipleRawFiles {
    pub urls: Vec<RawFileURLMapping>,

    // TODO should this be per file or per-entry?
    #[serde(rename = "check-certificates", default = "trivially_true")]
    pub check_ssl_certificates: bool,

    pub gc: Option<GCOptions>,
}

impl MultipleRawFiles {
    /// Find the first missing sha256sum if any exist
    pub fn missing_sha256sums(&self) -> Vec<String> {
        self.urls
            .iter()
            .filter_map(|f| {
                if f.sha256sum.is_none() {
                    Some(f.url.to_string())
                } else {
                    None
                }
            })
            .collect()
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct RawFileURLMapping {
    #[serde(default = "empty_relative_path")]
    pub destination: RelativePathBuf,
    pub url: url::Url,
    pub sha256sum: Option<String>,
}

fn trivially_true() -> bool {
    true
}

fn empty_relative_path() -> RelativePathBuf {
    RelativePathBuf::new()
}

/// Attempts to convert a string-like, presumably the contents of a `.lorry` file, into the internal representation of a lorry spec.
///
/// This spec object can then be passed to `mirror`.
pub fn extract_lorry_specs<S: AsRef<str>>(
    s: S,
) -> Result<BTreeMap<String, LorrySpec>, serde_yaml::Error> {
    let lorries = serde_yaml::from_str(s.as_ref())?;
    Ok(lorries)
}

#[test]
fn lorries_extract() {
    let lorry_dot_lorry = r"lorry:
    type: git
    url: https://gitlab.com/CodethinkLabs/lorry/lorry.git
  ";

    let extracted = extract_lorry_specs(lorry_dot_lorry).unwrap();

    let mut correct: BTreeMap<String, LorrySpec> = BTreeMap::new();
    correct.insert(
        "lorry".to_string(),
        LorrySpec::Git(SingleLorry {
            url: "https://gitlab.com/CodethinkLabs/lorry/lorry.git".to_string(),
            check_ssl_certificates: true,
            ..Default::default()
        }),
    );
    assert_eq!(extracted, correct);
}

#[test]
fn extract_multiple_lorries() {
    let lorries_as_string = r"ARM-software/arm-trusted-firmware:
    type: git
    url: https://github.com/ARM-software/arm-trusted-firmware.git
HarryMichal/go-version:
    type: git
    url: https://github.com/HarryMichal/go-version.git
TelepathyIM/telepathy-glib:
    type: git
    url: https://github.com/TelepathyIM/telepathy-glib.git
TelepathyIM/telepathy-logger:
    type: git
    url: https://github.com/TelepathyIM/telepathy-logger.git
TelepathyIM/telepathy-mission-control:
    type: git
    url: https://github.com/TelepathyIM/telepathy-mission-control.git
acobaugh/osrelease:
    type: git
    url: https://github.com/acobaugh/osrelease.git
ayufan-rock64/pinebook-pro-keyboard-updater:
    type: git
    url: https://github.com/ayufan-rock64/pinebook-pro-keyboard-updater.git
briandowns/spinner:
    type: git
    url: https://github.com/briandowns/spinner.git
docker/go-units:
    type: git
    url: https://github.com/docker/go-units.git
ebassi/graphene:
    type: git
    url: https://github.com/ebassi/graphene.git
endlessm/eos-installer:
    type: git
    url: https://github.com/endlessm/eos-installer.git
fatih/color:
    type: git
    url: https://github.com/fatih/color.git
fsnotify/fsnotify:
    type: git
    url: https://github.com/fsnotify/fsnotify.git
godbus/dbus:
    type: git
    url: https://github.com/godbus/dbus.git
golang/crypto:
    type: git
    url: https://github.com/golang/crypto.git
golang/sys:
    type: git
    url: https://github.com/golang/sys.git
hughsie/appstream-glib:
    type: git
    url: https://github.com/hughsie/appstream-glib.git
hughsie/libgusb:
    type: git
    url: https://github.com/hughsie/libgusb.git
inconshreveable/mousetrap:
    type: git
    url: https://github.com/inconshreveable/mousetrap.git
konsorten/go-windows-terminal-sequences:
    type: git
    url: https://github.com/konsorten/go-windows-terminal-sequences.git
libhangul/libhangul:
    type: git
    url: https://github.com/libhangul/libhangul.git
libpinyin/libpinyin:
    type: git
    url: https://github.com/libpinyin/libpinyin.git
libsigcplusplus/libsigcplusplus:
    type: git
    url: https://github.com/libsigcplusplus/libsigcplusplus.git
mattn/go-colorable:
    type: git
    url: https://github.com/mattn/go-colorable.git
mattn/go-isatty:
    type: git
    url: https://github.com/mattn/go-isatty.git
raspberrypi/userland:
    type: git
    url: https://github.com/raspberrypi/userland.git
sirupsen/logrus:
    type: git
    url: https://github.com/sirupsen/logrus.git
spf13/cobra:
    type: git
    url: https://github.com/spf13/cobra.git
spf13/pflag:
    type: git
    url: https://github.com/spf13/pflag.git
stevegrubb/libcap-ng:
    type: git
    url: https://github.com/stevegrubb/libcap-ng.git
streambinder/vpnc:
    type: git
    url: https://github.com/streambinder/vpnc.git

";

    //example from https://gitlab.gnome.org/Infrastructure/Mirrors/mirroring-config/-/blob/main/github_com/github_com.lorry

    let extracted = extract_lorry_specs(lorries_as_string).unwrap();

    assert_eq!(extracted.len(), 31);
}

#[test]
fn extract_raw_file_lorries() {
    let lorries_as_string = "tar-files:
    type: raw-file
    urls:
      - destination: debian/pool/main/a/anthy
        url: http://http.debian.net/debian/pool/main/a/anthy/anthy_0.3.orig.tar.gz
      - destination: debian/pool/main/d/db5.3
        url: http://http.debian.net/debian/pool/main/d/db5.3/db5.3_5.3.28.orig.tar.xz
  
  
  ";
    //example from https://gitlab.gnome.org/Infrastructure/Mirrors/mirroring-config/-/blob/main/github_com/github_com.lorry

    let extracted = extract_lorry_specs(lorries_as_string).unwrap();

    assert_eq!(extracted.len(), 1);
    assert_eq!(
        match &extracted["tar-files"] {
            LorrySpec::RawFiles(MultipleRawFiles {
                urls,
                check_ssl_certificates: _,
                gc: _,
            }) => urls.len(),
            _ => panic!("non-rawfiles parsed as raw-files!"),
        },
        2
    )
}

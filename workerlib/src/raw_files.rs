//! Helper wrapper for mirroring arbitrary raw files into an LFS backed remote
//! downstream repository.
//!

use std::io::Error as IoError;
use std::path::Path;

use crate::execute::{execute, Command, CommandBuilder, Error as CommandError};

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Error importing LFS data with the Git Binary: {0}")]
    Command(#[from] CommandError),

    #[error("Error writing .gitattributes: {0}")]
    Io(#[from] IoError),
}

/// Add all files by running `git add --all` which is required to commit files
/// to an LFS enabled raw-files mirror.
struct AddAll<'a> {
    pub worktree: &'a Path,

    // Path to the global git configuration file
    pub config_path: &'a Path,
}

impl CommandBuilder for AddAll<'_> {
    fn build(&self, current_dir: &Path) -> Command {
        let mut cmd = Command::new("git");
        cmd.current_dir(current_dir);
        cmd.envs([(
            crate::git_config::GIT_CONFIG_GLOBAL,
            self.config_path.to_string_lossy().as_ref(),
        )]);
        cmd.args([
            "--bare",
            &format!("--work-tree={}", self.worktree.to_string_lossy()),
            "add",
            "--all",
        ]);
        cmd
    }
}

/// Create a commit message in the git repository
struct CommitAll<'a> {
    pub worktree: &'a Path,

    // Path to the global git configuration file
    pub config_path: &'a Path,
    pub message: &'a str,
}

impl CommandBuilder for CommitAll<'_> {
    fn build(&self, current_dir: &Path) -> Command {
        let mut cmd = Command::new("git");
        cmd.current_dir(current_dir);
        cmd.envs([(
            crate::git_config::GIT_CONFIG_GLOBAL,
            self.config_path.to_string_lossy().as_ref(),
        )]);
        cmd.args([
            "--bare",
            &format!("--work-tree={}", self.worktree.to_string_lossy()),
            "commit",
            &format!("--message={}", self.message),
        ]);
        cmd
    }
}

/// Pull the remote into the configured worktree
///
/// NOTE: This command is only meant to be used for LFS where we need to pull
/// raw files into a working directory
pub struct FetchDownstreamRawFiles<'a> {
    pub url: &'a url::Url,
    pub lfs_url: &'a url::Url,
    pub worktree: &'a Path,

    // Path to the global git configuration file
    pub config_path: &'a Path,
}

impl CommandBuilder for FetchDownstreamRawFiles<'_> {
    fn build(&self, current_dir: &Path) -> Command {
        let mut cmd = Command::new("git");
        cmd.current_dir(current_dir);
        cmd.envs([(
            crate::git_config::GIT_CONFIG_GLOBAL,
            self.config_path.to_string_lossy().as_ref(),
        )]);
        cmd.args([
            "--bare",
            "-c",
            format!("lfs.url={}", self.lfs_url).as_str(),
            format!("--work-tree={}", self.worktree.to_string_lossy()).as_str(),
            "pull",
            self.url.as_str(),
            crate::git_config::DEFAULT_GIT_BRANCH,
        ]);
        cmd
    }
}

/// Push the contents of an LFS backed repository into a downstream
pub struct PushRawFiles<'a> {
    pub url: &'a url::Url,

    /// URL that is used for LFS push operations. NOTE: Due to weirdness with
    /// how git-lfs determines what URL to use to use for POST operations this
    /// is required.
    pub lfs_url: &'a url::Url,

    pub worktree: &'a Path,

    // Path to the global git configuration file
    pub config_path: &'a Path,
}

impl CommandBuilder for PushRawFiles<'_> {
    fn build(&self, current_dir: &Path) -> Command {
        let mut cmd = Command::new("git");
        cmd.current_dir(current_dir);
        cmd.envs([(
            crate::git_config::GIT_CONFIG_GLOBAL,
            self.config_path.to_string_lossy().as_ref(),
        )]);
        cmd.args([
            "--bare",
            "-c",
            format!("lfs.url={}", self.lfs_url).as_str(),
            format!("--work-tree={}", self.worktree.to_string_lossy()).as_str(),
            "push",
            "--porcelain",
            "--tags",
            self.url.as_str(),
            crate::git_config::DEFAULT_GIT_BRANCH,
        ]);
        cmd
    }
}

/// LFS Helper useful for initializing and updating LFS repositories
pub struct Helper(pub crate::workspace::Workspace);

impl Helper {
    async fn commit_all(&self, message: &str, config_path: &Path) -> Result<(), Error> {
        // All non-hidden files present in the work-tree of the repository are
        // added and committed.
        //
        // NOTE: This can never be ran via libgit2 since we need git-lfs's
        // hooks which depend on the Git binary.
        execute(
            &AddAll {
                worktree: self.0.lfs_data_path().as_path(),
                config_path,
            },
            self.0.repository_path().as_path(),
        )
        .await?;
        execute(
            &CommitAll {
                worktree: self.0.lfs_data_path().as_path(),
                config_path,
                message,
            },
            self.0.repository_path().as_path(),
        )
        .await?;
        Ok(())
    }

    pub async fn initial_commit_if_missing(&self, git_config_path: &Path) -> Result<(), Error> {
        // if an LFS repository has no HEAD we assume it's because it has no
        // commits yet and we will initialize it with an LFS configuration that
        // tracks all files within
        if self.0.head().is_some() {
            tracing::debug!("LFS mirror is already active");
            return Ok(());
        }
        tracing::info!("Initializing LFS mirror for the first time");

        // write a gitattribute file that tracks everything except itself
        // within the repository.

        let git_attributes_content = r#"
* filter=lfs diff=lfs merge=lfs -text
*/** filter=lfs diff=lfs merge=lfs -text
.gitattributes  !filter !diff !merge text
"#;

        std::fs::write(
            self.0.lfs_data_path().join(".gitattributes"),
            git_attributes_content.as_bytes(),
        )?;

        self.commit_all("Lorry Initial Commit", git_config_path)
            .await?;
        Ok(())
    }

    pub async fn import_data(&self, git_config_path: &Path) -> Result<(), Error> {
        self.commit_all("Lorry Automated Commit", git_config_path)
            .await?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::git_config::Config as GitConfig;
    use crate::workspace::Workspace;
    use git2::{Repository, Sort};
    use tempfile::tempdir;

    #[tokio::test]
    pub async fn test_rawfile_helper() {
        let test_dir = tempdir().unwrap();
        std::fs::create_dir_all(test_dir.path()).unwrap();
        let git_config_path = test_dir.path().join("gitconfig");
        let git_config = GitConfig(git_config_path);
        git_config
            .setup("hello@example.org", 1, false, Path::new("/dev/null"), None)
            .unwrap();
        let workspace = Workspace::new(test_dir.path(), "mirrors/fuu/bar");
        assert!(workspace.init_if_missing(true).unwrap());
        let helper = Helper(workspace.clone());
        helper
            .initial_commit_if_missing(git_config.0.as_path())
            .await
            .unwrap();
        let test_file = workspace.lfs_data_path().join("hello.txt");
        std::fs::write(test_file.as_path(), br#"world"#).unwrap();
        helper
            .commit_all("This is a new commit", git_config.0.as_path())
            .await
            .unwrap();
        let repo = Repository::open_bare(workspace.repository_path().as_path()).unwrap();
        let mut revwalk = repo.revwalk().unwrap();
        revwalk.set_sorting(Sort::TOPOLOGICAL).unwrap();
        revwalk.push_head().unwrap();
        assert!(revwalk.next().is_some());
        assert!(revwalk.next().is_some());
    }
}

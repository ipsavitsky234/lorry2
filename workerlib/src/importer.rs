//! Responsible for importing files into the file system for LFS backed mirrors
//!

use crate::download::FileDownloadError;
use crate::Arguments;
use crate::{
    download::download_file,
    lorry_specs::{MultipleRawFiles, RawFileURLMapping},
};
use relative_path::{Component, RelativePathBuf};
use sha2::{Digest, Sha256};
use std::collections::BTreeMap;
use std::fmt::{Debug, Display};
use std::fs::File;
use std::path::Path;
use std::{io, io::Error as IoError};
use walkdir::{Error as WalkDirError, WalkDir};

/// List of files to ignore when deleting data from the working directory
const IGNORE_FILES: &[&str] = &[".gitattributes"];

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Download error: {0}")]
    Download(#[from] FileDownloadError),

    #[error("FS error: {0}")]
    FS(#[from] WalkDirError),

    #[error("IO Error: {0}")]
    Io(#[from] IoError),

    #[error("Filename cannot be inferred from path: {0}")]
    InvalidFileName(String),

    /// Parent paths e.g. a/b/../../../../etc/passwd are prohibited since they
    /// could escape the working directory of the improter.
    #[error("Parent paths are prohibited: {0}")]
    ParentPathsProhibited(String),

    #[error("Sha256sum does not match:\n{file_name}\n{expected} != {actual}")]
    Sha256Sum {
        file_name: String,
        expected: String,
        actual: String,
    },
}

#[derive(Clone, Debug, PartialEq, PartialOrd)]
struct Hash(pub String);

impl Display for Hash {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl TryFrom<&Path> for Hash {
    type Error = IoError;

    fn try_from(value: &Path) -> Result<Self, Self::Error> {
        tracing::debug!("Reading sha256sum from file: {:?}", value);
        let mut file = File::open(value)?;
        let mut hasher = Sha256::new();
        let n_bytes = io::copy(&mut file, &mut hasher)?;
        tracing::debug!("Read {} bytes from file", n_bytes);
        let hash = hasher.finalize();
        let file_hash = Hash(format!("{:x}", hash));
        tracing::debug!("Hash: {:?}", file_hash);
        Ok(file_hash)
    }
}

/// Either a remote URL or a local file
#[derive(Clone)]
enum FileEntry {
    Metadata((std::fs::Metadata, Hash)),
    Remote((url::Url, Option<Hash>)),
}

/// Not actually a tree but a map of file system entries
struct FileTree(BTreeMap<RelativePathBuf, FileEntry>);

impl Debug for FileTree {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (key, entry) in self.0.iter() {
            match entry {
                FileEntry::Metadata(metadata) => {
                    writeln!(
                        f,
                        "{}: {:?} [{}]",
                        key,
                        metadata.0.file_type(),
                        metadata.1 .0
                    )?;
                }
                FileEntry::Remote(url) => {
                    writeln!(f, "{}: {:?} [{:?}]", key, url.0.to_string(), url.1)?;
                }
            }
        }
        Ok(())
    }
}

/// FileTrees is considered equal when both trees contain the same files based
/// on name only.
///
/// TODO: We need to implement SHA verification here as well
impl PartialEq for FileTree {
    fn eq(&self, other: &Self) -> bool {
        if self.0.len() != other.0.len() {
            return false;
        }

        for key in other.0.keys() {
            if let Some(this_value) = self.0.get(key) {
                let other_value = other.0.get(key).unwrap();
                match this_value {
                    FileEntry::Metadata(this_metadata) => match other_value {
                        FileEntry::Metadata(other_metadata) => {
                            if this_metadata.1 != other_metadata.1 {
                                return false;
                            }
                        }
                        FileEntry::Remote(other_remote) => {
                            if let Some(other_remote_hash) = &other_remote.1 {
                                if this_metadata.1 != *other_remote_hash {
                                    return false;
                                }
                            }
                        }
                    },
                    FileEntry::Remote(this_remote) => match other_value {
                        FileEntry::Metadata(other_metadata) => {
                            if let Some(this_remote_hash) = &this_remote.1 {
                                if *this_remote_hash != other_metadata.1 {
                                    return false;
                                }
                            }
                        }
                        FileEntry::Remote(other_remote) => {
                            if this_remote.1.is_some() && other_remote.1.is_some() {
                                let this_remote_hash = this_remote.1.as_ref().unwrap();
                                let other_remote_hash = other_remote.1.as_ref().unwrap();
                                if *this_remote_hash != *other_remote_hash {
                                    return false;
                                }
                            }
                        }
                    },
                }
            } else {
                return false;
            }
        }

        true
    }
}

impl FileTree {
    /// Create a FileTree from the local file system
    pub fn from_path(path: &Path) -> Result<FileTree, Error> {
        let mut tree: BTreeMap<RelativePathBuf, FileEntry> = BTreeMap::new();
        for entry in WalkDir::new(path) {
            let entry = entry?;
            if path == entry.path() {
                // skip root path
                continue;
            }
            let metadata = entry.metadata()?;
            if metadata.is_symlink() {
                // symlinks are ignored
                continue;
            }
            if metadata.is_dir() {
                // directories are ignored
                continue;
            }
            let entry_path = entry.into_path();
            let stripped =
                RelativePathBuf::from_path(entry_path.strip_prefix(path).unwrap()).unwrap();
            if IGNORE_FILES.contains(&stripped.as_str()) {
                continue;
            }
            let absolute_path = entry_path.as_path();
            let file_hash: Hash = absolute_path.try_into()?;
            tree.insert(
                stripped.normalize(),
                FileEntry::Metadata((metadata.clone(), file_hash)),
            );
        }
        Ok(FileTree(tree))
    }

    /// Create a FileTree from an array of URL mappings.
    pub fn from_urls(files: &[RawFileURLMapping]) -> Result<FileTree, Error> {
        let mut tree: BTreeMap<RelativePathBuf, FileEntry> = BTreeMap::new();
        for raw_file_mapping in files {
            // relative parent paths are prohibited since they could escape the
            // working directory.
            if raw_file_mapping
                .destination
                .components()
                .any(|component| matches!(component, Component::ParentDir))
            {
                return Err(Error::ParentPathsProhibited(
                    raw_file_mapping.destination.to_string(),
                ));
            }

            let url_path = Path::new(raw_file_mapping.url.path());
            let file_name = url_path.file_name().map_or(
                Err(Error::InvalidFileName(
                    raw_file_mapping.url.path().to_string(),
                )),
                |name| Ok(name.to_string_lossy()),
            )?;
            let mut target_path = raw_file_mapping.destination.clone();
            target_path.push(file_name.to_string());
            tree.insert(
                target_path.normalize(),
                FileEntry::Remote((
                    raw_file_mapping.url.clone(),
                    raw_file_mapping.sha256sum.clone().map(Hash),
                )),
            );
        }
        Ok(FileTree(tree))
    }
}

/// Importer is used to download raw files from various places on the
/// internet and import them into a directory which is tracked with Git LFS.
pub struct Importer(pub MultipleRawFiles);

impl Importer {
    /// Ensure that the file system matches what is desired in the
    /// configuraiton. Files and directories on the filesystem which are not
    /// described in the configuration are removed. New files which are present
    /// in the configuration but not on the file system are downloaded. Files
    /// which are present in both the configuration and file system are assumed
    /// to be up to date.
    ///
    /// FIXME: See the last sentence above.
    ///
    /// FIXME: No file verification via hashing is currently performed.
    ///
    /// FIXME: This operation is single threaded and one slow host will cause
    /// the entire operation to hang.
    ///
    /// NOTE: Empty directories are ignored since they're ignored in Git.
    pub async fn ensure(self, work_dir: &Path, args: &Arguments) -> Result<bool, Error> {
        let current_tree = FileTree::from_path(work_dir)?;
        tracing::debug!("Current local file tree: {:?}", current_tree);
        let desired_tree = FileTree::from_urls(self.0.urls.as_slice())?;
        tracing::debug!("Desired file tree: {:?}", desired_tree);
        if current_tree.eq(&desired_tree) {
            return Ok(false);
        }
        let mut modified = false;
        for (relative_path, entry) in current_tree.0 {
            if IGNORE_FILES.contains(&relative_path.to_string().as_str()) {
                tracing::debug!("Ignoring file: {:?}", relative_path);
                continue;
            };
            match entry {
                FileEntry::Metadata(metadata) => {
                    let absolute_path = relative_path.to_path(work_dir);
                    if let Some(desired_entry) = desired_tree.0.get(&relative_path) {
                        match desired_entry {
                            FileEntry::Metadata(_) => unreachable!(),
                            FileEntry::Remote(desired_remote_entry) => {
                                if let Some(desired_sha) = &desired_remote_entry.1 {
                                    if metadata.1 != *desired_sha {
                                        tracing::debug!(
                                            "Removing file {} because it has differing sha256sum values:\n{:?} != {:?}", 
                                            metadata.1.0, metadata.1, desired_sha);
                                        tokio::fs::remove_file(absolute_path).await?;
                                        modified = true;
                                    }
                                }
                            }
                        }
                    } else {
                        tracing::debug!("Removing file: {:?}", absolute_path);
                        if metadata.0.is_file() || metadata.0.is_symlink() {
                            tokio::fs::remove_file(absolute_path).await?;
                            modified = true;
                        } else if metadata.0.is_dir() {
                            // directories not considered
                        }
                    }
                }
                FileEntry::Remote(_) => unreachable!(),
            }
        }

        for (relative_path, entry) in &desired_tree.0 {
            match entry {
                FileEntry::Metadata(_) => unreachable!(),
                FileEntry::Remote(url) => {
                    // ensure that relative path components e.g. fuu/../../bar
                    // cannot escape the work directory.
                    let resolved_path = relative_path.to_path(work_dir);
                    if tokio::fs::metadata(&resolved_path).await.is_ok() {
                        // file already exists, nothing to do
                        continue;
                    }
                    if let Some(parent_dir) = relative_path.parent() {
                        tokio::fs::create_dir_all(parent_dir.to_path(work_dir)).await?;
                    }
                    tracing::info!("Downloading remote file: {:?}", resolved_path);
                    download_file(&resolved_path, &url.0, false, args).await?;
                    if let Some(desired_hash) = &url.1 {
                        let path = resolved_path.as_path();
                        let actual_hash: Hash = path.try_into()?;
                        if *desired_hash != actual_hash {
                            tracing::warn!(
                                "sha256sums do not match: [{:?}] {:?} != {:?}",
                                resolved_path,
                                desired_hash,
                                actual_hash
                            );
                            return Err(Error::Sha256Sum {
                                file_name: url.0.path().to_string(),
                                expected: desired_hash.to_string(),
                                actual: actual_hash.to_string(),
                            });
                        }
                    }
                    modified = true
                }
            }
        }

        Ok(modified)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use mockito::Server;
    use relative_path::RelativePathBuf;
    use std::path::Path;
    use tempfile::tempdir;
    use url::Url;

    use crate::lorry_specs::RawFileURLMapping;

    #[test]
    pub fn test_importer_file_tree() {
        let work_dir = tempdir().unwrap();
        let fs_dir = work_dir.path();
        let target_file = fs_dir.join("fuu/bar/a.txt");
        std::fs::create_dir_all(fs_dir.join("fuu/bar")).unwrap();
        std::fs::write(target_file, "hello".as_bytes()).unwrap();
        let fs_tree = FileTree::from_path(work_dir.path()).unwrap();
        let remote_tree = FileTree::from_urls(&[RawFileURLMapping {
            destination: RelativePathBuf::from_path(Path::new("fuu/bar")).unwrap(),
            url: Url::parse("http://example.org/some/random/path/a.txt").unwrap(),
            sha256sum: None,
        }])
        .unwrap();
        assert!(fs_tree.eq(&remote_tree));
    }

    #[test]
    pub fn test_importer_file_tree_sha256sum() {
        let work_dir = tempdir().unwrap();
        let fs_dir = work_dir.path();
        let target_file = fs_dir.join("fuu/bar/a.txt");
        std::fs::create_dir_all(fs_dir.join("fuu/bar")).unwrap();
        std::fs::write(target_file, "hello".as_bytes()).unwrap();
        let fs_tree = FileTree::from_path(work_dir.path()).unwrap();
        let remote_tree = FileTree::from_urls(&[RawFileURLMapping {
            destination: RelativePathBuf::from_path(Path::new("fuu/bar")).unwrap(),
            url: Url::parse("http://example.org/some/random/path/a.txt").unwrap(),
            sha256sum: Some(String::from(
                "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824",
            )),
        }])
        .unwrap();
        assert!(fs_tree.eq(&remote_tree));
    }

    #[test]
    pub fn test_importer_parent_path() {
        assert!(FileTree::from_urls(&[RawFileURLMapping {
            destination: RelativePathBuf::from_path(Path::new("../../etc/passwd")).unwrap(),
            url: Url::parse("http://example.org/some/random/path/a.txt").unwrap(),
            sha256sum: None,
        }])
        .is_err_and(|e| matches!(e, Error::ParentPathsProhibited(_))))
    }

    /// Desired file file structure within the repository is:
    /// ```txt
    /// test/content-1.txt
    /// a/b/content-2.txt
    /// ```
    #[tokio::test]
    pub async fn test_importer_empty_dir() {
        let mut server = Server::new_async().await;
        let content_url_1 = server
            .mock("GET", "/content-1.txt")
            .with_body("content-1")
            .with_status(200)
            .create_async()
            .await;

        let content_url_2 = server
            .mock("GET", "/content-2.txt")
            .with_body("content-2")
            .with_status(200)
            .create_async()
            .await;

        let importer = Importer(MultipleRawFiles {
            urls: vec![
                RawFileURLMapping {
                    destination: RelativePathBuf::from_path(Path::new("test")).unwrap(),
                    url: Url::parse(&(server.url() + "/content-1.txt")).unwrap(),
                    sha256sum: None,
                },
                RawFileURLMapping {
                    destination: RelativePathBuf::from_path(Path::new("a/b")).unwrap(),
                    url: Url::parse(&(server.url() + "/content-2.txt")).unwrap(),
                    sha256sum: None,
                },
            ],
            ..Default::default()
        });

        let work_dir = tempdir().unwrap();
        assert!(importer
            .ensure(
                work_dir.path(),
                &Arguments {
                    working_area: work_dir.path().to_path_buf(),
                    check_ssl_certificates: false,
                    ..Default::default()
                },
            )
            .await
            .unwrap());

        // both content URLs must have been accessed
        content_url_1.assert_async().await;
        content_url_2.assert_async().await;

        let content_1_path = work_dir.path().join("test/content-1.txt");
        let content_1 = std::fs::read_to_string(content_1_path).unwrap();
        assert!(content_1 == "content-1");

        let content_2_path = work_dir.path().join("a/b/content-2.txt");
        let content_2 = std::fs::read_to_string(content_2_path).unwrap();
        assert!(content_2 == "content-2");
    }

    /// Desired file file structure within the repository is:
    /// ```text
    /// test/content-1.txt
    /// a/b/content-2.txt
    /// ```
    #[tokio::test]
    pub async fn test_importer_invalid_sha256sum() {
        let mut server = Server::new_async().await;

        let content_url = server
            .mock("GET", "/content.txt")
            .with_body("content")
            .with_status(200)
            .create_async()
            .await;

        let importer = Importer(MultipleRawFiles {
            urls: vec![RawFileURLMapping {
                destination: RelativePathBuf::from_path(Path::new("a/b")).unwrap(),
                url: Url::parse(&(server.url() + "/content.txt")).unwrap(),
                sha256sum: Some(String::from("invalid sha")),
            }],
            ..Default::default()
        });

        let work_dir = tempdir().unwrap();
        let err = importer
            .ensure(
                work_dir.path(),
                &Arguments {
                    working_area: work_dir.path().to_path_buf(),
                    check_ssl_certificates: false,
                    ..Default::default()
                },
            )
            .await;

        assert!(err.is_err_and(|e| {
            match e {
                Error::Sha256Sum {
                    file_name,
                    expected,
                    actual,
                } => {
                    (file_name == "/content.txt")
                        && (expected == "invalid sha")
                        // SHA of 'content'
                        && (actual
                            == "ed7002b439e9ac845f22357d822bac1444730fbdb6016d3ec9432297b9ec9f73")
                }
                _ => panic!("wrong error type"),
            }
        }));

        content_url.assert_async().await;

        let content_path = work_dir.path().join("a/b/content.txt");
        let content = std::fs::read_to_string(content_path).unwrap();
        assert!(content == "content");

        // fix the SHA and run again

        let mut server = Server::new_async().await;

        let content_url = server
            .mock("GET", "/content.txt")
            .with_body("content")
            .with_status(200)
            .create_async()
            .await;

        let importer = Importer(MultipleRawFiles {
            urls: vec![RawFileURLMapping {
                destination: RelativePathBuf::from_path(Path::new("a/b")).unwrap(),
                url: Url::parse(&(server.url() + "/content.txt")).unwrap(),
                sha256sum: Some(String::from(
                    "ed7002b439e9ac845f22357d822bac1444730fbdb6016d3ec9432297b9ec9f73",
                )),
            }],
            ..Default::default()
        });

        let work_dir = tempdir().unwrap();
        assert!(importer
            .ensure(
                work_dir.path(),
                &Arguments {
                    working_area: work_dir.path().to_path_buf(),
                    check_ssl_certificates: false,
                    ..Default::default()
                },
            )
            .await
            .unwrap());

        content_url.assert_async().await;

        let content_path = work_dir.path().join("a/b/content.txt");
        let content = std::fs::read_to_string(content_path).unwrap();
        assert!(content == "content");
    }

    /// Desired file file structure within the repository is:
    /// ```txt
    /// test/content-1.txt
    /// a/b/content-2.txt
    /// ```
    ///
    /// The initial content of the repository is:
    /// ```txt
    /// excess-file.txt
    /// ```
    #[tokio::test]
    pub async fn test_importer_excess_files() {
        let mut server = Server::new_async().await;
        let content_url_1 = server
            .mock("GET", "/content-1.txt")
            .with_body("content-1")
            .with_status(200)
            .create_async()
            .await;

        let content_url_2 = server
            .mock("GET", "/content-2.txt")
            .with_body("content-2")
            .with_status(200)
            .create_async()
            .await;

        let importer = Importer(MultipleRawFiles {
            urls: vec![
                RawFileURLMapping {
                    destination: RelativePathBuf::from_path(Path::new("content")).unwrap(),
                    url: Url::parse(&(server.url() + "/content-1.txt")).unwrap(),
                    sha256sum: None,
                },
                RawFileURLMapping {
                    destination: RelativePathBuf::from_path(Path::new("a/b")).unwrap(),
                    url: Url::parse(&(server.url() + "/content-2.txt")).unwrap(),
                    sha256sum: None,
                },
            ],
            ..Default::default()
        });

        let work_dir = tempdir().unwrap();

        let tmp_dir = work_dir.path();
        let excess_file_path = tmp_dir.join("excess-file.txt");

        std::fs::write(&excess_file_path, b"fuu").unwrap();

        assert!(importer
            .ensure(
                work_dir.path(),
                &Arguments {
                    working_area: work_dir.path().to_path_buf(),
                    check_ssl_certificates: false,
                    ..Default::default()
                },
            )
            .await
            .unwrap());

        // Both content URLs must have been accessed
        content_url_1.assert_async().await;
        content_url_2.assert_async().await;

        let content_1_path = work_dir.path().join("content/content-1.txt");
        let content_1 = std::fs::read_to_string(content_1_path).unwrap();
        assert!(content_1 == "content-1");

        let content_2_path = work_dir.path().join("a/b/content-2.txt");
        let content_2 = std::fs::read_to_string(content_2_path).unwrap();
        assert!(content_2 == "content-2");

        assert!(std::fs::metadata(excess_file_path).is_err());
    }

    /// Verify that importer successfully detects no files have changed and
    /// thus no commit is required.
    #[tokio::test]
    pub async fn test_importer_no_change() {
        let importer = Importer(MultipleRawFiles {
            urls: vec![RawFileURLMapping {
                destination: RelativePathBuf::from_path(Path::new("a/b")).unwrap(),
                url: Url::parse("http://example.org/fuu/bar/content-2.txt").unwrap(),
                sha256sum: None,
            }],
            ..Default::default()
        });

        let work_dir = tempdir().unwrap();

        let tmp_dir = work_dir.path();
        let excess_file_path = tmp_dir.join("a/b/content-2.txt");

        std::fs::create_dir_all(excess_file_path.parent().unwrap()).unwrap();
        std::fs::write(&excess_file_path, b"content-2").unwrap();

        assert!(!importer
            .ensure(
                work_dir.path(),
                &Arguments {
                    working_area: work_dir.path().to_path_buf(),
                    check_ssl_certificates: false,
                    ..Default::default()
                },
            )
            .await
            .unwrap());
    }

    /// Test to ensure key names are normalized
    #[tokio::test]
    pub async fn test_importer_normalized_key_names() {
        let work_dir = tempdir().unwrap();
        let tmp_dir = work_dir.path();
        let excess_file_path = tmp_dir.join("content-2.txt");
        std::fs::create_dir_all(excess_file_path.parent().unwrap()).unwrap();
        std::fs::write(&excess_file_path, b"content-2").unwrap();

        // This file should not be considered
        let git_attributes_file_path = tmp_dir.join(".gitattributes");
        std::fs::create_dir_all(git_attributes_file_path.parent().unwrap()).unwrap();
        std::fs::write(&git_attributes_file_path, b"attributes content").unwrap();

        let fs_tree = &FileTree::from_path(tmp_dir).unwrap();
        let desired_tree = &FileTree::from_urls(&[RawFileURLMapping {
            destination: RelativePathBuf::from_path(Path::new(".")).unwrap(),
            url: Url::parse("http://example.org/content-2.txt").unwrap(),
            sha256sum: None,
        }])
        .unwrap();

        println!("Local:\n{:?}", fs_tree);
        println!("Desired:\n{:?}", desired_tree);

        assert!(fs_tree == desired_tree);
    }
}

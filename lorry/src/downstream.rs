//!
//! // TODO the way this is implemented, it might make more sense to use a Trait Object instead of an enum?
//!

pub mod git_lab;
pub mod local;

use crate::comms;
use git_lab::PrepareRepoError;
use git_lab::TokenSource;
use local::PrepareLocalRepoError;
use url::Url;

/// Metadata about the mirrored repo. Should be reasonably lowest common denominator
#[derive(Debug)]
pub struct RepoMetadata {
    /// The branch to use as the default branch of the repo
    pub head: Option<String>,

    /// Repo description to display to viewers
    pub description: Option<String>,
}

/// Connection to a downstream host.
#[derive(Clone, Debug)]
pub enum Downstream {
    GitLab(git_lab::GitLabDownstream),
    Local(local::LocalDownstream),
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum DownstreamError {
    #[error("Failure preparing Gitlab repo: {0}")]
    PrepareGitlabRepo(#[from] PrepareRepoError),

    #[error("Failure preparing local repo: {0}")]
    PrepareLocalRepo(#[from] PrepareLocalRepoError),
}

impl Downstream {
    /// Create the target git repository for a minion to mirror to.
    pub async fn prepare_repo(
        &self,
        repo_path: &comms::LorryPath,
        metadata: RepoMetadata,
    ) -> Result<(), DownstreamError> {
        match self {
            Downstream::GitLab(g) => g.prepare_repo(repo_path, metadata).await?,
            Downstream::Local(l) => l.prepare_repo(repo_path, metadata).await?,
        }
        Ok(())
    }

    pub(crate) fn mirror_base_url(&self) -> Url {
        match self {
            Downstream::GitLab(g) => g.git_url.clone(),
            // Don't care out the panic here - `Local` is only used for testing purposes!
            Downstream::Local(l) => Url::from_directory_path(&l.base_dir)
                .expect("Lorry needs an absolute file path for local downstreams"),
        }
    }

    pub(crate) fn insecure(&self) -> Option<bool> {
        match self {
            Downstream::GitLab(g) => Some(g.insecure),
            Downstream::Local(_) => None,
        }
    }
}

/// Configuration for the connection to the downstream host. Can be converted to a `Downstream` to create the connection.
#[derive(Debug)]
#[allow(dead_code)]
pub enum DownstreamSetup {
    /// Use the local filesystem to host mirror repositories.
    // TODO: Re-implement support for this.
    Local {
        /// The directory to the folder containing mirrors
        base_dir: std::path::PathBuf,
    },

    /// Use a downstream git server running GitLab
    Gitlab {
        /// The base host name of the server.
        /// Please do not include a scheme identifier.
        hostname: Url,

        /// Visibility setting for the created repositories
        downstream_visibility: Visibility,

        /// Private access token to use for access to the GitLab API. Make sure that this has full access to the server you are mirroring to!
        private_token: Option<String>,

        // A path to a file that contains a token for the Gitlab API.
        token_file: Option<std::path::PathBuf>,

        /// Permit insecure access to gitlab
        insecure: bool,
    },
}

/// Visibility setting to use when creating a repo. While this is host-dependent,
/// the current implementation should be lowest common denominator.
#[derive(clap::ValueEnum, Copy, Clone, Debug, serde::Deserialize, Default)]
pub enum Visibility {
    #[serde(rename = "public")]
    Public,

    #[default]
    #[serde(rename = "internal")]
    Internal,

    #[serde(rename = "private")]
    Private,
}

#[async_convert::async_trait]
impl async_convert::TryFrom<DownstreamSetup> for Downstream {
    type Error = gitlab::GitlabError;

    async fn try_from(value: DownstreamSetup) -> Result<Self, Self::Error> {
        match value {
            DownstreamSetup::Local { base_dir } => {
                Ok(Downstream::Local(local::LocalDownstream { base_dir }))
            }
            DownstreamSetup::Gitlab {
                hostname,
                downstream_visibility,
                private_token,
                token_file,
                insecure,
            } => Ok(Downstream::GitLab(git_lab::GitLabDownstream {
                downstream_visibility: downstream_visibility.into(),
                git_url: hostname,
                token_source: if let Some(t) = private_token {
                    TokenSource::Token(t)
                } else if let Some(f) = token_file {
                    TokenSource::Path(f)
                } else {
                    panic!("Not token source given")
                },
                insecure,
            })),
        }
    }
}

impl From<Visibility> for gitlab::api::common::VisibilityLevel {
    fn from(value: Visibility) -> Self {
        match value {
            Visibility::Public => gitlab::api::common::VisibilityLevel::Public,
            Visibility::Internal => gitlab::api::common::VisibilityLevel::Internal,
            Visibility::Private => gitlab::api::common::VisibilityLevel::Private,
        }
    }
}

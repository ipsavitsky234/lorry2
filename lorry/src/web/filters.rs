//!
//! Helpers for displaying info in the web UI.
//!

use askama::{Error, Result};
use chrono::{DateTime, Utc};
use std::fmt::Display;

// Apparently these exist in nightly rust?
const SECOND: i64 = 1;
const MINUTE: i64 = SECOND * 60;
const HOUR: i64 = MINUTE * 60;
const DAY: i64 = HOUR * 24;
const WEEK: i64 = DAY * 7;
const MONTH: i64 = DAY * 30;
const YEAR: i64 = DAY * 365;

fn pluralize(value: i64) -> String {
    match value {
        1 => String::new(),
        _ => String::from("s"),
    }
}

/// Convert number of a seconds into a friendly string.
///
/// As second/minute/hour/day/week/month/year
///
/// e.g.:
/// * 1 second
/// * 2 seconds
/// * 3 days
/// * 2 months
/// * 1 year
fn friendly(seconds: i64) -> String {
    let message: String;
    if seconds < MINUTE {
        message = format!("{} second{}", seconds, pluralize(seconds));
    } else if seconds < HOUR {
        message = format!("{} minute{}", seconds / MINUTE, pluralize(seconds / MINUTE),)
    } else if seconds < DAY {
        message = format!("{} hour{}", seconds / HOUR, pluralize(seconds / HOUR),)
    } else if seconds < WEEK {
        message = format!("{} day{}", seconds / DAY, pluralize(seconds / DAY),)
    } else if seconds < MONTH {
        message = format!("{} week{}", seconds / WEEK, pluralize(seconds / WEEK),)
    } else if seconds < YEAR {
        message = format!("{} month{}", seconds / MONTH, pluralize(seconds / MONTH),)
    } else {
        message = format!("{} year{}", seconds / YEAR, pluralize(seconds / YEAR),)
    }
    message
}

fn get_i64<T: Display>(s: T) -> Result<i64> {
    let seconds: i64 = s
        .to_string()
        .parse()
        .map_err(|e| Error::Custom(Box::new(e)))?;
    Ok(seconds)
}

pub fn timestamp<T: Display>(s: T) -> Result<String> {
    let seconds = get_i64(s)?;
    match DateTime::from_timestamp(seconds, 0) {
        Some(timestamp) => Ok(timestamp.to_rfc3339()),
        None => Ok(String::from("?")),
    }
}

pub fn friendly_time<T: Display>(s: T) -> Result<String> {
    let seconds = get_i64(s)?;
    if seconds > 0 {
        match DateTime::from_timestamp(seconds, 0) {
            Some(timestamp) => Ok(format!(
                "{} ago",
                friendly(Utc::now().timestamp() - timestamp.timestamp())
            )),
            None => Ok(String::from("?")),
        }
    } else {
        Ok(String::from("?"))
    }
}

pub fn friendly_duration<T: Display>(s: T) -> Result<String> {
    let seconds = get_i64(s)?;
    Ok(friendly(seconds))
}

pub fn duration_between<T: Display>(start: T, end: T) -> Result<String> {
    let start_time = DateTime::from_timestamp(get_i64(start)?, 0).unwrap();
    let end_time = DateTime::from_timestamp(get_i64(end)?, 0).unwrap();
    let duration = end_time.signed_duration_since(start_time);
    Ok(format!("{}s", duration.num_seconds()))
}

pub fn pretty_job_status<T: Display>(code_str: T) -> Result<String> {
    match code_str.to_string().as_str() {
        "None" => Ok(String::from(
            r#"<span class="badge" style="background-color: Grey">None</span>"#,
        )),
        "Successful" => Ok(String::from(
            r#"<span class="badge" style="background-color: DarkGreen">Success</span>"#,
        )),
        "Failed" => Ok(String::from(
            r#"<span class="badge" style="background-color: DarkRed">Failed</span>"#,
        )),
        "RefsFailed" => Ok(String::from(
            r#"<span class="badge" style="background-color: Indigo">RefsFailed</span>"#,
        )),
        _ => unreachable!(), // unreachable due to db constraints
    }
}

pub fn pretty_status<T: Display>(status: T) -> Result<String> {
    match status.to_string().as_str() {
        "Running" => Ok(
            r#"<span class="badge" style="background-color: DarkGreen">Running</span>"#.to_string(),
        ),
        "Ready" => Ok(
            r#"<span class="badge" style="background-color: DarkBlue">Ready</span>"#.to_string(),
        ),
        "Idle" => {
            Ok(r#"<span class="badge" style="background-color: DarkGray">Idle</span>"#.to_string())
        }
        status => Ok(format!(r#"<span class="badge">{}</span>"#, status)),
    }
}

pub fn pretty_warning_badge<T: Display>(n_warnings: T) -> Result<String> {
    match get_i64(n_warnings) {
        Ok(count) => {
            if count == 0 {
                Ok(String::default())
            } else {
                Ok(format!(
                    r#"<span class="badge" style="background-color: Peru">Warnings: {}</span>"#,
                    count
                ))
            }
        }
        Err(e) => Ok(e.to_string()),
    }
}

pub fn pretty_warning_count<T: Display>(n_warnings: T) -> Result<String> {
    match get_i64(n_warnings) {
        Ok(count) => {
            if count == 0 {
                Ok(String::default())
            } else {
                Ok(format!(
                    r#"<span class="badge" style="background-color: Peru">Count: {}</span>"#,
                    count
                ))
            }
        }
        Err(e) => Ok(e.to_string()),
    }
}

pub fn thread_status<T: Display>(status: T) -> Result<String> {
    match status.to_string().as_str() {
        "Busy" => {
            Ok(r#"<span class="badge" style="background-color: DarkBlue">Busy</span>"#.to_string())
        }
        "Idle" => {
            Ok(r#"<span class="badge" style="background-color: DarkGray">Idle</span>"#.to_string())
        }
        status => Ok(format!(r#"<span class="badge">{}</span>"#, status)),
    }
}

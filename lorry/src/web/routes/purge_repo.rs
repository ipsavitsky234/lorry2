//!
//! Tell minions that check in to delete old copies of a mirror, because it is
//! believed that corruption or other unrecoverable error occurred.
//!

use crate::web::ControllerState;
use axum::{extract::State, http::StatusCode, Json};

/// Path to the endpoint to mark a mirror as needed deletion from minion
/// internal workspaces.
pub const PATH: &str = "/1.0/purge";

#[derive(thiserror::Error, Debug)]
pub enum PurgeError {
    #[error(transparent)]
    DBOperationFailed(#[from] sqlx::Error),
}

impl axum::response::IntoResponse for PurgeError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            PurgeError::DBOperationFailed(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("DB transaction failed: {:?}", e),
            ),
        };

        tracing::error!(
            endpoint =%PATH,
            "Failed to set this repo to be deleted from disks: {}",
            err_msg
        ); // TODO: Get the path of the lorry that was accessed, so that the DB logger will also know
           // Then again, this would be called from the purge_repo span
           // Yes but it won't be logged to the `from_path` field in the DB

        (status, err_msg).into_response()
    }
}

/// Handle a GET request to [PATH]
///
/// In some scenarios, it is possible for the internal repositories kept by
/// minions to be damaged in some way.  This will result in errors when
/// carrying out the mirroring operation. The only recovery method would be to
/// delete the internal repos and start mirroring again. This is a safe
/// operation because the downstream will be unaffected.
///
/// This endpoint tells the controller that future minions checking in on this
/// Lorry should delete internal repos from before the time the endpoint was
/// called.
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_post_purge_repo(
    State(state): State<ControllerState>,
) -> Result<Json<String>, PurgeError> {
    state.db.set_repo_to_purge().await?;

    Ok("Repo has been marked for deletion from local systems"
        .to_string()
        .into())
}

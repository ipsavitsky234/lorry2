//!
//! View the Lorry config.
//!

use crate::read_config::resolve;
use crate::web::error::WebError;
use crate::web::ControllerState;
use askama::Template;
use axum::extract::State;
use serde_yaml::to_string;

#[derive(Template)]
#[template(path = "config.html")]
pub struct Spec<'a> {
    title: &'a str,
    stylesheet: &'static str,
    logo: &'static str,
    resolved: String,
}

/// Handle a GET request to `/config`.
///
/// Display the Lorry config used.
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_config(
    State(state): State<ControllerState>,
) -> Result<Spec<'static>, WebError> {
    let configs = resolve(&state.app_settings.configuration_directory)
        .map_err(|e| WebError::Message(e.to_string()))?;
    let resolved = to_string(&configs).unwrap();
    Ok(Spec {
        stylesheet: include_str!("../../../assets/pico.min.css"),
        logo: include_str!("../../../assets/codethink-logo.svg"),
        title: "Configuration",
        resolved,
    })
}

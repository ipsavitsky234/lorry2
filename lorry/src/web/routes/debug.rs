//!
//! Display debug info such as the status of Lorry threads.
//!

use crate::web::ControllerState;
use crate::web::{error::WebError, filters};
use crate::worker::State as WorkerState;
use askama::Template;
use axum::extract::State;

pub type Thread = (String, String, Option<(String, i64)>);

#[derive(Template)]
#[template(path = "debug.html")]
pub struct Index<'a> {
    title: &'a str,
    stylesheet: &'static str,
    logo: &'static str,
    threads: Vec<Thread>,
}

fn read_threads(state: ControllerState) -> Vec<Thread> {
    let mut threads: Vec<Thread> = Vec::with_capacity(10);
    let tracker = state.tracker.read().unwrap();
    for (key, value) in tracker.iter() {
        let name = format!("{:?}", key);
        match value {
            WorkerState::Busy((lorry, job_id)) => {
                threads.push((name, "Busy".to_string(), Some((lorry.clone(), job_id.0))))
            }
            WorkerState::Idle => threads.push((name, "Idle".to_string(), None)),
        }
    }
    threads
}

/// Handle a GET request to `/debug`
#[axum_macros::debug_handler]
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_debug(
    State(state): State<ControllerState>,
) -> Result<Index<'static>, WebError> {
    let threads = read_threads(state.clone());
    Ok(Index {
        title: "Debug",
        threads,
        stylesheet: include_str!("../../../assets/pico.min.css"),
        logo: include_str!("../../../assets/codethink-logo.svg"),
    })
}

//!
//! Give a list of all lorries, as well as status information, such as the last
//! run results, or the mirroring interval.
//!

use crate::state_db::LorryEntry;
use crate::web::ControllerState;
use axum::{extract::State, http::StatusCode, Json};

/// Path to the endpoint to list mirror status
pub const PATH: &str = "/1.0/list-lorries";

#[derive(thiserror::Error, Debug)]
pub(crate) enum ListAllLorriesAsJsonError {
    #[error("Error occurred during database operation")]
    DBError(#[from] sqlx::Error),
}

impl axum::response::IntoResponse for ListAllLorriesAsJsonError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            ListAllLorriesAsJsonError::DBError(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Database operation getting the jobs failed: {:?}", e),
            ),
        };

        // TODO is it worth logging this error to the database?
        // Firstly, it's not critical to operation.
        // Secondly, the only possible error is a DB error, so odds are the write will fail too.
        tracing::error!(endpoint = %PATH, "Error at listing all lorries: {}", err_msg);

        (status, err_msg).into_response()
    }
}

/// Handle a GET request to [PATH]
///
/// Return a list of all the mirror configurations.
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_list_all_lorries(
    State(state): State<ControllerState>,
) -> Result<Json<Vec<LorryEntry>>, ListAllLorriesAsJsonError> {
    state
        .db
        .get_all_lorries_info()
        .await
        .map_err(ListAllLorriesAsJsonError::DBError)
        .map(Json::from)
}

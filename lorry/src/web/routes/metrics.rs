//!
//! Endpoint to expose Prometheus metrics.
//!

use crate::state_db::models::{JobStatus, LorryStatus};
use crate::web::ControllerState;
use axum::{extract::State, http::StatusCode, response::IntoResponse};
use prometheus_client::encoding::text::encode;
use prometheus_client::metrics::family::Family;
use prometheus_client::metrics::gauge::Gauge;
use std::collections::HashMap;

pub const PATH: &str = "/1.0/metrics";

/// Metrics divided into buckets based on path names
struct Namespaced(HashMap<String, i64>);

#[derive(thiserror::Error, Debug)]
pub(crate) enum MetricsError {
    #[error("Unable to encode metrics: {0}")]
    EncodeFailed(#[from] std::fmt::Error),

    #[error("Error occured while getting a value from database: {0}")]
    DBError(#[from] sqlx::Error),

    #[error("Error converting value to integer: {0}")]
    ConversionFailed(#[from] std::num::TryFromIntError),
}

impl IntoResponse for MetricsError {
    fn into_response(self) -> axum::response::Response {
        tracing::error!(endpoint =%PATH, "Metrics failed: {self}");
        (StatusCode::INTERNAL_SERVER_ERROR, self).into_response()
    }
}

fn trim_key(input: (&String, &i64)) -> (String, i64) {
    let key = input.0.trim_end_matches('/');
    (key.to_string(), *input.1)
}

fn to_namespaces(
    statuses: Vec<LorryStatus>,
    max_depth: usize,
) -> (Namespaced, Namespaced, Namespaced) {
    let mut successful: HashMap<String, i64> = HashMap::new();
    let mut failed: HashMap<String, i64> = HashMap::new();
    let mut degraded: HashMap<String, i64> = HashMap::new();
    statuses.iter().for_each(|lorry_status| {
        let mut key = String::default();
        let split = lorry_status.path.split('/');
        let n_components = split.clone().count();
        split.enumerate().for_each(|(i, component)| {
            if i == n_components || i > max_depth {
                return;
            };
            key.push_str(&(component.to_owned() + "/"));
            match lorry_status.status {
                JobStatus::None => {}
                JobStatus::Successful => {
                    let current_value = successful.get(key.as_str()).copied().unwrap_or(0);
                    successful.insert(key.to_string(), current_value + 1);
                }
                JobStatus::Failed => {
                    let current_value = failed.get(key.as_str()).copied().unwrap_or(0);
                    failed.insert(key.to_string(), current_value + 1);
                }
                JobStatus::RefsFailed => {
                    let current_value = degraded.get(key.as_str()).copied().unwrap_or(0);
                    degraded.insert(key.to_string(), current_value + 1);
                }
            };
        });
    });
    (
        Namespaced(successful.iter().map(trim_key).collect()),
        Namespaced(failed.iter().map(trim_key).collect()),
        Namespaced(degraded.iter().map(trim_key).collect()),
    )
}

/// Handle a GET request to [PATH]
///
/// Check that all the components of the controller are working properly
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_metrics(
    State(state): State<ControllerState>,
) -> Result<String, MetricsError> {
    let mut body = String::new();

    // FIXME: Do not merge until we add an index to speed up this query
    // FIXME: With 1500 mirrors and 1.7 million jobs this takes about 3s
    let statuses = state.db.get_latest_lorry_statuses().await?;

    // last run state of every configured lorry
    let (successful, failed, degraded) =
        statuses
            .iter()
            .fold(
                (0, 0, 0),
                |(successful, failed, degraded), lorry_status| match lorry_status.status {
                    JobStatus::None => (successful, failed, degraded),
                    JobStatus::Successful => (successful + 1, failed, degraded),
                    JobStatus::Failed => (successful, failed + 1, degraded),
                    JobStatus::RefsFailed => (successful, failed, degraded + 1),
                },
            );

    state.total_lorries.set(statuses.len() as i64);
    state.total_lorries_successful.set(successful.into());
    state.total_lorries_errors.set(failed.into());
    state.total_lorries_degraded.set(degraded.into());

    let (successful, failed, degraded) =
        to_namespaces(statuses, state.app_settings.namespace_depth);

    state.total_lorries_successful_namespaced.clear();
    record_metrics(&successful, state.total_lorries_successful_namespaced);

    state.total_lorries_errors_namespaced.clear();
    record_metrics(&failed, state.total_lorries_errors_namespaced);

    state.total_lorries_degraded_namespaced.clear();
    record_metrics(&degraded, state.total_lorries_degraded_namespaced);

    encode(&mut body, &state.metrics_registry.lock().unwrap())?;

    Ok(body)
}

fn record_metrics(metrics: &Namespaced, family: Family<Vec<(String, String)>, Gauge>) {
    metrics.0.iter().for_each(|(key, count)| {
        family
            .get_or_create(&vec![("namespace".to_owned(), key.to_string())])
            .set(*count);
    });
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn metric_namespacing() {
        let statuses = vec![
            LorryStatus {
                path: String::from("/fuu/bar/baz"),
                status: JobStatus::Successful,
                ..Default::default()
            },
            LorryStatus {
                path: String::from("/fuu/bar/qux"),
                status: JobStatus::Successful,
                ..Default::default()
            },
            LorryStatus {
                path: String::from("/fuu/bar/aaa"),
                status: JobStatus::Successful,
                ..Default::default()
            },
            LorryStatus {
                path: String::from("/fuu/bbb"),
                status: JobStatus::Failed,
                ..Default::default()
            },
        ];

        let (successful, failed, _) = to_namespaces(statuses, 10);
        assert!(*successful.0.get("/fuu").unwrap() == 3);
        assert!(*successful.0.get("/fuu/bar").unwrap() == 3);
        assert!(*successful.0.get("/fuu/bar/baz").unwrap() == 1);
        assert!(*successful.0.get("/fuu/bar/qux").unwrap() == 1);
        assert!(*successful.0.get("/fuu/bar/aaa").unwrap() == 1);
        assert!(*failed.0.get("/fuu").unwrap() == 1);
        assert!(*failed.0.get("/fuu/bbb").unwrap() == 1);
    }
}

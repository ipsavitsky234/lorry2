use crate::state_db::models::Lorry;
use crate::web::ControllerState;
use crate::web::{error::WebError, filters};
use askama::Template;
use axum::{
    extract::{Request, State},
    response::Redirect,
};

#[derive(Template)]
#[template(path = "failed.html")]
pub struct Index<'a> {
    title: &'a str,
    lorries: Vec<Lorry>,
    stylesheet: &'static str,
    logo: &'static str,
}

pub(crate) async fn handle_post_schedule_failed(
    State(state): State<ControllerState>,
    request: Request,
) -> Result<Redirect, WebError> {
    let failed_lorries = state.db.get_all_failed_lorries_info().await?;

    for lorry in failed_lorries {
        tracing::info!("user is requesting priority for {}", &lorry.name);
        let path = crate::comms::LorryPath::new(lorry.name);
        state.db.toggle_priority_run(&path, true).await?;
    }
    let target = request.uri().path();
    Ok(Redirect::to(target))
}

#[axum_macros::debug_handler]
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_failed(
    State(state): State<ControllerState>,
) -> Result<Index<'static>, WebError> {
    let db = state.db.clone();
    let lorries = db.get_all_failed_lorries_info_simple().await?;
    Ok(Index {
        title: "Failed Lorries",
        lorries,
        stylesheet: include_str!("../../../assets/pico.min.css"),
        logo: include_str!("../../../assets/codethink-logo.svg"),
    })
}

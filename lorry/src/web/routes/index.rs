//!
//! The Lorry web UI homepage.
//!

use crate::state_db::models::Lorry;
use crate::web::ControllerState;
use crate::web::{error::WebError, filters};
use askama::Template;
use axum::extract::State;

#[derive(Template)]
#[template(path = "index.html")]
pub struct Index<'a> {
    title: &'a str,
    lorries: Vec<Lorry>,
    stylesheet: &'static str,
    logo: &'static str,
}

///
/// Handle a GET request to `/`
///
#[axum_macros::debug_handler]
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_home(
    State(state): State<ControllerState>,
) -> Result<Index<'static>, WebError> {
    let db = state.db.clone();
    let lorries = db.get_all_lorries_info_simple().await?;
    Ok(Index {
        title: "",
        lorries,
        stylesheet: include_str!("../../../assets/pico.min.css"),
        logo: include_str!("../../../assets/codethink-logo.svg"),
    })
}

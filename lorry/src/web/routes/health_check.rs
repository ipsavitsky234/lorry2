//!
//! Test that the subcomponents of the controller are working properly.
//!
//! Currently this just means checking the DB connection is alive, but should be expanded if
//! more subsystems are added at a later date.
//!

use crate::web::ControllerState;
use axum::{extract::State, http::StatusCode, response::IntoResponse};

/// Path to the health check endpoint
pub const PATH: &str = "/1.0/health-check";

/// Handle a GET request to [PATH]
///
/// Check that all the components of the controller are working properly
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_health_check(
    State(state): State<ControllerState>,
) -> Result<(), HealthError> {
    //Test the DB connection is working
    state
        .db
        .test_connection()
        .await
        .map_err(HealthError::DBConnectionBroken)?;

    Ok(())
}

pub enum HealthError {
    DBConnectionBroken(sqlx::Error),
}

impl IntoResponse for HealthError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            HealthError::DBConnectionBroken(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Problem with database connection: {:?}", e),
            ),
        };
        // TODO: Does it actually make sense to do this, given it will try ot log to DB and... fail?
        tracing::error!(endpoint =%PATH, "Health check failed: {}", err_msg);

        (status, err_msg).into_response()
    }
}

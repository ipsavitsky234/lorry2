//!
//! View a job's history, and request/schedule a re-run.
//!

use crate::state_db::models::Job;
use crate::web::ControllerState;
use crate::web::{error::WebError, filters};
use askama::Template;
use axum::{
    extract::{Path, Request, State},
    response::Redirect,
};

#[derive(Template)]
#[template(path = "jobs.html")]
pub struct Jobs<'a> {
    title: &'a str,
    stylesheet: &'static str,
    logo: &'static str,
    name: String,
    jobs: Vec<Job>,
    this_url: String,
}

/// Handle a GET request to `/jobs/{job-path}`.
///
/// Display the job's history (id, time started, runtime, status, warnings)
pub(crate) async fn handle_get_job_by_name(
    State(state): State<ControllerState>,
    Path(name): Path<String>,
    request: Request,
) -> Result<Jobs<'static>, WebError> {
    let jobs = state.db.get_mirror_jobs(&name).await?;
    let this_url = request.uri().path().to_string();
    Ok(Jobs {
        title: "Job History",
        name: name.clone(),
        stylesheet: include_str!("../../../assets/pico.min.css"),
        logo: include_str!("../../../assets/codethink-logo.svg"),
        jobs,
        this_url,
    })
}

/// Handle a POST request to `/jobs/{job-path}` which schedules a re-run of
/// that job
pub(crate) async fn handle_post_schedule_job_re_run_by_path(
    State(state): State<ControllerState>,
    Path(name): Path<String>,
    request: Request,
) -> Result<Redirect, WebError> {
    let path = crate::comms::LorryPath::new(name.clone());
    tracing::info!("user is requesting priority for {}", name);
    state.db.toggle_priority_run(&path, true).await?;
    let target = request.uri().path();
    Ok(Redirect::to(target))
}

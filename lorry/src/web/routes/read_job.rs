//!
//! Get info about a specific job by it's ID.
//!

use crate::state_db::models::JobStatus;
use crate::web::ControllerState;
use crate::web::{error::WebError, filters};
use askama::Template;
use axum::extract::{Path, State};

/// Return a nicely formatted ref result with only the part at the end.
///
/// Example original line:
///
/// ```text
/// refs/tags/v2.0.0-rc.1:refs/tags/v2.0.0-rc.1     [up to date]
/// output: [up to date]
/// ```
fn clean_message(msg: &str) -> String {
    let mut cleaned = String::default();
    let mut split = msg.split('\t');
    split.next();
    split.next();
    split.for_each(|chunk| cleaned.push_str(chunk));
    cleaned
}

#[derive(Template)]
#[template(path = "job.html")]
pub struct Job {
    title: String,
    job_id: i64,
    output: Option<String>,
    exit_code: Option<i64>,
    started: i64,
    ended: Option<i64>,
    path: String,
    status: JobStatus,
    stylesheet: &'static str,
    logo: &'static str,
    n_refs: usize,
    refs: Vec<(String, bool, String)>,
    n_warnings: usize,
    warnings: Vec<(i64, String, String)>,
}

/// Handle a GET request to `/job/{job-id}`.
///
/// Display info about a specifc run of a job:
/// * ID
/// * Starttime
/// * Path
/// * Exit code
/// * Warnings
/// * Refs
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_job_by_id(
    State(state): State<ControllerState>,
    Path(job_id): Path<i64>,
) -> Result<Job, WebError> {
    let job = state
        .db
        .get_job_by_id(job_id)
        .await?
        .ok_or(WebError::NotFound(format!("{}", job_id)))?;
    let refs = state.db.get_job_refs(job.id).await?;
    let warnings = state.db.get_job_warnings(job.id).await?;
    Ok(Job {
        title: String::from("Job Status"),
        output: job.output.and_then(|message| {
            if message.is_empty() {
                None
            } else {
                Some(message.clone())
            }
        }),
        job_id: job.id,
        exit_code: job.exit_code,
        started: job.started,
        ended: job.ended,
        path: job.path.clone(),
        status: job.status,
        stylesheet: include_str!("../../../assets/pico.min.css"),
        logo: include_str!("../../../assets/codethink-logo.svg"),
        n_refs: refs.len(),
        refs: refs
            .iter()
            .map(|r| (r.name.clone(), r.successful, clean_message(&r.message)))
            .collect(),
        n_warnings: warnings.len(),
        warnings: warnings
            .iter()
            .map(|r| (r.id, r.warning.name(), r.warning.message()))
            .collect(),
    })
}

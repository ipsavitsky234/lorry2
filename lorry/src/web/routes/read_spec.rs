//!
//! View the spec (config) for a specific job.
//!
//! Get by the job's path.
//!

use crate::read_config::{resolve, Config};
use crate::web::error::WebError;
use crate::web::ControllerState;
use askama::Template;
use axum::extract::{Path, State};
use serde_yaml::to_string;

#[derive(Template)]
#[template(path = "spec.html")]
pub struct Spec {
    title: String,
    spec: String,
    stylesheet: &'static str,
    logo: &'static str,
}

#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_spec_by_name(
    State(state): State<ControllerState>,
    Path(name): Path<String>,
) -> Result<Spec, WebError> {
    let configs = resolve(&state.app_settings.configuration_directory)
        .map_err(|e| WebError::Message(e.to_string()))?;
    for config in configs {
        let Config::Lorries(cfg) = config.0;
        if name.starts_with(&cfg.prefix) {
            let remainder = name.replace(&cfg.prefix, "");
            let remainder = remainder.trim_start_matches('/').to_string();
            if let Some(cfg) = config.1.get(&remainder) {
                let spec = to_string(cfg)?;
                return Ok(Spec {
                    title: format!("Job Spec: {}", name),
                    stylesheet: include_str!("../../../assets/pico.min.css"),
                    logo: include_str!("../../../assets/codethink-logo.svg"),
                    spec,
                });
            }
        }
    }
    Err(WebError::Message(format!(
        "cannot resolve spec for {}",
        name
    )))
}

//!
//! Web errors ([WebError] enum and associated [IntoResponse] implementation).
//!

use axum::{http::StatusCode, response::IntoResponse};

#[derive(thiserror::Error, Debug)]
pub(crate) enum WebError {
    #[error("Error occured while contacting the database: {0}")]
    DBError(#[from] sqlx::Error),

    #[error("An error occurred while processing the request: {0}")]
    Message(String),

    #[error("Resource not found: {0}")]
    NotFound(String),

    #[error("Serialization error: {0}")]
    Serialization(#[from] serde_yaml::Error),
}

impl IntoResponse for WebError {
    fn into_response(self) -> axum::response::Response {
        match self {
            WebError::DBError(e) => {
                (StatusCode::INTERNAL_SERVER_ERROR, e.to_string()).into_response()
            }
            WebError::Message(e) => (StatusCode::INTERNAL_SERVER_ERROR, e).into_response(),
            WebError::NotFound(resource) => (StatusCode::NOT_FOUND, resource).into_response(),
            WebError::Serialization(e) => {
                (StatusCode::INTERNAL_SERVER_ERROR, e.to_string()).into_response()
            }
        }
    }
}

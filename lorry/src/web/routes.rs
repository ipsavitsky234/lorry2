//!
//! Handle all requests from the Lorry web UI.
//!
//! Uses the [axum] crate.
//!

pub mod debug;
pub mod health_check;
pub mod index;
pub mod list_jobs;
pub mod list_lorries;
pub mod metrics;
pub mod purge_repo;
pub mod read_config;
pub mod read_failed;
pub mod read_job;
pub mod read_jobs;
pub mod read_spec;

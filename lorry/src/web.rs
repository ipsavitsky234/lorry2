//!
//! The Lorry web UI
//!

use axum::{
    routing::{get, post},
    Router,
};
use prometheus_client::{metrics::family::Family, metrics::gauge::Gauge, registry::Registry};
use std::{path::PathBuf, sync::Arc, sync::Mutex};

pub mod error;
pub mod filters;
pub mod routes;

use crate::downstream::Downstream;
use crate::scheduler::Tracker;
use crate::state_db::StateDatabase;

/// Global controller state object used across the web application
#[derive(Clone)]
pub struct ControllerState {
    db: Arc<StateDatabase>,
    app_settings: ControllerSettings,
    tracker: Tracker,
    metrics_registry: Arc<Mutex<Registry>>,
    total_lorries: Gauge,
    total_lorries_errors: Gauge,
    total_lorries_successful: Gauge,
    total_lorries_degraded: Gauge,

    // Same as the lorries above but split into namespaces to allow custom
    // alert rules for specific "groups".
    total_lorries_errors_namespaced: Family<Vec<(String, String)>, Gauge>,

    total_lorries_successful_namespaced: Family<Vec<(String, String)>, Gauge>,
    total_lorries_degraded_namespaced: Family<Vec<(String, String)>, Gauge>,
}

/// Concatenation of the major sub-components of the controller.
pub struct ControllerComponents {
    /// The server that listens on endpoints to manage minion instances.
    pub router: Router,
}

/// Settings for the creating the lorry controller server.
///
/// Note this is not a configuration of the mirrors; rather, it is the data
/// necessary to fetch the mirroring configuration later.
#[derive(Clone, Debug)]
pub struct ControllerSettings {
    /// Representation of some connection to the host of the downstream git
    /// mirrors. This could be git forge instance, a local filesystem, or
    /// something else.
    ///
    /// This is responsible for creating the downstream git mirrors for worker
    /// instances to mirror to.
    ///
    // TODO as it stands, we open a session (say, of a gitlab connection) and
    // hold it while the program runs, rather than only opening and closing
    // when needed. Is this the best practice to follow?
    pub downstream: Downstream,

    /// When the server updates its configuration, it does so by mirroring the
    /// config repository into this folder.
    pub configuration_directory: PathBuf,

    /// If provided the configuration will be cloned from the URL into the
    /// configuration_directory otherwise lorry configuration will be read
    /// directly from the configuration_directory.
    pub confgit_url: Option<String>,

    /// Inspect the branch with this name when pulling in `confgit_url` for
    /// configuration files.
    pub confgit_branch: String,

    /// Hostname of this system as resolved at startup
    pub hostname: String,

    /// The currently running process ID
    pub current_pid: u32,

    /// Depth at which to expose namespaced metrics
    pub namespace_depth: usize,
}

/// Pull together everything that's needed to run the Lorry web UI
///
/// * Setup metrics registries
/// * Initialise the state ([ControllerState])
/// * Setup the router (paths and their handlers)
pub async fn app(
    pool: Arc<StateDatabase>,
    app_settings: ControllerSettings,
    tracker: Tracker,
) -> ControllerComponents {
    let total_lorries: Gauge = Gauge::default();
    let total_lorries_errors: Gauge = Gauge::default();
    let total_lorries_successful: Gauge = Gauge::default();
    let total_lorries_degraded: Gauge = Gauge::default();

    let total_lorries_errors_namespaced = Family::<Vec<(String, String)>, Gauge>::default();
    let total_lorries_successful_namespaced = Family::<Vec<(String, String)>, Gauge>::default();
    let total_lorries_degraded_namespaced = Family::<Vec<(String, String)>, Gauge>::default();

    let mut metrics_registry: Registry = Registry::default();

    metrics_registry.register(
        "lorry2_total_lorries",
        "The total amount of lorries",
        total_lorries.clone(),
    );

    metrics_registry.register(
        "lorry2_total_lorries_degraded",
        "The total amount of lorries partially failed",
        total_lorries_degraded.clone(),
    );

    metrics_registry.register(
        "lorry2_total_lorries_errors",
        "The total amount of lorries in a failed state",
        total_lorries_errors.clone(),
    );

    metrics_registry.register(
        "lorry2_total_lorries_successful",
        "The total amount of successful mirrors",
        total_lorries_successful.clone(),
    );

    metrics_registry.register(
        "lorry2_total_lorries_degraded_namespaced",
        "The total amount of lorries partially failed",
        total_lorries_degraded_namespaced.clone(),
    );

    metrics_registry.register(
        "lorry2_total_lorries_errors_namespaced",
        "The total amount of lorries in a failed state",
        total_lorries_errors_namespaced.clone(),
    );

    metrics_registry.register(
        "lorry2_total_lorries_successful_namespaced",
        "The total amount of successful mirrors",
        total_lorries_successful_namespaced.clone(),
    );

    let state = ControllerState {
        db: pool,
        app_settings,
        tracker,
        metrics_registry: Arc::new(Mutex::new(metrics_registry)),
        total_lorries,
        total_lorries_errors,
        total_lorries_successful,
        total_lorries_degraded,
        total_lorries_errors_namespaced,
        total_lorries_successful_namespaced,
        total_lorries_degraded_namespaced,
    };

    // TODO: Consider making get function names more explicit
    let router = Router::new()
        .route("/", get(routes::index::handle_get_home))
        .route("/config", get(routes::read_config::handle_get_config))
        .route("/debug", get(routes::debug::handle_get_debug))
        .route("/failed", get(routes::read_failed::handle_get_failed))
        .route(
            "/failed",
            post(routes::read_failed::handle_post_schedule_failed),
        )
        .route(
            "/spec/*name",
            get(routes::read_spec::handle_get_spec_by_name),
        )
        .route(
            "/jobs/*name",
            get(routes::read_jobs::handle_get_job_by_name),
        )
        .route(
            "/jobs/*name",
            post(routes::read_jobs::handle_post_schedule_job_re_run_by_path),
        )
        .route("/job/:job_id", get(routes::read_job::handle_get_job_by_id))
        .route(
            routes::list_lorries::PATH,
            get(routes::list_lorries::handle_get_list_all_lorries),
        )
        .route(
            routes::list_jobs::PATH,
            get(routes::list_jobs::handle_get_list_all_jobs),
        )
        .route(
            routes::purge_repo::PATH,
            post(routes::purge_repo::handle_post_purge_repo),
        )
        .route(
            routes::health_check::PATH,
            get(routes::health_check::handle_get_health_check),
        )
        .route(
            routes::metrics::PATH,
            get(routes::metrics::handle_get_metrics),
        )
        .with_state(state);

    ControllerComponents { router }
}

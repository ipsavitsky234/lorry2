use crate::comms::{Job, JobId};
use crate::executor::{Executor, Params as ExecutorParams};
use crate::scheduler::CHANNEL_TIMEOUT_MS;
use crate::state_db::LorryEntry;
use crate::url_builder::UrlBuilder;
use std::error::Error as StdError;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::mpsc::{channel, Sender};
use tokio::sync::Mutex;
use tokio::task::spawn;
use tokio::time::{sleep, Instant};
use work_queue::LocalQueue;
use workerlib::{Error, MirrorStatus};

/// State of a given thread, can only be busy or idle.
#[derive(Clone, Debug)]
pub enum State {
    /// JobId the thread is currently processing
    Busy((String, JobId)),

    Idle,
}

/// A simple numeric identifier for tracking active threads.
#[derive(Clone, Debug, Hash, Copy, Eq, PartialEq, Default)]
pub struct ThreadId(pub usize);

/// Work stealing local job queue
pub type LocalJobQueue = LocalQueue<JobReady>;

pub struct JobReady(pub LorryEntry, pub Sender<JobAccepted>);

#[allow(dead_code)]
pub struct JobAccepted(pub ThreadId, pub LorryEntry, pub Sender<Job>);

/// A message to indicate the status of a particular job as returned from a
/// worker thread.
pub struct JobResult(pub ThreadId, pub Job, pub Result<MirrorStatus, Error>);

/// Runner is responsible for executing mirroring operations from within a
/// worker thread.
#[async_trait::async_trait]
pub trait Worker: Send + Sync {
    /// Run the Lorry job returning any failure that happened during the
    /// mirroring operation.
    async fn work(self, queue: LocalJobQueue) -> Result<(), Box<dyn StdError>>;
}

/// Worker is a thread bound to a channel that is responsible for running
/// Lorry operations. All operations have a timeout and when that timeout is
/// exceeded the underlying operations are killed. When all threads are busy
/// jobs are added to queue and scheduled when a thread becomes available.
/// Workers only communicate by passing messages and have no access to the
/// underlying data store.
pub struct DefaultWorker<E>
where
    E: Executor + Clone + Send + Sync + 'static,
{
    /// Unique thread ID
    pub thread_id: ThreadId,

    /// Static runner implementation, typically just DefaultRunner
    pub runner: Arc<Mutex<Box<E>>>,

    /// Settings passed into workerlib
    pub mirror_settings: workerlib::Arguments,

    /// Used to construct URLs that pass into workerlib
    pub url_builder: UrlBuilder,

    /// Interval used to poll for jobs from the queue within the worker thread
    pub polling_interval: Duration,

    /// Channel to send results from the worker back to the scheduler
    pub result_ch: Sender<JobResult>,
}

#[async_trait::async_trait]
impl<E> Worker for DefaultWorker<E>
where
    E: Executor + Clone + Send + Sync + 'static,
{
    async fn work(self, mut queue: LocalJobQueue) -> Result<(), Box<dyn StdError>> {
        loop {
            if let Some(message) = queue.pop() {
                // Global timeout where if it is exceeded we will kill the
                // underlying job.
                //
                // TODO: individual mirrors should be able to specify this too.
                let timeout = Duration::from_secs(message.0.lorry_timeout.0 as u64);
                let now = Instant::now();
                let (tx, mut rx) = channel::<Job>(1);
                message
                    .1
                    .send_timeout(
                        JobAccepted(self.thread_id, message.0.clone(), tx),
                        Duration::from_millis(CHANNEL_TIMEOUT_MS),
                    )
                    .await
                    .unwrap();

                let job = rx.recv().await.unwrap();
                let thread_id = self.thread_id;
                let working_area = self.mirror_settings.working_area.clone();
                let target_url = self
                    .url_builder
                    .build(&job.mirror_server_base_url, job.insecure);

                let mirror_settings = self.mirror_settings.clone();
                let worker_job = job.clone();
                let result_ch = self.result_ch.clone();
                let runner = self.runner.clone();

                let mirror_path = worker_job.path.to_string();

                let handle = spawn(async move {
                    let runner = runner.lock().await;

                    match runner
                        .run(&ExecutorParams {
                            working_area: working_area.as_path(),
                            job: &worker_job,
                            target_url: &target_url,
                            mirror_settings: &mirror_settings,
                        })
                        .await
                    {
                        Ok(status) => {
                            tracing::info!("mirroring operation for {:?}, completed", mirror_path);
                            result_ch
                                .send_timeout(
                                    JobResult(thread_id, worker_job, Ok(status)),
                                    Duration::from_secs(CHANNEL_TIMEOUT_MS),
                                )
                                .await
                                .unwrap();
                        }
                        Err(err) => {
                            match err {
                                Error::SomeRefspecsFailed { refs: _, n_failed } => {
                                    tracing::info!(
                                        "Mirroring operation for {:?} as {} failed refs",
                                        mirror_path,
                                        n_failed
                                    );
                                }
                                _ => {
                                    tracing::info!(
                                        "Mirroring operation for {:?} has failed: {:?}",
                                        mirror_path,
                                        err
                                    );
                                }
                            }

                            result_ch
                                .send_timeout(
                                    JobResult(thread_id, worker_job, Err(err)),
                                    Duration::from_secs(CHANNEL_TIMEOUT_MS),
                                )
                                .await
                                .unwrap();
                        }
                    };
                });
                'handle_loop: while !handle.is_finished() {
                    if now.elapsed() >= timeout {
                        tracing::warn!(
                            "job has exceeded its allocated runtime [{:?}], terminating",
                            timeout
                        );
                        handle.abort();
                        self.result_ch
                            .send_timeout(
                                JobResult(
                                    thread_id,
                                    job,
                                    Err(Error::TimeoutExceeded {
                                        seconds: now.elapsed().as_secs(),
                                    }),
                                ),
                                Duration::from_secs(CHANNEL_TIMEOUT_MS),
                            )
                            .await
                            .unwrap();
                        break 'handle_loop;
                    };
                    let elapsed = now.elapsed();
                    let elapsed_seconds = elapsed.as_secs();
                    if (elapsed_seconds as f32 / 60.0) % 1.0 == 0.0 && elapsed_seconds > 0 {
                        tracing::info!(
                            "Thread {:?} [{}] has been active for {:?}",
                            self.thread_id,
                            message.0.path.to_string(),
                            elapsed,
                        );
                    }
                    sleep(self.polling_interval).await;
                }
            } else {
                sleep(self.polling_interval).await;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::comms::{Interval, JobExitStatus, LorryPath, TimeStamp};
    use async_trait::async_trait;
    use tokio::sync::mpsc::channel;
    use url::Url;
    use work_queue::Queue;
    use workerlib::{lorry_specs::SingleLorry, LorrySpec};

    #[derive(Clone)]
    struct NoopRunner;

    #[async_trait]
    impl Executor for NoopRunner {
        async fn run(&self, _params: &ExecutorParams) -> Result<MirrorStatus, Error> {
            Ok(MirrorStatus::default())
        }
    }

    #[tokio::test]
    async fn test_worker_single_op() {
        let queue = Queue::<JobReady>::new(1, 1);

        let (tx, mut rx) = channel::<JobAccepted>(1);
        let (result_tx, mut result_rx) = channel::<JobResult>(32);

        let worker = DefaultWorker {
            thread_id: ThreadId(0),
            runner: Arc::new(Mutex::new(Box::new(NoopRunner {}))),
            polling_interval: Duration::from_millis(300),
            url_builder: UrlBuilder::default(),
            mirror_settings: workerlib::Arguments::default(),
            result_ch: result_tx,
        };

        let local_job_queue = queue.local_queues().next().unwrap();

        spawn(async move {
            worker.work(local_job_queue).await.unwrap();
        });

        // TODO: implement defaults
        queue.push(JobReady(
            LorryEntry {
                path: LorryPath::new(String::new()),
                name: String::new(),
                spec: LorrySpec::Git(SingleLorry::default()),
                running_job: None,
                last_run: TimeStamp(0),
                last_attempted: TimeStamp(0),
                interval: Interval(0),
                lorry_timeout: Interval(10), // 10s timeout
                last_run_results: JobExitStatus::FinishedExitCode(-1),
                last_run_error: None,
                purge_from_before: TimeStamp(0),
                priority: false,
            },
            tx,
        ));

        let accepted = rx.recv().await.unwrap();
        assert!(accepted.0 .0 == 0);
        let start_ch = accepted.2;
        start_ch
            .send(Job {
                lorry_name: String::from("test lorry"),
                lorry_spec: LorrySpec::Git(SingleLorry::default()),
                id: JobId(0),
                path: LorryPath::new(String::new()),
                purge_cutoff: TimeStamp(0),
                insecure: None,
                mirror_server_base_url: Url::parse("http://localhost:8080").unwrap(),
            })
            .await
            .unwrap();

        let result = result_rx.recv().await.unwrap();
        assert!(result.0 .0 == 0);
        assert!(result.1.lorry_name == "test lorry");
        assert!(result.2.is_ok());
    }
}

use ansi_term::Colour::{Green, Red, Yellow};
use axum::serve;
use clap::{arg, Parser};
use scheduler::ShutdownHandle;
use std::error::Error;
use std::panic::set_hook;
use std::{net::SocketAddr, path::PathBuf};
use tokio::net::TcpListener;
use tokio::signal::unix::{signal, SignalKind};
use tracing::Level;
use url::Url;
use web::ControllerSettings;

mod comms;
mod config;
mod downstream;
mod executor;
mod give_job;
mod linter;
mod read_config;
mod scheduler;
mod state_db;
mod url_builder;
mod web;
mod worker;

use crate::config::load;
use crate::downstream::DownstreamSetup;
use crate::executor::DefaultExecutor;
use crate::give_job::{DBJobAllocator, DBRecorder};
use crate::read_config::DBConfigReader;
use crate::state_db::StateDatabase;

/// The default behavior of Lorry is to immediately shutdown anytime a thread
/// panics for any reason.
const FAIL_FAST: bool = true;

/// Lorry Controller
#[derive(Parser)]
struct ControllerCommandLineArguments {
    /// Path to your configuration file
    #[arg(long = "config")]
    config_file: Option<PathBuf>,

    /// Logging level
    #[arg(long = "level")]
    pub log_level: Option<Level>,

    /// Poll the configuration and update the database for scheduling
    #[arg(long = "poll-config", default_value = "true")]
    pub poll_config: Option<bool>,

    /// Process jobs from the database
    #[arg(long = "process-jobs", default_value = "true")]
    pub process_jobs: Option<bool>,

    /// Enable the http interface
    #[arg(long = "enable-http", default_value = "true")]
    pub enable_http: Option<bool>,

    /// Validate a Lorry configuration file
    #[arg(long = "validate")]
    validate: Option<PathBuf>,

    /// Allow duplicate mirrors and raw files in config validation
    #[arg(long = "allow-duplicates", default_value = "false")]
    validate_allow_duplicates: bool,
}

/// The `hostname` parameter in the config can contain only a name and a port
/// number. This checks to make sure no scheme was specified and then converts
/// it to a Url object with `https` as the default scheme.
// It would be nice in the future to check no credentials or path was provided
// either.
fn parse_hostname(hostname: &str, insecure: bool) -> Result<Url, String> {
    // Confirm that no scheme has been specified in hostname
    if let Ok(tmp_url) = Url::parse(hostname) {
        if tmp_url.host().is_some() {
            return Err(format!("It looks like you've specified a scheme in `hostname` in the controller config - please do not!: {hostname}"));
        }
    }

    // Use https as default here. Swapping out for http or ssh is handled in
    // `distributed::minion::run_job` (at time of writing)
    if insecure {
        eprintln!("WARNING: communicating over plain text, do not use this in production");
        Url::parse(&format!("http:{}", hostname))
            .map_err(|e| format!("failed to parse hostname {hostname}: {e}"))
    } else {
        Url::parse(&format!("https:{}", hostname))
            .map_err(|e| format!("failed to parse hostname {hostname}: {e}"))
    }
}

/// Initialize the configuration at start, if the configuration is not
/// available it will crash the server immediately. Subsequent updates are
/// handled by the ConfigPoller.
async fn init_config(
    db: &StateDatabase,
    settings: &ControllerSettings,
) -> Result<(), Box<dyn Error>> {
    read_config::read_config(db, settings).await?;
    // Purge all jobs recorded as running since the scheduler is only now
    // initializing and no jobs can be running.
    let n_rows = db.reset().await?;
    if n_rows > 0 {
        tracing::info!("Reset {} lorries", n_rows);
    }
    Ok(())
}

async fn graceful_shutdown(shutdown_func: ShutdownHandle) {
    let mut interrupt_future =
        signal(SignalKind::interrupt()).expect("Failed to setup interrupt signal");
    let mut terminate_future =
        signal(SignalKind::terminate()).expect("Failed to setup terminate signal");

    tokio::select! {
        _ = interrupt_future.recv() => {},
        _ = terminate_future.recv() => {},
    }

    tracing::info!("caught interrupt, shutting down lorry, peacefully.");
    shutdown_func.shutdown().await;
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = ControllerCommandLineArguments::parse();

    std::panic::set_hook(Box::new(|info| {
        eprintln!("=====💣=¡Don't panic, Lorry has panicked!=💣=====");
        eprintln!("{}", utils::redact::redact(info));
        eprintln!("=================================================");
    }));

    if FAIL_FAST {
        let default_handler = std::panic::take_hook();
        set_hook(Box::new(move |info| {
            default_handler(info);
            std::process::exit(1);
        }));
    }

    if let Some(validate_test_dir) = args.validate {
        let linter = crate::linter::Linter::new(&validate_test_dir);
        match linter.lint() {
            Ok(_) => {
                let _ = linter.stats().map(|stats| println!("{}", stats));
            }
            Err(errors) => {
                if errors.iter().any(|e| match e {
                    linter::LinterError::Config(_) => {
                        eprintln!("{}:\n{}", Red.paint("[ERROR]"), e);
                        true
                    }
                    linter::LinterError::DuplicateRawFiles(url, _) => {
                        if args.validate_allow_duplicates {
                            eprintln!("{}: Duplicate Raw File: {}", Yellow.paint("[WARN]"), url);
                            false
                        } else {
                            eprintln!("{}: Duplicate Raw File: {}", Red.paint("[ERROR]"), url);
                            true
                        }
                    }
                    linter::LinterError::DuplicateGitMirrors(url, _) => {
                        if args.validate_allow_duplicates {
                            eprintln!("{}: Duplicate Git Mirror: {}", Yellow.paint("[WARN]"), url);
                            false
                        } else {
                            eprintln!("{}: Duplicate Git Mirror: {}", Red.paint("[ERROR]"), url);
                            true
                        }
                    }
                }) {
                    std::process::exit(1)
                } else {
                    let _ = linter.stats().map(|stats| println!("{}", stats));
                }
            }
        };
        eprintln!("{}", Green.paint("[OK]"));
        std::process::exit(0)
    }

    let config_path = args.config_file.unwrap_or("controller.conf".into());
    let config = match load(config_path.as_path()) {
        Ok(config) => config,
        Err(err) => {
            panic!("failed to load controller config:\n{}", err);
        }
    };

    // initialize the global git config
    workerlib::git_config::Config(config.git_config_path.clone()).setup(
        &config.email,
        config.n_threads as i64,
        config.insecure,
        &config.askpass_program,
        config.git_credentials_file.as_deref(),
    )?;

    let formatter = tracing_subscriber::fmt::format::debug_fn(|writer, field, value| {
        let field_str = format!("{:?}", value);
        write!(writer, "{}: {}", field, utils::redact::redact(&field_str))
    });

    tracing_subscriber::fmt()
        .compact()
        .with_line_number(true)
        .with_level(true)
        .with_thread_ids(true)
        .fmt_fields(formatter)
        .with_max_level(args.log_level.unwrap_or(config.log_level))
        .init();

    tracing::info!("Logger initialized");

    state_db::init_and_migrate(&config.state_db).await?;

    let db = StateDatabase::connect(&config.state_db, false).await?;

    tracing::info!("Database initialized");

    let hostname = match parse_hostname(&config.hostname, config.insecure) {
        Ok(hostname) => hostname,
        Err(err) => panic!("failed to load controller config:\n{err}"),
    };

    // only gitlab is currently supported
    let downstream =
        async_convert::TryInto::<downstream::Downstream>::try_into(DownstreamSetup::Gitlab {
            hostname,
            downstream_visibility: config.visibility,
            private_token: config.gitlab_private_token.clone(),
            token_file: config.gitlab_private_token_file.clone(),
            insecure: config.insecure,
        })
        .await?;

    // lookup the hostname of this system
    let resolved_hostname = hostname::get()?.into_string().unwrap();
    tracing::info!("Initializing Lorry controller {}", resolved_hostname);

    let settings = web::ControllerSettings {
        downstream,
        configuration_directory: config.configuration_directory,
        confgit_url: config.confgit_url,
        confgit_branch: config.confgit_branch,
        hostname: resolved_hostname,
        current_pid: std::process::id(),
        namespace_depth: config.namespace_depth,
    };
    let addr = SocketAddr::from(([0, 0, 0, 0], config.port));
    let mut workerlib_args = workerlib::Arguments {
        pack_threads: Some(config.clone.n_threads),
        use_git_binary: matches!(config.clone.engine, config::CloneEngine::GitBinary).into(),
        git_config_path: config.git_config_path,
        sha256sum_required: config.sha256sums_required.is_some_and(|required| required),
        ..Default::default()
    };
    workerlib_args.working_area.clone_from(&config.working_area);
    if let Some(maximum_redirects) = config.maximum_redirects {
        workerlib_args.maximum_redirects = maximum_redirects
    };
    workerlib_args.check_ssl_certificates = !config.insecure;

    let mut tracker: Option<scheduler::Tracker> = None;
    let mut shutdown: Option<ShutdownHandle> = None;

    if args.poll_config.is_some_and(|poll| poll)
        && (args.process_jobs.is_none() || args.process_jobs.is_some_and(|process| !process))
    {
        tracing::info!("Running standalone config poller");
        init_config(&db, &settings).await?;
        let db = db.clone();
        let settings = settings.clone();
        tokio::task::spawn(async move {
            let reader = scheduler::IntervalConfigReader {
                config_reader: scheduler::component(DBConfigReader(db, settings)),
                interval: config.confgit_update_period.into(),
            };
            reader.read().await.expect("Error refreshing configuration");
        });
    } else if args.process_jobs.is_some_and(|process| process) {
        let mut config_reader: Option<DBConfigReader> = None;
        if args.poll_config.is_some_and(|poll| poll) {
            tracing::info!("Launching config poller with multi-threaded worker");
            init_config(&db, &settings).await?;
            config_reader = Some(DBConfigReader(db.clone(), settings.clone()));
        } else {
            tracing::info!("Launching standalone multi-threaded worker");
        };
        let scheduler = scheduler::Builder::default()
            .username(&config.username.unwrap())
            .hostname(&config.hostname)
            .private_token(config.gitlab_private_token)
            .token_file(config.gitlab_private_token_file)
            .threads(config.n_threads)
            .config_reader_interval(config.confgit_update_period.into())
            .build(
                &workerlib_args,
                DBJobAllocator(db.clone(), settings.clone()),
                DBRecorder(db.clone()),
                config_reader,
                DefaultExecutor {},
            );
        let (_shutdown, _tracker) = scheduler.schedule_all().await?;
        shutdown = Some(_shutdown);
        tracker = Some(_tracker);
    };

    if args.enable_http.is_some_and(|http| http) {
        tracing::info!("Launching http server listening on port {}", config.port);
        let app = web::app(
            db.into(),
            settings.clone(),
            tracker.unwrap_or(scheduler::tracker()),
        )
        .await;

        let listener = TcpListener::bind(addr).await?;

        serve(listener, app.router)
            .with_graceful_shutdown(graceful_shutdown(shutdown.unwrap()))
            .await
            .expect("Failed to run server..");

        Ok(())
    } else {
        loop {
            // FIXME FIXME FIXME
            tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        }
    }
}

mod tests {
    #[test]
    fn test_parse_hostname() {
        assert!(super::parse_hostname("https://archlinux.org/", false).is_err());
        assert!(super::parse_hostname("archlinux.org/", false).is_ok());
        assert!(super::parse_hostname("archlinux.org:909", false).is_ok());

        assert!(super::parse_hostname("name:secret@archlinux.org:909", false).is_ok());
        assert!(super::parse_hostname("archlinux.org:909/hello", false).is_ok());
    }
}

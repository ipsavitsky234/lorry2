use crate::scheduler::ConfigReader;
use crate::{state_db::StateDatabase, web::ControllerSettings};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::error::Error as StdError;
use std::{
    fmt::Write,
    fs::read_to_string,
    path::{Path, PathBuf},
};
use tracing::error;
use utils::command_wrapper::CommandBuilder;
use workerlib::lorry_specs::{extract_lorry_specs, LorrySpec};

const CONFIG_FILENAME: &str = "lorry-controller.conf";

/// Rresolved configuration
pub type Resolved = Vec<(Config, BTreeMap<String, LorrySpec>)>;

#[derive(thiserror::Error, Debug)]
pub enum ReadConfigError {
    #[error("An error occurred when trying to mirror the upstream configuration git repo")]
    FailedToFetchConfgit(Box<dyn std::error::Error + Send + Sync>),

    #[error("An error occurred removing unneeded lorries from config")]
    FailedToRemoveLorries(sqlx::Error),

    #[error("Glob Error: {0}")]
    Glob(#[from] glob::GlobError),

    #[error("Glob Pattern Invalid: {0}")]
    GlobPattern(#[from] glob::PatternError),

    #[error("IO error for file {0:?}: {1}")]
    IOError(PathBuf, std::io::Error),

    #[error("Failed to parse specs in {0}: {1}")]
    SpecParse(PathBuf, serde_yaml::Error),

    #[error("Failed to read config:\n{}", errors.iter().fold(String::new(), |mut acc, e| {let _ = writeln!(acc, "{e}"); acc}))]
    MultipleReadFails { errors: Vec<ReadConfigError> },

    #[error("DB transaction failed: {0}")]
    DBOperationFailed(#[from] sqlx::Error),
}

// TODO verify that we actually use other types, becuase looking at gnome it seems we don't
/// Configuration block contained in `lorry-controller.conf`, specifying a set of mirrors
/// and information about how to run them
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum Config {
    #[serde(rename = "lorries")]
    Lorries(LorryConfig),
}

/// Configuration block corresponding to a set of lorry mirrors.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct LorryConfig {
    // TODO give default value of 1 day
    pub interval: super::comms::Interval,

    pub prefix: String,
    pub globs: Vec<String>,

    // TODO give default value of 1 day
    // TODO is this value even used anywhere?
    // If not, we should drop it from the DB too
    pub timeout: super::comms::Interval,

    pub lfs: Option<bool>,
}

/// ConfigPoller waits for new configuration to be loaded from the remote source
/// and then populates the database accordingly.
pub struct DBConfigReader(pub StateDatabase, pub ControllerSettings);

#[async_trait]
impl ConfigReader for DBConfigReader {
    async fn read(&self) -> Result<(), Box<dyn StdError>> {
        // TODO: When not using a remote git repository for configuration inotify
        // style file watching should be used instead of polling.
        read_config(&self.0, &self.1).await?;
        Ok(())
    }
}

/// Pull configuration from the remote configuration repository and update configuration based on it.
#[tracing::instrument(skip_all)]
pub async fn read_config(
    db: &StateDatabase,
    app_settings: &ControllerSettings,
) -> Result<(), ReadConfigError> {
    get_confgit(app_settings)
        .await
        .map_err(ReadConfigError::FailedToFetchConfgit)?;
    let config_obj = read_config_file(&app_settings.configuration_directory)?;

    let lorries_to_remove = db.get_lorries_paths().await?;
    let mut m = std::collections::HashSet::new();
    for i in lorries_to_remove {
        m.insert(i);
    }
    let mut lorries_to_remove = m;

    for section in config_obj {
        let Config::Lorries(l) = section;
        let added = add_matching_lorries_to_statedb(db, l, app_settings).await?;
        lorries_to_remove = &lorries_to_remove - &added;
    }

    futures::future::join_all(
        lorries_to_remove
            .iter()
            .map(|path| async { db.remove_lorry(path).await }),
    )
    .await
    .into_iter()
    .collect::<Result<Vec<_>, _>>()
    .map_err(ReadConfigError::FailedToRemoveLorries)?;

    Ok(())
}

#[tracing::instrument(skip_all)]
async fn add_matching_lorries_to_statedb(
    db: &StateDatabase,
    l: LorryConfig,
    app_settings: &ControllerSettings,
) -> Result<std::collections::HashSet<super::comms::LorryPath>, ReadConfigError> {
    let (filenames, errors): (Vec<_>, Vec<_>) = l
        .globs
        .into_iter()
        .map(|i| {
            let l = app_settings
                .configuration_directory
                .join(CONFIG_FILENAME)
                .parent()
                .expect("We should have a parent, we JUST APPENDED SOMETHING TO IT")
                .join(i); //This may seem a bit circuitous; the rationale is that CONFIG_FILENAME may have multiple components, so the call to parent() won't necessarily just pop it off

            tracing::debug!("path= {:?}", &l);
            l
        })
        .map(|s| glob::glob(s.to_str().expect("Path cannot be globbed over...")))
        .flat_map(|s| s.expect("globbing failed"))
        .partition(Result::is_ok);
    tracing::debug!("filenames: {:?}", filenames);
    tracing::debug!("failed to read files: {:?}", errors);

    // File names is safe to unwrap now due to partitioning above
    let filenames = filenames.into_iter().map(|f| f.unwrap()).collect();

    // TODO sort `filenames` rather than just iterating over it. Then again, is that strictly necessary?
    let mut lorry_specs = read_spec_files(filenames, l.lfs)?;

    tracing::debug!("read lorry specs: {:?}", lorry_specs);

    // TODO do we actually need to sort lorry_specs? If we don't need to, we can avoid the `collect` call
    lorry_specs.sort_by(|(s1, _), (s2, _)| s1.cmp(s2));

    let interval = l.interval;
    let timeout = l.timeout;
    let prefix = &l.prefix;
    let added_paths = lorry_specs.into_iter().map(|(subpath, obj)| async move {
        let path = super::comms::LorryPath::new(format!("{}/{}", &prefix, subpath));

        let mut b = std::collections::BTreeMap::new();
        b.insert(&path, obj);
        let spec = b;
        db.add_to_lorries(&path, &spec, interval, timeout).await?;
        Ok::<_, ReadConfigError>(path)
    });

    let added_paths = futures::future::join_all(added_paths)
        .await
        .into_iter()
        .collect::<Result<Vec<_>, _>>()?;

    Ok(added_paths
        .into_iter()
        .collect::<std::collections::HashSet<_>>())
}

#[allow(deprecated)]
pub fn read_spec_files(
    filenames: Vec<PathBuf>,
    config_lfs: Option<bool>,
) -> Result<Vec<(String, LorrySpec)>, ReadConfigError> {
    let mut errors = vec![];
    let specs = filenames
        .into_iter()
        .filter_map(|filename| {
            tracing::debug!(
                "Reading .lorry file: {:?}",
                filename.canonicalize().expect("unwrap issue")
            );
            get_valid_lorry_specs(filename)
                .map_err(|e| errors.push(e))
                .ok()
        })
        .flat_map(|b| b.into_iter())
        .map(|(name, mut spec)| {
            if let LorrySpec::Git(ref mut single_lorry) = spec {
                if let Some(refspecs) = single_lorry.refspecs.as_ref() {
                    tracing::warn!("Deprecated refspecs field in use, use ref_patterns instead. This will be removed in a future version of Lorry");
                    single_lorry.ref_patterns = Some(refspecs.clone());
                }
                if single_lorry.lfs.is_none() {
                    single_lorry.lfs = config_lfs;
                }
            }
            (name, spec)
        })
        .collect();

    match errors.is_empty() {
        true => Ok(specs),
        false => Err(ReadConfigError::MultipleReadFails { errors }),
    }
}

fn get_valid_lorry_specs(
    filename: PathBuf,
) -> Result<std::collections::BTreeMap<String, workerlib::LorrySpec>, ReadConfigError> {
    let s = std::fs::read_to_string(&filename)
        .map_err(|e| ReadConfigError::IOError(filename.clone(), e))?;
    workerlib::extract_lorry_specs(s).map_err(|e| ReadConfigError::SpecParse(filename, e))
}

pub fn read_config_file(configuration_directory: &Path) -> Result<Vec<Config>, ReadConfigError> {
    let filename = configuration_directory.join(CONFIG_FILENAME);
    tracing::debug!("Reading configuration file {:?}", filename);

    match std::fs::read_to_string(&filename) {
        Ok(s) => match serde_yaml::from_str::<Vec<Config>>(&s) {
            Ok(j) => Ok(j),
            Err(e) => {
                tracing::error!("Error parsing config: {}", e);
                Err(ReadConfigError::SpecParse(filename.to_path_buf(), e))
            }
        },
        Err(e) => {
            tracing::error!(
                "There was an error accessing the lorry-controller.conf, is your config repo correctly formed?: {}", e);
            Err(ReadConfigError::IOError(filename.to_path_buf(), e))
        }
    }
}

/// Resolve the global configuration into all its underlying Lorry specs
pub fn resolve(configuration_directory: &Path) -> Result<Resolved, ReadConfigError> {
    let config = read_config_file(configuration_directory)?;
    let mut results: Vec<(Config, BTreeMap<String, LorrySpec>)> = Vec::with_capacity(config.len());
    for cfg in config {
        let Config::Lorries(lorry_cfg) = cfg;
        let mut inner: BTreeMap<String, LorrySpec> = BTreeMap::new();
        for pattern in lorry_cfg.globs.iter() {
            let config_dir = configuration_directory.to_str().unwrap().to_string();
            let target_glob = format!("{}/{}", config_dir, pattern);
            let specs = glob::glob(&target_glob)?;
            for path in specs {
                let path = path?;
                let spec = read_to_string(path.as_path())
                    .map_err(|e| ReadConfigError::IOError(path.to_path_buf(), e))?;
                let lorry_spec = extract_lorry_specs(spec)
                    .map_err(|e| ReadConfigError::SpecParse(path.to_path_buf(), e))?;
                inner.extend(lorry_spec);
            }
        }
        results.push((Config::Lorries(lorry_cfg), inner));
    }
    Ok(results)
}

#[tracing::instrument(skip_all)]
pub(crate) async fn get_confgit(
    app_settings: &ControllerSettings,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let confdir = &app_settings.configuration_directory;
    if let Some(config_url) = &app_settings.confgit_url {
        if confdir.exists() {
            update_confgit(
                config_url,
                &app_settings.confgit_branch,
                &app_settings.configuration_directory,
            )
            .await?
        } else {
            git_clone_confgit(
                config_url,
                &app_settings.confgit_branch,
                &app_settings.configuration_directory,
            )
            .await?
        }
    }
    // nothing to do
    Ok(())
}

#[tracing::instrument(skip_all)]
async fn update_confgit(
    _config_url: &str,
    config_branch: &str,
    confdir: &Path,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    // TODO: if the config_url changes you need to change the origin
    // TODO: These system calls are no longer consistent with how we do things
    // in workerlib, refactor this.
    tracing::info!("Updating CONFGIT in {:?}", &confdir);

    CommandBuilder::new("git")
        .args(&["reset", "--hard"])
        .execute(&confdir)
        .await?;

    CommandBuilder::new("git")
        .args(&["clean", "-fdx"])
        .execute(&confdir)
        .await?;

    CommandBuilder::new("git")
        .args(&["remote", "update", "origin"])
        .execute(&confdir)
        .await?;
    CommandBuilder::new("git")
        .args(&["reset", "--hard", &format!("origin/{}", &config_branch)])
        .execute(&confdir)
        .await?;

    Ok(())
}

#[tracing::instrument(skip_all)]
async fn git_clone_confgit(
    config_url: &str,
    config_branch: &str,
    confdir: &Path,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    tracing::info!("Cloning {} to {:?}", config_url, confdir);
    CommandBuilder::new("git")
        .args(&["clone", "-b"])
        .arg(config_branch)
        .arg(config_url)
        .arg(confdir)
        .execute(std::env::current_dir()?)
        .await?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::downstream::local::LocalDownstream;
    use crate::downstream::Downstream;
    use indoc::indoc;
    use rand::{distributions::Alphanumeric, thread_rng, Rng};
    use std::fs::{create_dir_all, remove_dir_all, write};
    use std::io::Write;
    use tempfile::NamedTempFile;

    #[test]
    fn test_config_resolve() {
        let test_dir_name: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(12)
            .map(char::from)
            .collect();
        let test_dir_path = format!("./test-data/{}", test_dir_name);
        let test_dir = Path::new(&test_dir_path);
        create_dir_all(test_dir).unwrap();

        write(
            Path::new(&test_dir_path).join(Path::new("lorry-controller.conf")),
            indoc! {r#"
                [
                    {
                        "type": "lorries",
                        "interval": "PT1H",
                        "timeout": "PT1H",
                        "prefix": "lorry-mirrors/hello",
                        "globs": [
                            "hello.lorry"
                        ]
                    },
                    {
                        "type": "lorries",
                        "interval": "PT1M",
                        "timeout": "PT1H",
                        "prefix": "lorry-mirrors/world",
                        "globs": [
                            "world.lorry"
                        ]
                    },
                    {
                        "type": "lorries",
                        "interval": "PT1M",
                        "timeout": "PT1H",
                        "prefix": "lorry-mirrors/git",
                        "globs": [
                            "git.lorry"
                        ],
                        "lfs": true
                    }
                ]
            "#},
        )
        .unwrap();

        write(
            Path::new(&test_dir_path).join(Path::new("hello.lorry")),
            indoc! {r#"
                raw-files1:
                    type: raw-file
                    urls:
                    - destination: hello/1.0 
                      url: https://example.org/hello-1.0.tar.xz
            "#},
        )
        .unwrap();

        write(
            Path::new(&test_dir_path).join(Path::new("world.lorry")),
            indoc! {r#"
                raw-files2:
                    type: raw-file
                    urls:
                    - destination: world/1.0 
                      url: https://example.org/world-1.0.tar.xz
            "#},
        )
        .unwrap();

        write(
            Path::new(&test_dir_path).join(Path::new("git.lorry")),
            indoc! {r#"
                git_mirror:
                    type: git
                    url: https://some.url
                    lfs: true
            "#},
        )
        .unwrap();

        let settings = ControllerSettings {
            downstream: Downstream::Local(LocalDownstream {
                base_dir: test_dir.to_path_buf(),
            }),
            configuration_directory: test_dir.to_path_buf(),
            confgit_url: None,
            confgit_branch: String::new(),
            hostname: String::new(),
            current_pid: 1,
            namespace_depth: 2,
        };

        let resolved_config = resolve(&settings.configuration_directory).unwrap();
        assert!(resolved_config.len() == 3);

        let first = resolved_config.first().unwrap();
        let raw_files1 = first.1.get("raw-files1").unwrap();
        match raw_files1 {
            LorrySpec::RawFiles(files) => {
                let file_spec = files.urls.first().unwrap();
                assert!(file_spec.url.to_string() == "https://example.org/hello-1.0.tar.xz")
            }
            _ => panic!("wrong lorry spec type"),
        }

        let git_mirror = &resolved_config[2];
        match &git_mirror.0 {
            Config::Lorries(spec) => assert!(spec
                .lfs
                .expect("failed to detect that lfs was specified in lorry-controller.conf")),
        };
        match git_mirror.1.get("git_mirror").unwrap() {
            LorrySpec::Git(spec) => {
                assert!(spec
                    .lfs
                    .expect("failed to detect that lfs was specified in single lorry spec"));
            }
            _ => panic!("wrong lorry spec type"),
        }

        remove_dir_all(test_dir_path).unwrap()
    }

    /// Check that lfs option in single lorry specs takes precedence over the
    /// global config
    #[test]
    fn test_lfs_resolve() {
        let mut with_lfs = NamedTempFile::new().unwrap();
        with_lfs
            .write_all(indoc! {br#"
                with_lfs:
                    type: git
                    url: https://some.url
                    lfs: true
            "#})
            .unwrap();
        let with_lfs = with_lfs.path().to_owned();

        let mut without_lfs = NamedTempFile::new().unwrap();
        without_lfs
            .write_all(indoc! {br#"
                without_lfs:
                    type: git
                    url: https://some.url
            "#})
            .unwrap();
        let without_lfs = without_lfs.path().to_owned();

        let specs = read_spec_files(vec![with_lfs, without_lfs], Some(false)).unwrap();
        assert_eq!(specs.len(), 2);

        match &specs[0].1 {
            LorrySpec::Git(spec) => assert!(spec.lfs.unwrap()),
            _ => panic!("Wrong lorry spec"),
        };

        match &specs[1].1 {
            LorrySpec::Git(spec) => assert!(!spec.lfs.unwrap()),
            _ => panic!("Wrong lorry spec"),
        };
    }
}

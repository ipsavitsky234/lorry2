use crate::comms::Job;
use async_trait::async_trait;
use std::path::Path;
use url::Url;
use workerlib::{try_mirror, Error, MirrorStatus};

/// Params passed into the runner
pub struct Params<'a> {
    pub working_area: &'a Path,
    pub job: &'a Job,
    pub target_url: &'a Url,
    pub mirror_settings: &'a workerlib::Arguments,
}

/// Executor is responsible for executing mirroring operations from within a
/// worker thread.
#[async_trait::async_trait]
pub trait Executor {
    /// Run the Lorry job returning any failure that happened during the
    /// mirroring operation.
    async fn run(&self, params: &Params) -> Result<MirrorStatus, Error>;
}

/// Default mirror executor used for normal operation
#[derive(Clone)]
pub struct DefaultExecutor;

#[async_trait]
impl Executor for DefaultExecutor {
    async fn run(&self, params: &Params) -> Result<MirrorStatus, Error> {
        // working area needs to always be an absolute path
        std::fs::create_dir_all(params.working_area).unwrap();
        let absolute_working_area = params.working_area.canonicalize().unwrap();
        let workspace =
            workerlib::workspace::Workspace::new(&absolute_working_area, &params.job.lorry_name);
        workspace.init_if_missing(matches!(
            params.job.lorry_spec,
            workerlib::LorrySpec::RawFiles(_)
        ))?;
        tracing::info!("workspace initialized: {:?}", workspace);
        let lorry_name = &params.job.lorry_name;
        let downstream_url = params
            .target_url
            .join(&format!("{lorry_name}.git"))
            .unwrap();
        try_mirror(
            &params.job.lorry_spec,
            lorry_name,
            &downstream_url,
            &workspace,
            params.mirror_settings,
        )
        .await
    }
}

//! TODO
//!

use crate::comms;
use std::{
    fs::File,
    io::{Error, Write},
    path::PathBuf,
};
use utils::command_wrapper::CommandExecutionError;

#[derive(thiserror::Error, Debug)]
pub(crate) enum PrepareLocalRepoError {
    #[error("Failure with IO: {0}")]
    Io(#[from] Error),

    #[error("Failure preparing command executor: {0}")]
    CommandExecution(#[from] CommandExecutionError),
}

#[derive(Clone, Debug)]
pub struct LocalDownstream {
    pub base_dir: PathBuf,
}

/// This is only used for testing purposes, and should not be used by real users
/// This is prevented by having `clap` skip the command that would go down this codepath, so it should be inaccessible
impl LocalDownstream {
    pub(crate) async fn prepare_repo(
        &self,
        repo_path: &comms::LorryPath,
        metadata: crate::downstream::RepoMetadata,
    ) -> Result<(), PrepareLocalRepoError> {
        let repo_path = self.base_dir.join(format!("{}.git", repo_path));

        std::fs::create_dir_all(&repo_path)?;

        let cwd = std::env::current_dir()?;
        utils::command_wrapper::CommandBuilder::new("git")
            // TODO this code is not ideal: (see how if metadata.head is None then we create the repo with the default branch but then only call git-dir if it was set...), I have a feeling in practice head should never be None and its just defensive programming on the part of the old version; we can ascertain this when we implement concrete upstreams.
            .args(&[
                "init",
                "--bare",
                "-b",
                metadata
                    .head
                    .as_deref()
                    .unwrap_or(workerlib::DEFAULT_BRANCH_NAME),
            ])
            .arg(&repo_path)
            .execute(&cwd)
            .await?;

        if let Some(head) = metadata.head {
            utils::command_wrapper::CommandBuilder::new("git")
                .arg("--git-dir")
                .arg(&repo_path)
                .args(&["symbolic-ref", "HEAD"])
                .arg(format!("refs/heads/{}", head))
                .execute(&cwd)
                .await?;
        }

        if let Some(description) = metadata.description {
            let mut f = File::create(repo_path.join("description"))?;
            f.write_all(description.as_bytes())?;
        }

        Ok(())
    }
}

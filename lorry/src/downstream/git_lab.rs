use crate::comms;
use crate::downstream;
use futures::StreamExt;
use futures::TryStreamExt;
use gitlab::api::groups::CreateGroupBuilderError;
use gitlab::api::groups::GroupBuilderError;
use gitlab::api::projects::repository::branches::CreateBranchBuilderError;
use gitlab::api::projects::CreateProjectBuilderError;
use gitlab::api::projects::EditProjectBuilderError;
use gitlab::api::projects::ProjectBuilderError;
use gitlab::api::ApiError;
use gitlab::api::AsyncQuery;
use gitlab::AsyncGitlab;
use gitlab::GitlabError;
use gitlab::RestError;
use std::path::PathBuf;
use tokio::fs;
use url::Url;

/// Implements minimal structs needed to construct projects and group
/// namespaces in downstream gitlab.
mod types {
    use serde::Deserialize;

    pub struct GroupId(pub u64);

    #[derive(Deserialize)]
    pub struct Group {
        pub id: u64,
        pub full_path: String,
    }

    #[derive(Deserialize)]
    pub struct Project {
        pub id: u64,
        pub description: Option<String>,
        pub default_branch: Option<String>,
    }

    #[derive(Deserialize)]
    pub struct RepoBranch {}
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum PrepareRepoError {
    #[error("Project path contains less than two components: This would attempt to create a project outside a group.")]
    ProjectPathTooShort,

    #[error("Failure building GitLab client: {0}")]
    GitlabBuilder(#[from] GitlabError),

    #[error("Failure preparing project builder: {0}")]
    GitlabProjectBuilder(#[from] ProjectBuilderError),

    #[error("Failure preparing create branch builder: {0}")]
    GitlabCreateBranchBuilder(#[from] CreateBranchBuilderError),

    #[error("Failure preparing create project builder: {0}")]
    GitlabCreateProjectBuilder(#[from] CreateProjectBuilderError),

    #[error("Failure preparing edit project builder: {0}")]
    GitlabEditProjectBuilder(#[from] EditProjectBuilderError),

    #[error("Failure preparing create group builder: {0}")]
    GitlabCreateGroupBuilder(#[from] CreateGroupBuilderError),

    #[error("Failure preparing group builder: {0}")]
    GitlabGroupBuilder(#[from] GroupBuilderError),

    #[error("Failure in API: {0}")]
    GitlabApi(#[from] ApiError<RestError>),

    #[error("Could not read token file: {0}")]
    TokenFileCouldNotBeRead(#[from] std::io::Error),
}

trait GitlabErrorReport {
    fn report(self, why: impl Into<String>) -> Self;
}

impl<T, E> GitlabErrorReport for Result<T, E>
where
    E: std::error::Error,
{
    fn report(self, why: impl Into<String>) -> Self {
        if let Err(e) = &self {
            let why = why.into();
            tracing::warn!("{why} - {e} - {e:?}");
        }
        self
    }
}

#[derive(Debug, Clone)]
pub enum TokenSource {
    Token(String),
    Path(PathBuf),
}

#[derive(Clone, Debug)]
pub struct GitLabDownstream {
    pub downstream_visibility: gitlab::api::common::VisibilityLevel,
    pub token_source: TokenSource,
    pub git_url: Url,
    pub insecure: bool,
}

impl GitLabDownstream {
    async fn get_current_client(&self) -> Result<AsyncGitlab, PrepareRepoError> {
        let token = match &self.token_source {
            TokenSource::Token(t) => t.to_owned(),
            TokenSource::Path(p) => fs::read_to_string(p)
                .await
                .map_err(PrepareRepoError::TokenFileCouldNotBeRead)?
                .trim()
                .to_string(),
        };
        if self.insecure {
            tracing::debug!("url: {}", self.git_url.authority());
            Ok(gitlab::GitlabBuilder::new(self.git_url.authority(), token)
                .insecure()
                .build_async()
                .await
                .report("Error creating client")?)
        } else {
            Ok(gitlab::GitlabBuilder::new(self.git_url.authority(), token)
                .build_async()
                .await
                .report("Error creating client")?)
        }
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn prepare_repo(
        &self,
        repo_path: &comms::LorryPath,
        metadata: downstream::RepoMetadata,
    ) -> Result<(), PrepareRepoError> {
        let gitlab_instance = self.get_current_client().await?;

        match gitlab::api::projects::Project::builder()
            .project(repo_path)
            .build()
            .report(format!("building project {repo_path}"))?
            .query_async(&gitlab_instance)
            .await
            .report(format!("querying project {repo_path}"))
        {
            Ok::<types::Project, _>(project) => {
                tracing::debug!("Project {repo_path} exists in local GitLab already.");

                //Update the description to match that from the upstream repo, if necessary
                if metadata.description != project.description {
                    if let Some(new_description) = metadata.description {
                        match gitlab::api::projects::EditProject::builder()
                            .project(repo_path)
                            .description(&new_description)
                            .build()
                            .report(format!(
                                "building editproject for {repo_path}: {new_description}"
                            )) {
                            Ok(query) => {
                                query
                                    .query_async(&gitlab_instance)
                                    .await
                                    .report(format!(
                                        "running editproject for {repo_path}: {new_description}"
                                    ))
                                    // Handle errors by reporting and continuing on; updating
                                    // project description is not critical!
                                    .map_or_else(
                                        |e| {
                                            tracing::warn!(
                                                "Failed to update description for {}: {e}",
                                                repo_path
                                            )
                                        },
                                        |_: types::Project| (),
                                    );
                            }
                            Err(e) => tracing::warn!(
                                "Failed to create update query for {}: {e}",
                                repo_path
                            ),
                        }
                    }
                }

                if project.default_branch.is_some() {
                    if metadata.head != project.default_branch && metadata.head.is_some() {
                        let new_head = metadata.head.unwrap();
                        // Create the branch in case it does not already exist

                        match gitlab::api::projects::repository::branches::CreateBranch::builder()
                            .project(repo_path)
                            .branch(&new_head)
                            .build()
                            .report(format!("building createbranch {repo_path}:{new_head}"))
                        {
                            Ok(q) => {
                                let _branch: Option<types::RepoBranch> = q
                                    .query_async(&gitlab_instance)
                                    .await
                                    .report(format!("running createbranch {repo_path}:{new_head}"))
                                    .ok();
                            }
                            Err(e) => tracing::warn!(
                                "Failed to create query to create a branch for {}: {e}",
                                repo_path
                            ),
                        };

                        match gitlab::api::projects::EditProject::builder()
                            .project(repo_path)
                            .default_branch(&new_head)
                            .build()
                            .report(format!("building editproject {repo_path}:{new_head}"))
                        {
                            Ok(q) => {
                                let _proj: Option<types::Project> = q
                                    .query_async(&gitlab_instance)
                                    .await
                                    .report(format!("running editproject {repo_path}:{new_head}"))
                                    .ok();
                            }
                            Err(e) => tracing::warn!(
                                "Failed to create query to set default branch for {}: {e}",
                                repo_path
                            ),
                        }
                        Ok(())
                    } else {
                        // Either the downstream and upstream defrault brnaches agree, which is good,
                        // we don't need to do anything. Or, the upstream default branch is not set.
                        // I have no idea what would cause that but I guess we'll just chug along anyway
                        //
                        // TODO: Seriously what would that case mean? Is it even possible?
                        Ok(())
                    }
                } else {
                    // The downstream has no default branch. That's OK, that should just mean we
                    // haven't pulled from upstream yet. Once we do that we'll get a default branch.
                    // So we return OK on the basis that we're about to pull to the downstream,
                    // which will shouldn't be obstructed by this fact, and will fix it
                    Ok(())
                }
            }

            Err(e) => {
                tracing::info!("Need to create {repo_path} on local GitLab due to err: {e}");
                let path_components = repo_path.components();

                if path_components.len() < 2 {
                    tracing::error!("Project path {repo_path} is too short!");
                    return Err(PrepareRepoError::ProjectPathTooShort);
                }

                let namespace_id = self.create_parent_groups(&path_components).await?;
                let created_project: types::Project = {
                    let existing_project: Result<types::Project, _> =
                        gitlab::api::projects::Project::builder()
                            .project(repo_path)
                            .build()
                            .report(format!("building project for get {repo_path}"))?
                            .query_async(&gitlab_instance)
                            .await
                            .report(format!("getting project {repo_path}"));
                    match existing_project {
                        Ok(ex) => Ok(ex),
                        Err(_) => {
                            let mut b = gitlab::api::projects::CreateProject::builder();
                            b.name(*(path_components.last().unwrap()))
                                .visibility(self.downstream_visibility)
                                .namespace_id(namespace_id.0);
                            b.path(*(path_components.last().unwrap()));

                            // TODO perhaps supply worker::DEFAULT_BRANCH_NAME if metadata.head is None? But that would not line up with the upstream...
                            // TODO we need to create a branch befor we set it as default!

                            if let Some(head) = metadata.head {
                                // Create the branch before we set it as default, or we'll get an error
                                let _: Option<types::RepoBranch> =
                                gitlab::api::projects::repository::branches::CreateBranch::builder()
                                    .project(repo_path)
                                    .branch(&head)
                                    .build()
                                    .report(format!("build createbranch for new project {repo_path}:{head}"))?
                                    .query_async(&gitlab_instance)
                                    .await
                                    .report(format!("run createbranch for new project {repo_path}:{head}"))
                                    .ok();
                                b.default_branch(head);
                            }

                            metadata
                                .description
                                .map(|description| b.description(description));
                            b.pages_access_level(
                                gitlab::api::projects::FeatureAccessLevelPublic::Disabled,
                            )
                            .container_registry_enabled(false)
                            .autoclose_referenced_issues(false)
                            .lfs_enabled(true)
                            .auto_devops_enabled(false);

                            b
                                .build()
                                .report(format!("build createproject for new project {repo_path}"))?
                                .query_async(&gitlab_instance)
                                .await
                                .report(format!("run createproject for new project {repo_path}"))
                        }
                    }
                }
                .report(format!("acquire created project {repo_path}"))?;

                tracing::info!("Created {} project in local Gitlab.", repo_path);
                let _: types::Project = gitlab::api::projects::EditProject::builder()
                    .project(created_project.id)
                    .issues_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
                    .merge_requests_access_level(
                        gitlab::api::projects::FeatureAccessLevel::Disabled,
                    )
                    .builds_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
                    .wiki_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
                    .snippets_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
                    .build()
                    .report(format!("building editproject for {repo_path}"))?
                    .query_async(&gitlab_instance)
                    .await
                    .report(format!("Running editproject for {repo_path}"))?;

                tracing::info!("Set up fully {} project in local Gitlab.", repo_path);
                Ok(())
            }
        }
    }

    /// Go through the path components. For each path component, make sure that the respective group exists, to create the full group tree, in preperation for creating a project at the most deeply nested group
    #[tracing::instrument(skip_all)]
    async fn create_parent_groups(
        &self,
        path_components: &[&str],
    ) -> Result<types::GroupId, PrepareRepoError> {
        struct InnerGroup {
            full_path: String,
            id: types::GroupId,
        }

        let r = futures::stream::iter(path_components[0..(path_components.len() - 1)].iter())
            .map(Ok::<_, PrepareRepoError>)
            .try_fold(
                InnerGroup {
                    full_path: "".to_string(),
                    id: types::GroupId(0),
                }, // These are dummy placeholder values that will be replaced in the first iteration of the fold.
                // TODO THIS IS STILL A GUARD VALUE THOUGH FIX IT UP TO BE CLEARER
                |parent_group: InnerGroup, &group_name| async move {
                    // FIXME This is very not ideal, as this rereads the token and recreates the api object for every path component
                    let gitlab_instance = self.get_current_client().await?;

                    let group_path = if parent_group.full_path.is_empty() {
                        group_name.to_string()
                    } else {
                        format!("{}/{group_name}", parent_group.full_path)
                    };

                    tracing::debug!("Creating group {group_path} if necessary");

                    let group: InnerGroup = match gitlab::api::groups::Group::builder()
                        .group(group_path.as_str())
                        .build()
                        .report(format!("building group get for {group_path}"))?
                        .query_async(&gitlab_instance)
                        .await
                        .report(format!("running group get for {group_path}"))
                    {
                        Ok::<types::Group, _>(g) => {
                            tracing::debug!("Group {} was found!", &g.full_path);
                            Ok(InnerGroup {
                                full_path: g.full_path,
                                id: types::GroupId(g.id),
                            })
                        }

                        Err(e) => match e {
                            // TODO I am really unsure about this. I used to check for ApiError::GitlabService, but that was failing in testing
                            // Looking at the docs, it seems we can get the "group not found" message in several different enum variants
                            // i.e GitLabService allowed me to specify an error code!
                            // so in conclusion TODO Make sure that we cover all the possible ways Gitlab can tell us we need to create this group
                            gitlab::api::ApiError::Gitlab { msg: _ } => {
                                tracing::debug!("trying to create group {group_path}...");
                                let r: types::Group = {
                                    let mut b = gitlab::api::groups::CreateGroup::builder();
                                    b.name(group_name);
                                    b.path(group_name);
                                    if !parent_group.full_path.is_empty() {
                                        b.parent_id(parent_group.id.0); //There is a real parent group so make the new group a subgroup of it
                                    }
                                    b.visibility(self.downstream_visibility);

                                    b.build()
                                        .report(format!("Building creategroup for {group_name}"))?
                                        .query_async(&gitlab_instance)
                                        .await
                                        .report(format!("Running creategroup for {group_name}"))
                                }?;

                                Ok(InnerGroup {
                                    full_path: r.full_path,
                                    id: types::GroupId(r.id),
                                })
                            }

                            e => {
                                tracing::debug!(
                                    "Got some other critical error:{e}. Debug form: {:?}",
                                    e
                                );
                                Err(e)
                            }
                        },
                    }?;

                    Ok(group)
                },
            )
            .await?
            .id;

        Ok(r)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use mockito::Server;

    #[tokio::test]
    pub async fn prepare_repo_exists() {
        let mut server = Server::new_async().await;

        // gitlab login
        let login = server
            .mock("GET", "/api/v4/user?")
            .with_status(200)
            .create_async()
            .await;

        let project = server
            .mock("GET", "/api/v4/projects/%2Ffuu%2Fbar?")
            .with_status(200)
            .with_body(r#"{"id": 1, "full_path": "/fuu/bar"}"#)
            .create_async()
            .await;

        let downstream = GitLabDownstream {
            downstream_visibility: gitlab::api::common::VisibilityLevel::Public,
            token_source: TokenSource::Token("gplat-FAKE".to_string()),
            git_url: Url::parse(&server.url()).unwrap(),
            insecure: true,
        };

        downstream
            .prepare_repo(
                &comms::LorryPath::new(String::from("/fuu/bar")),
                downstream::RepoMetadata {
                    head: Some(String::from("main")),
                    description: Some(String::from("some really great repo")),
                },
            )
            .await
            .unwrap();

        login.assert();

        project.assert()
    }
}

use crate::comms::Job;
use crate::executor::Executor;
use crate::state_db::LorryEntry;
use crate::url_builder::UrlBuilder;
use crate::worker::{DefaultWorker, JobAccepted, JobReady, JobResult, State, ThreadId, Worker};
use async_trait::async_trait;
use std::collections::HashMap;
use std::error::Error as StdError;
use std::sync::{Arc, RwLock};
use std::thread::available_parallelism;
use thiserror::Error;
use tokio::sync::mpsc::error::SendTimeoutError;
use tokio::sync::mpsc::{channel, Receiver};
use tokio::sync::Mutex;
use tokio::task::{spawn, JoinHandle};
use tokio::time::{self, Duration};
use tokio_util::sync::CancellationToken;
use work_queue::{LocalQueue, Queue};
use workerlib::Arguments;

/// Maximum amount of time sending a message on a channel can wait until it
/// causes a panic and the scheduler shuts down.
pub const CHANNEL_TIMEOUT_MS: u64 = 900;

/// Globally readable object that records the status of each running thread for
/// display in the Lorry web UI.
pub type Tracker = Arc<RwLock<HashMap<ThreadId, State>>>;

/// Scheduler component that runs in a separate thread
pub type Component<T> = Arc<Mutex<Box<T>>>;

#[derive(Error, Debug)]
pub enum SchedulerError {
    #[error("job recorder error: {0}")]
    Recorder(String),

    #[error("job polling error: {0}")]
    Polling(String),

    #[error("config polling error: {0}")]
    Configuration(String),

    #[error("channel timeout: {0}")]
    Timeout(#[from] Box<SendTimeoutError<Job>>),
}

struct Timing {
    allocator_interval: Duration,
    config_reader_interval: Duration,
    worker_interval: Duration,
}

impl Default for Timing {
    fn default() -> Self {
        Timing {
            allocator_interval: Duration::from_millis(800),
            config_reader_interval: Duration::from_millis(5000),
            worker_interval: Duration::from_millis(1500),
        }
    }
}

// TODO: This can be reworked so that a CancellationToken is referenced to a respective JoinHandle but for now I see no reason to add this
pub struct ShutdownHandle {
    handles: Vec<JoinHandle<()>>,
    cancellation_tokens: Vec<CancellationToken>,
}

impl ShutdownHandle {
    pub async fn shutdown(self) {
        tracing::warn!("shutting down scheduler threads");
        tracing::trace!(
            "running threads: {}",
            self.handles
                .iter()
                .filter(|handle| !handle.is_finished())
                .count()
        );

        tracing::trace!("cancelling {} tokens", self.cancellation_tokens.len());

        self.cancellation_tokens
            .iter()
            .for_each(|token| token.cancel());
        futures::future::join_all(self.handles).await;
    }
}

/// Create a new tracker
pub fn tracker() -> Tracker {
    Arc::new(RwLock::new(HashMap::new()))
}

/// Create a new component that can be accessed across threads
pub fn component<T>(t: T) -> Component<T> {
    Arc::new(Mutex::new(Box::new(t)))
}

/// Responsible for checking for new lorries to run as jobs
#[async_trait]
pub trait JobAllocator: Send + 'static {
    /// Return the next available lorry to run from the database
    async fn next(&self) -> Result<Option<LorryEntry>, Box<dyn StdError>>;

    /// Allocate a job to be ran by an available worker
    async fn issue(&self, entry: &LorryEntry) -> Result<Job, Box<dyn StdError>>;

    async fn cleanup_db(&self) -> Result<(), Box<dyn StdError>>;
}

/// Job poller that runs at a set interval
struct IntervalJobPoller<T>
where
    T: JobAllocator + Send + 'static,
{
    pub poller: Component<T>,
    pub interval: Duration,
    pub tracker: Tracker,
    pub queue: Queue<JobReady>,
}

impl<T> IntervalJobPoller<T>
where
    T: JobAllocator + Send + 'static,
{
    async fn poll(&self) -> Result<(), SchedulerError> {
        let mut interval = time::interval(self.interval);
        let poller = self.poller.lock().await;
        tracing::info!("Initializing job poller");
        loop {
            if let Some(lorry) = poller
                .next()
                .await
                .map_err(|e| SchedulerError::Polling(e.to_string()))?
            {
                let lorry_path = lorry.path.clone();
                let (tx, mut rx) = channel::<JobAccepted>(1);
                self.queue.push(JobReady(lorry.clone(), tx));
                let result = rx.recv().await.unwrap();
                let job = poller
                    .issue(&lorry)
                    .await
                    .map_err(|e| SchedulerError::Polling(e.to_string()))?;
                let job_id = job.id;
                result
                    .2
                    .send_timeout(job, Duration::from_millis(CHANNEL_TIMEOUT_MS))
                    .await
                    .map_err(Box::new)?;
                // record that the thread is now busy
                self.tracker
                    .write()
                    .unwrap()
                    .insert(result.0, State::Busy((lorry_path.to_string(), job_id)));
            } else {
                interval.tick().await;
            }
        }
    }

    async fn cleanup_db(&self) -> Result<(), SchedulerError> {
        let poller = self.poller.lock().await;
        poller
            .cleanup_db()
            .await
            .map_err(|e| SchedulerError::Polling(e.to_string()))
    }
}

/// Responsible for saving job results into a persistent store like a database
#[async_trait]
pub trait Recorder: Send + 'static {
    async fn record(&self, result: &JobResult) -> Result<(), Box<dyn StdError>>;
}

/// Wraps around a recorder and updates it as soon as it receives a result
struct FixedRecorder<T>
where
    T: Recorder + Send + 'static,
{
    pub results: Receiver<JobResult>,
    pub recorder: Component<T>,
    pub tracker: Tracker,
}

impl<T> FixedRecorder<T>
where
    T: Recorder + Send + 'static,
{
    async fn record(&mut self) -> Result<(), SchedulerError> {
        let recorder = self.recorder.lock().await;
        tracing::info!("Initializing job recorder");
        loop {
            if let Some(result) = self.results.recv().await {
                let thread_id = result.0;
                recorder
                    .record(&result)
                    .await
                    .map_err(|e| SchedulerError::Recorder(e.to_string()))?;
                self.tracker.write().unwrap().insert(thread_id, State::Idle);
            } else {
                tracing::warn!("Results channel has been shutdown");
                return Err(SchedulerError::Recorder(String::from(
                    "Results channel has shutdown",
                )));
            }
        }
    }
}

/// Responsble for periodically polling lorry configuration and updating the
/// database
#[async_trait]
pub trait ConfigReader: Send + 'static {
    /// Read the configuration from the file system or remote git repository
    /// and update the database to reflect the most recent config
    async fn read(&self) -> Result<(), Box<dyn StdError>>;
}

/// Read the configuration at a specified interval
pub struct IntervalConfigReader<T>
where
    T: ConfigReader + Send + 'static,
{
    pub config_reader: Component<T>,
    pub interval: Duration,
}

impl<T> IntervalConfigReader<T>
where
    T: ConfigReader + Send + 'static,
{
    pub async fn read(&self) -> Result<(), SchedulerError> {
        let config_reader = self.config_reader.lock().await;
        let mut interval = time::interval(self.interval);
        tracing::info!("Initializing config reader");
        loop {
            config_reader
                .read()
                .await
                .map_err(|e| SchedulerError::Configuration(e.to_string()))?;
            interval.tick().await;
        }
    }
}

struct Settings {
    threads: usize,
    local_queue_size: u16,
}

impl Default for Settings {
    fn default() -> Self {
        let n_cpus = available_parallelism().unwrap();
        let threads = if usize::from(n_cpus) == 1 {
            1
        } else {
            usize::from(n_cpus) - 1
        };
        Settings {
            threads,
            local_queue_size: 32,
        }
    }
}

/// Configure the job scheduler
#[derive(Default)]
pub struct Builder {
    settings: Settings,
    timing: Timing,
    url_builder: UrlBuilder,
}

#[allow(dead_code)]
impl Builder {
    /// Username of the downstream git forge
    pub fn username(mut self, username: &str) -> Self {
        self.url_builder.username = username.to_string();
        self
    }

    /// Hostname of the downstream git forge
    pub fn hostname(mut self, hostname: &str) -> Self {
        self.url_builder.hostname = Some(hostname.to_string());
        self
    }

    /// Personal Access Token (PAT) for downstream git forge
    pub fn private_token(mut self, token: Option<String>) -> Self {
        self.url_builder.private_token = token;
        self
    }

    /// A path to a file that contains a token for downstream git forge
    pub fn token_file(mut self, file: Option<std::path::PathBuf>) -> Self {
        self.url_builder.token_file = file;
        self
    }

    /// Number of worker threads to spawn
    pub fn threads(mut self, n_threads: usize) -> Self {
        self.settings.threads = n_threads;
        self
    }

    /// Queue size for each underlying worker thread
    pub fn local_queue_size(mut self, size: u16) -> Self {
        self.settings.local_queue_size = size;
        self
    }

    /// Time between checking for new jobs in the database
    pub fn poller_interval(mut self, duration: Duration) -> Self {
        self.timing.allocator_interval = duration;
        self
    }

    /// Interval between attempts to refresh the remote (or local) configuration
    pub fn config_reader_interval(mut self, duration: Duration) -> Self {
        self.timing.config_reader_interval = duration;
        self
    }

    pub fn build<P, R, C, E>(
        self,
        arguments: &Arguments,
        poller: P,
        recorder: R,
        config_reader: Option<C>,
        executor: E,
    ) -> Scheduler<P, R, C, E>
    where
        P: JobAllocator + Send + 'static,
        R: Recorder + Send + 'static,
        C: ConfigReader + Send + 'static,
        E: Executor + Clone + Send + Sync + 'static,
    {
        // Work stealing queue passed to each thread bound worker
        let queue = Queue::<JobReady>::new(self.settings.threads, self.settings.local_queue_size);
        let (result_tx, result_rx) = channel::<JobResult>(self.settings.threads * 4);

        let tracker = Tracker::new(RwLock::new(HashMap::from_iter(
            (0..self.settings.threads).map(|n| (ThreadId(n), State::Idle)),
        )));

        let workers: Vec<(LocalQueue<JobReady>, DefaultWorker<E>)> = queue
            .local_queues()
            .enumerate()
            .map(|(i, q)| {
                (
                    q,
                    DefaultWorker {
                        thread_id: ThreadId(i),
                        runner: component(executor.clone()),
                        mirror_settings: arguments.clone(),
                        url_builder: self.url_builder.clone(),
                        polling_interval: self.timing.worker_interval,
                        result_ch: result_tx.clone(),
                    },
                )
            })
            .collect();

        Scheduler {
            tracker: tracker.clone(),
            poller: IntervalJobPoller {
                poller: Arc::new(Mutex::new(Box::new(poller))),
                interval: self.timing.allocator_interval,
                tracker: tracker.clone(),
                queue,
            },
            recorder: FixedRecorder {
                recorder: Arc::new(Mutex::new(Box::new(recorder))),
                results: result_rx,
                tracker: tracker.clone(),
            },
            config_reader: config_reader.map(|config_reader| IntervalConfigReader {
                config_reader: Arc::new(Mutex::new(Box::new(config_reader))),
                interval: self.timing.config_reader_interval,
            }),
            workers,
        }
    }
}

pub struct Scheduler<P, R, C, E>
where
    P: JobAllocator + Send + 'static,
    R: Recorder + Send + 'static,
    C: ConfigReader + Send + 'static,
    E: Executor + Clone + Send + Sync + 'static,
{
    poller: IntervalJobPoller<P>,
    recorder: FixedRecorder<R>,
    config_reader: Option<IntervalConfigReader<C>>,
    tracker: Tracker,
    workers: Vec<(LocalQueue<JobReady>, DefaultWorker<E>)>,
}

impl<P, R, C, E> Scheduler<P, R, C, E>
where
    P: JobAllocator + Send + 'static,
    R: Recorder + Send + 'static,
    C: ConfigReader + Send + 'static,
    E: Executor + Clone + Send + Sync + 'static,
{
    pub async fn schedule_all(mut self) -> Result<(ShutdownHandle, Tracker), SchedulerError> {
        let mut handles: Vec<JoinHandle<()>> = Vec::new();
        let mut cancellation_tokens: Vec<CancellationToken> = Vec::new();

        // spawn job poller
        let poller_cancellation_token = CancellationToken::new();
        cancellation_tokens.push(poller_cancellation_token.clone());
        handles.push(spawn(async move {
            tokio::select! {
                poller_error = self.poller.poll() => {
                    if let Err(err) = poller_error {
                        panic!("job poller shut down: {err}");
                    }
                }
                _ = poller_cancellation_token.cancelled() => {
                    tracing::debug!("shutting down poller");
                    self.poller.cleanup_db().await.expect("cleanup failed");
                }
            }
        }));

        // spawn n workers for each thread
        for (q, worker) in self.workers.into_iter() {
            let current_task_token = CancellationToken::new();
            cancellation_tokens.push(current_task_token.clone());
            handles.push(spawn(async move {
                tokio::select! {
                    worker_error = worker.work(q) => {
                        if let Err(err) = worker_error {
                            panic!("worker shut down: {err}");
                        }
                    }
                    _ = current_task_token.cancelled() => {
                        tracing::debug!("shutting down job");
                    }
                }
            }));
        }

        // spawn db recorder
        let db_cancellation_token = CancellationToken::new();
        cancellation_tokens.push(db_cancellation_token.clone());
        handles.push(spawn(async move {
            tokio::select! {
                db_error = self.recorder.record() => {
                    if let Err(err) = db_error {
                        panic!("recorder shut down: {err}");
                    }
                }
                _ = db_cancellation_token.cancelled() => {
                    tracing::debug!("shutting down db");
                }
            }
        }));

        // spawn config reader
        let config_reader_cancellation_token = CancellationToken::new();
        cancellation_tokens.push(config_reader_cancellation_token.clone());
        if let Some(config_reader) = self.config_reader {
            handles.push(spawn(async move {
                tokio::select! {
                    config_reader_error = config_reader.read() => {
                        if let Err(err) = config_reader_error {
                            panic!("config reader shut down: {err}")
                        }
                    }
                    _ = config_reader_cancellation_token.cancelled() => {
                        tracing::debug!("shutting down config reader");
                    }
                }
            }));
        }

        Ok((
            ShutdownHandle {
                handles,
                cancellation_tokens,
            },
            self.tracker.clone(),
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::comms::{Interval, JobExitStatus, JobId, LorryPath, TimeStamp};
    use crate::executor::{Executor, Params};
    use async_trait::async_trait;
    use tokio::sync::mpsc::{channel, Sender};
    use tokio::time::sleep;
    use url::Url;
    use workerlib::{lorry_specs::SingleLorry, LorrySpec};
    use workerlib::{Error, MirrorStatus, PushRefs};

    #[derive(Clone, Default)]
    struct NoopExecutor {
        pub error: bool,
        pub panic: bool,
        pub runtime: Option<Duration>,
    }

    #[async_trait]
    impl Executor for NoopExecutor {
        async fn run(&self, _params: &Params) -> Result<MirrorStatus, Error> {
            if let Some(runtime) = self.runtime {
                sleep(runtime).await;
                return Ok(MirrorStatus::default());
            }
            if self.panic {
                panic!("runner has paniced");
            }
            if self.error {
                return Err(Error::AllRefspecsFailed {
                    refs: PushRefs::default(),
                    n_attempted: 1,
                });
            }
            Ok(MirrorStatus::default())
        }
    }

    struct NoopPoller(Option<Sender<bool>>);

    #[async_trait]
    impl JobAllocator for NoopPoller {
        async fn next(&self) -> Result<Option<LorryEntry>, Box<dyn StdError>> {
            Ok(Some(LorryEntry {
                path: LorryPath::new(String::new()),
                name: String::new(),
                spec: LorrySpec::Git(SingleLorry::default()),
                running_job: None,
                last_run: TimeStamp(0),
                last_attempted: TimeStamp(0),
                interval: Interval(0),
                // 1s timeout for lorry jobs
                lorry_timeout: Interval(1),
                last_run_results: JobExitStatus::Running,
                last_run_error: None,
                purge_from_before: TimeStamp(0),
                priority: false,
            }))
        }

        async fn issue(&self, _entry: &LorryEntry) -> Result<Job, Box<dyn StdError>> {
            Ok(Job {
                lorry_name: String::new(),
                lorry_spec: LorrySpec::Git(SingleLorry::default()),
                id: JobId(0),
                path: LorryPath::new(String::new()),
                purge_cutoff: TimeStamp(0),
                insecure: None,
                mirror_server_base_url: Url::parse("http://localhost").unwrap(),
            })
        }

        async fn cleanup_db(&self) -> Result<(), Box<dyn StdError>> {
            if let Some(chan) = &self.0 {
                chan.send(true).await.unwrap();
            }
            Ok(())
        }
    }

    struct NoopRecorder(Option<Sender<(ThreadId, bool)>>);

    #[async_trait]
    impl Recorder for NoopRecorder {
        async fn record(&self, result: &JobResult) -> Result<(), Box<dyn StdError>> {
            if let Some(chan) = &self.0 {
                chan.send((result.0, result.2.is_err())).await.unwrap();
            }
            Ok(())
        }
    }

    struct NoopConfigReader;

    #[async_trait]
    impl ConfigReader for NoopConfigReader {
        async fn read(&self) -> Result<(), Box<dyn StdError>> {
            Ok(())
        }
    }

    #[tokio::test]
    async fn scheduler_run_1x_thread() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = Builder::default().threads(1).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor::default(),
        );

        let (shutdown, _) = scheduler.schedule_all().await?;
        let result = rx.recv().await.unwrap();

        assert!(result.0 == ThreadId(0));
        assert!(!result.1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    async fn scheduler_run_3x_thread() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = Builder::default().threads(3).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor::default(),
        );

        let (shutdown, _) = scheduler.schedule_all().await?;
        let result = rx.recv().await.unwrap();

        assert!(result.0 == ThreadId(0));
        assert!(!result.1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    async fn scheduler_run_exceeds_runtime() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = Builder::default().threads(3).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor {
                runtime: Some(Duration::from_secs(3)),
                ..Default::default()
            },
        );

        let (shutdown, _) = scheduler.schedule_all().await?;
        let result = rx.recv().await.unwrap();

        assert!(result.0 == ThreadId(0));
        // job fails
        assert!(result.1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    async fn scheduler_run_2_threads_5_jobs_all_blocking() -> Result<(), Box<dyn std::error::Error>>
    {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = Builder::default().threads(2).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor {
                runtime: Some(Duration::from_secs(3)),
                ..Default::default()
            },
        );

        let (shutdown, _) = scheduler.schedule_all().await?;

        assert!(rx.recv().await.unwrap().1);
        assert!(rx.recv().await.unwrap().1);
        assert!(rx.recv().await.unwrap().1);
        assert!(rx.recv().await.unwrap().1);
        assert!(rx.recv().await.unwrap().1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    async fn scheduler_run_job_fails() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = Builder::default().threads(3).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor {
                error: true,
                ..Default::default()
            },
        );

        let (shutdown, _) = scheduler.schedule_all().await?;
        let result = rx.recv().await.unwrap();

        assert!(result.0 == ThreadId(0));
        assert!(result.1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    #[should_panic]
    async fn scheduler_run_job_panics() {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = Builder::default().threads(3).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor {
                panic: true,
                ..Default::default()
            },
        );

        let _ = scheduler.schedule_all().await.unwrap();
        tokio::time::timeout(Duration::from_millis(500), rx.recv())
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn scheduler_graceful_shutdown() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<bool>(1);
        let scheduler = Builder::default()
            .threads(3)
            .poller_interval(Duration::from_millis(100))
            .build(
                &Arguments::default(),
                NoopPoller(Some(tx)),
                NoopRecorder(None),
                Some(NoopConfigReader {}),
                NoopExecutor::default(),
            );

        let (shutdown, _tracker) = scheduler.schedule_all().await?;

        shutdown.shutdown().await;

        let shutdown_val = rx.recv().await.unwrap();

        assert!(shutdown_val);

        Ok(())
    }
}

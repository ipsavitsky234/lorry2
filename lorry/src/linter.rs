use crate::read_config::ReadConfigError;
use std::{collections::BTreeMap, fmt::Display, path::Path};
use workerlib::LorrySpec;

#[derive(thiserror::Error, Debug)]
pub enum LinterError {
    /// Indicates something went wrong loading the mirror specifications
    #[error("Configuration Error: {0}")]
    Config(#[from] ReadConfigError),

    /// Indicates the configuration has a raw file specified twice
    #[error("Duplicate Raw Files: {0}:\n{1:?}")]
    DuplicateRawFiles(String, LorrySpec),

    /// Indicates the configuration has a git mirror specified twice
    #[error("Duplicate Git Mirror: {0}:\n{1:?}")]
    DuplicateGitMirrors(String, LorrySpec),
}

/// Configuration stats
#[derive(Default)]
pub struct Stats {
    spec_files: usize,
    git_mirrors: usize,
    raw_files: usize,
}

impl Display for Stats {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Spec File Count: {}", self.spec_files)?;
        writeln!(f, "Git Mirrors: {}", self.git_mirrors)?;
        write!(f, "Raw Files: {}", self.raw_files)?;
        Ok(())
    }
}

/// Lorry configuration linter
pub struct Linter<'a> {
    path: &'a Path,
}

impl<'a> Linter<'a> {
    pub fn new(path: &'a Path) -> Linter<'a> {
        Self { path }
    }

    pub fn stats(&self) -> Result<Stats, ReadConfigError> {
        let resolved = crate::read_config::resolve(self.path)?;
        Ok(resolved.iter().fold(Stats::default(), |mut accm, spec| {
            accm.spec_files = resolved.len();
            accm.raw_files += spec.1.iter().fold(0, |accm, (_, spec)| match spec {
                LorrySpec::RawFiles(entries) => accm + entries.urls.len(),
                _ => accm,
            });
            accm.git_mirrors += spec.1.iter().fold(0, |accm, (_, spec)| match spec {
                LorrySpec::Git(_) => accm + 1,
                _ => accm,
            });
            accm
        }))
    }

    pub fn lint(&self) -> Result<(), Vec<LinterError>> {
        let resolved = crate::read_config::resolve(self.path).map_err(|e| vec![e.into()])?;
        let mut git_mirrors_by_url: BTreeMap<String, LorrySpec> = BTreeMap::new();
        let mut raw_files_by_url: BTreeMap<String, LorrySpec> = BTreeMap::new();
        let mut linter_errors: Vec<LinterError> = Vec::new();
        resolved.iter().for_each(|(_, specs)| {
            specs.values().for_each(|spec| match spec {
                workerlib::LorrySpec::Git(git_spec) => {
                    if let Some(other) =
                        git_mirrors_by_url.insert(git_spec.url.clone(), spec.clone())
                    {
                        linter_errors.push(LinterError::DuplicateGitMirrors(
                            git_spec.url.clone(),
                            other.clone(),
                        ))
                    }
                }
                workerlib::LorrySpec::RawFiles(raw_file_specs) => {
                    raw_file_specs.urls.iter().for_each(|raw_file_url| {
                        if let Some(other) =
                            raw_files_by_url.insert(raw_file_url.url.to_string(), spec.clone())
                        {
                            linter_errors.push(LinterError::DuplicateRawFiles(
                                raw_file_url.url.to_string(),
                                other.clone(),
                            ))
                        }
                    })
                }
            })
        });
        if linter_errors.is_empty() {
            Ok(())
        } else {
            Err(linter_errors)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{fs::write, path::Path};

    const TEST_CONFIG_CONTENTS: &str = r#"
[
    {
        "type": "lorries",
        "interval": "PT1H",
        "timeout": "PT1H",
        "prefix": "lorry-mirrors/hello",
        "globs": [
            "hello.lorry"
        ]
    }
]
"#;

    const TEST_LORRY_CONTENTS_OK: &str = r#"
raw-files1:
    type: raw-file
    urls:
    - destination: hello/1.0 
      url: https://example.org/hello-1.0.tar.xz
"#;

    const TEST_LORRY_DUPLICATE_RAW_FILES: &str = r#"
raw-files1:
    type: raw-file
    urls:
    - destination: hello/1.0 
      url: https://example.org/hello-1.0.tar.xz
raw-files2:
    type: raw-file
    urls:
    - destination: hello/2.0 
      url: https://example.org/hello-1.0.tar.xz
"#;

    const TEST_LORRY_DUPLICATE_GIT_MIRRORS: &str = r#"
git_mirror1:
    type: git
    url: https://some.url
git_mirror2:
    type: git
    url: https://some.url
"#;

    #[test]
    fn test_linter_basic() {
        let temp_dir = tempfile::TempDir::new().unwrap();
        let config_path = temp_dir.path().join(Path::new("lorry-controller.conf"));
        write(config_path, TEST_CONFIG_CONTENTS).unwrap();
        let lorry_path = temp_dir.path().join(Path::new("hello.lorry"));
        let lorry_path = lorry_path.as_path();
        write(lorry_path, TEST_LORRY_CONTENTS_OK).unwrap();
        let linter = Linter::new(temp_dir.path());
        assert!(linter.lint().err().is_none());
        write(lorry_path, TEST_LORRY_DUPLICATE_RAW_FILES).unwrap();
        let error = linter.lint().err().unwrap();
        let first_error = error.first().unwrap();
        assert!(matches!(first_error, LinterError::DuplicateRawFiles(_, _)));
        write(lorry_path, TEST_LORRY_DUPLICATE_GIT_MIRRORS).unwrap();
        let error = linter.lint().err().unwrap();
        let first_error = error.first().unwrap();
        assert!(matches!(
            first_error,
            LinterError::DuplicateGitMirrors(_, _)
        ));
    }

    #[test]
    fn test_linter_duplicate_raw_files() {
        let temp_dir = tempfile::TempDir::new().unwrap();
        let config_path = temp_dir.path().join(Path::new("lorry-controller.conf"));
        write(config_path, TEST_CONFIG_CONTENTS).unwrap();
        let lorry_path = temp_dir.path().join(Path::new("hello.lorry"));
        let lorry_path = lorry_path.as_path();
        write(lorry_path, TEST_LORRY_DUPLICATE_RAW_FILES).unwrap();
        let linter = Linter::new(temp_dir.path());
        let error = linter.lint().err().unwrap();
        let first_error = error.first().unwrap();
        assert!(matches!(first_error, LinterError::DuplicateRawFiles(_, _)));
        match first_error {
            LinterError::DuplicateRawFiles(url, _) => {
                assert!(url == "https://example.org/hello-1.0.tar.xz")
            }
            _ => unreachable!(),
        }
    }

    #[test]
    fn test_linter_duplicate_git_mirrors() {
        let temp_dir = tempfile::TempDir::new().unwrap();
        let config_path = temp_dir.path().join(Path::new("lorry-controller.conf"));
        write(config_path, TEST_CONFIG_CONTENTS).unwrap();
        let lorry_path = temp_dir.path().join(Path::new("hello.lorry"));
        let lorry_path = lorry_path.as_path();
        write(lorry_path, TEST_LORRY_DUPLICATE_GIT_MIRRORS).unwrap();
        let linter = Linter::new(temp_dir.path());
        let error = linter.lint().err().unwrap();
        let first_error = error.first().unwrap();
        assert!(matches!(
            first_error,
            LinterError::DuplicateGitMirrors(_, _)
        ));
        match first_error {
            LinterError::DuplicateGitMirrors(url, _) => {
                assert!(url == "https://some.url")
            }
            _ => unreachable!(),
        }
    }
}

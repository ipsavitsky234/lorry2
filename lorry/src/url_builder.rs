use url::Url;

#[derive(Debug, Clone)]
pub struct UrlBuilder {
    pub username: String,
    pub private_token: Option<String>,
    pub token_file: Option<std::path::PathBuf>,
    pub hostname: Option<String>,
    pub port: Option<u16>,
}

impl Default for UrlBuilder {
    fn default() -> Self {
        Self::new("oauth2")
    }
}

impl UrlBuilder {
    pub fn new(username: &str) -> Self {
        UrlBuilder {
            username: username.to_string(),
            private_token: None,
            token_file: None,
            hostname: None,
            port: None,
        }
    }

    pub fn build(&self, base_url: &Url, insecure: Option<bool>) -> Url {
        let token = if self.private_token.is_some() {
            self.private_token.clone()
        } else {
            self.token_file.as_ref().map(|path| {
                std::fs::read_to_string(path)
                    .unwrap_or_else(|_| panic!("Could not read the token file: {}", path.display()))
                    .trim()
                    .to_string()
            })
        };

        let mut mirror_server_base_url = if let Some(ref token) = token {
            let mut url = base_url.clone();
            if insecure.expect("Internal error: It looks like you want to use a remote downstream but `insecure` hasn't been specified") {
            tracing::info!("Using http to push to mirror");
            url.set_scheme("http").unwrap_or_else(|_| panic!("Internal error: Failed to set scheme for {url}"));
        } else {
            tracing::info!("Using https to push to mirror");
        };
            url.set_username(&self.username)
                .expect("Internal error: Lorry failed to apply credentials for downstream server");
            url.set_password(Some(token))
                .expect("Internal error: Lorry failed to apply credentials for downstream server");
            url
        } else if base_url.scheme() == "file" {
            base_url.clone()
        } else {
            tracing::info!("Using ssh to push to mirror");
            Url::parse(&format!(
                "ssh://git@{}",
                base_url.host_str().unwrap_or_else(|| panic!(
                    "Internal error: Lorry passed empty hostname to minion: {}",
                    base_url
                ))
            ))
            .expect("Internal error: Failed to build ssh URL")
        };

        if let Some(host_name) = &self.hostname {
            mirror_server_base_url
                .set_host(Some(host_name))
                .expect("Logic error: Failed to set host name specified in worker conf");
        }

        if let Some(port) = self.port {
            mirror_server_base_url
                .set_port(Some(port))
                .expect("Logic error: Failed to set port number specified in worker conf");
        }

        mirror_server_base_url
    }
}

#[cfg(test)]
mod tests {
    use std::{fs, io::Write};
    use tempfile::tempdir;

    use super::*;

    #[test]
    fn test_url_builder_token() {
        let base_url = Url::parse("https://github.com/rsc/hello").unwrap();
        let builder = UrlBuilder {
            username: String::from("oauth2"),
            private_token: Some(String::from("super-secret-token")),
            token_file: None,
            hostname: Some(String::from("localhost")),
            port: Some(8080),
        };
        let target = builder.build(&base_url, Some(true));
        assert!(target.to_string() == "http://oauth2:super-secret-token@localhost:8080/rsc/hello");
    }

    #[test]
    fn test_url_builder_token_file() {
        let tempdir = tempdir().expect("Could not get temporary directory");
        let tempfile_path = tempdir.path().join(".token");
        fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create_new(true)
            .open(&tempfile_path)
            .unwrap();

        let mut tempfile_writer =
            fs::File::create(tempfile_path.clone()).expect("Could not create file");
        tempfile_writer
            .write(b"token")
            .expect("Could not write to tempfile");

        let base_url = Url::parse("https://github.com/rsc/hello").unwrap();
        let builder = UrlBuilder {
            username: String::from("oauth2"),
            private_token: None,
            token_file: Some(tempfile_path),
            hostname: Some(String::from("localhost")),
            port: Some(8080),
        };
        let target = builder.build(&base_url, Some(true));
        assert!(target.to_string() == "http://oauth2:token@localhost:8080/rsc/hello");
    }
}

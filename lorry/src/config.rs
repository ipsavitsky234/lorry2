use crate::downstream::Visibility;
use iso8601::{duration, Duration};
use serde::Deserialize;
use serde_with::{serde_as, DisplayFromStr};
use std::env::var;
use std::fs::read_to_string;
use std::io::Error as IoError;
use std::path::{Path, PathBuf};
use std::thread::available_parallelism;
use thiserror::Error;
use toml::de::Error as SerializationError;
use tracing::Level;

/// A configuration related error
#[derive(Error, Debug)]
pub enum Error {
    #[error("IO Error")]
    Io(#[from] IoError),

    #[error("Serialization Error: {0}")]
    Serialization(#[from] SerializationError),

    #[error("Required Value is Missing: {0}")]
    RequiredValue(String),
}

/// Mechanism to use for cloing repositories
#[derive(Default, Debug, Deserialize, Clone)]
pub enum CloneEngine {
    #[default]
    GitBinary,

    Libgit2,
}

/// Clone options
#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(default)]
pub struct Clone {
    /// Tool to use to accomplish mirroring the remote, can be libgit2 or
    /// the native git binary. Each option has different performance and
    /// security implications.
    pub engine: CloneEngine,

    /// Number of threads to use for cloning, this setting only effects
    /// the operation when using the git binary
    pub n_threads: usize,
}

impl Default for Clone {
    fn default() -> Self {
        Clone {
            engine: CloneEngine::GitBinary,
            n_threads: 1,
        }
    }
}

/// Settings for the creating the lorry controller server.
#[serde_as]
#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(default)]
pub struct Configuration {
    /// Local directory populated with configuration files. This will be populated when the /read_config
    /// endpoint is called. Initially, this is not read, and the endpoint must be called for controller
    /// to read in the configuration. This includes information about what we want to mirror.
    pub configuration_directory: PathBuf,

    /// A URL representing a remote Lorry configuration in a Git repository.
    ///
    /// If the URL is not specified the configuration will be read directly
    /// from configuration_directory.
    pub confgit_url: Option<String>,

    pub confgit_branch: String,

    /// The period between lorry's attempt to re-read the confgit
    ///
    /// Defaults to 2 minutes.
    ///
    /// Note, Lorry reads the confgit immediately at startup, regardless of the
    /// value set here.
    pub confgit_update_period: Duration,

    /// Path to the state database. If there is no database at this path, one will be created.
    #[serde(rename = "statedb")]
    pub state_db: String,

    #[serde_as(as = "DisplayFromStr")]
    pub log_level: Level,

    /// Port to listen on for incoming HTTP connections
    pub port: u16,

    // "gitlab downstream" hacks
    // all there is support for is gitlab
    // TODO: Support more targets
    pub hostname: String,

    pub visibility: Visibility,

    /// Private token for use with Gitlab
    ///
    /// NOTE: if this value is not set Lorry will check for an environment
    /// variable named LORRY_GITLAB_PRIVATE_TOKEN and if that is not specified
    /// crash the server.
    #[serde(rename = "gitlab-private-token")]
    pub gitlab_private_token: Option<String>,

    /// A file that contains a private token for Gitlab
    #[serde(rename = "gitlab-private-token-file")]
    pub gitlab_private_token_file: Option<PathBuf>,

    /// If enabled non-TLS communication will be prefered (useful for
    /// development or testing purposes only)
    #[serde(rename = "gitlab-insecure-http")]
    pub insecure: bool,

    /// Number of workers to spawn that a responsible for cloning repositories
    /// defaults to the number of CPUs that are available on your system.
    pub n_threads: usize,

    // Worker/thread settings below
    /// Username for auth when pushing to git server
    #[serde(rename = "username")]
    pub username: Option<String>,

    /// Maximum number of HTTP redirections to follow when downloading raw-files
    #[serde(rename = "maximum-redirects")]
    pub maximum_redirects: Option<u32>,

    pub working_area: PathBuf,

    /// Clone/fetch related options.
    pub clone: Clone,

    /// Email address used as the administrator contact as well as part of
    /// the global git configuration.
    pub email: String,

    /// Path to the managed git configuration file
    pub git_config_path: PathBuf,

    /// Path to the program that returns the basic authentication password
    /// when pushing updates via LFS
    pub askpass_program: PathBuf,

    /// When enabled any raw-file mirror without a sha256sum will cause the
    /// mirror operation to fail with a Sha256sumNotSpecified error.
    #[serde(rename = "sha256sums-required")]
    pub sha256sums_required: Option<bool>,

    /// The "depth" at which to expose namespaced metrics. Lorrys have a
    /// concatanated path name associated with them e.g. sources/github/fuu.
    /// Metrics are exposed via prometheus to based on this value so that
    /// "mirror owners" can be alerted when failures or miss-configurations
    /// occur in mirrors that they are responsible for.
    pub namespace_depth: usize,

    /// The path to a credentials store that only works with the git binary.
    /// More on the credentials store in the
    /// [git documentation](https://git-scm.com/docs/git-credential-store)
    /// Format your file according to this
    /// [storage format](https://git-scm.com/docs/git-credential-store)
    pub git_credentials_file: Option<PathBuf>,
}

// Default configuration values
impl Default for Configuration {
    fn default() -> Self {
        Configuration {
            configuration_directory: PathBuf::default(),
            confgit_url: None,
            confgit_branch: String::from("main"),
            confgit_update_period: duration("PT2M").unwrap(),
            state_db: String::from("db"),
            log_level: Level::INFO,
            port: 3000,
            hostname: String::from("localhost"),
            visibility: Visibility::default(),
            gitlab_private_token: None,
            gitlab_private_token_file: None,
            insecure: bool::default(),
            n_threads: available_parallelism().unwrap().get(),
            username: None,
            maximum_redirects: None,
            working_area: PathBuf::from("workd"),
            clone: Clone::default(),
            email: String::default(), // was "lorry@example.org"
            git_config_path: Path::new(workerlib::git_config::DEFAULT_GIT_CONFIG_PATH)
                .to_path_buf(),
            git_credentials_file: None,
            askpass_program: Path::new("lorry-askpass").to_path_buf(),
            sha256sums_required: None,
            namespace_depth: 4,
        }
    }
}

pub fn load_str(config: &str) -> Result<Configuration, Error> {
    let mut cfg: Configuration = toml::from_str(config)?;

    // Prefer the environment variable over the configuration value
    // if it is present. If the value is not set then crash.
    let gitlab_private_token = var("LORRY_GITLAB_PRIVATE_TOKEN").ok();

    if let Some(token) = gitlab_private_token {
        cfg.gitlab_private_token = Some(token);
    }

    if cfg.gitlab_private_token.is_none() && cfg.gitlab_private_token_file.is_none() {
        return Err(Error::RequiredValue(
            "gitlab-private-token or gitlab-private-token-file".to_string(),
        ));
    }

    if cfg.gitlab_private_token.is_some() && cfg.gitlab_private_token_file.is_some() {
        tracing::warn!("both token and token file are set!");
    }

    if let Some(token_file) = cfg.gitlab_private_token_file {
        cfg.gitlab_private_token_file = Some(std::path::absolute(token_file.as_path())?);
    }

    // Resolve the absolute path of the global git config file
    cfg.git_config_path = std::path::absolute(cfg.git_config_path.as_path())?;

    // Read from path first, otherwise take it from the working directory
    if let Some(program) = get_program(cfg.askpass_program.as_path()) {
        cfg.askpass_program = program
    } else {
        cfg.askpass_program = std::path::absolute(cfg.askpass_program.as_path())?;
    }

    // Set the absolute path to the credentials file
    if let Some(credential_configuration) = cfg.git_credentials_file {
        cfg.git_credentials_file = Some(std::path::absolute(credential_configuration.as_path())?);
    }

    // You cannot specify a private token without a username
    if cfg
        .gitlab_private_token
        .as_ref()
        .is_some_and(|_| cfg.username.is_none())
        || cfg
            .gitlab_private_token_file
            .as_ref()
            .is_some_and(|_| cfg.username.is_none())
    {
        return Err(Error::RequiredValue(
            "missing username since private token is provided".to_string(),
        ));
    }

    Ok(cfg)
}

pub fn load(path: &Path) -> Result<Configuration, Error> {
    let cfg_str = read_to_string(path)?;
    let cfg = load_str(&cfg_str)?;
    Ok(cfg)
}

/// Lookup a program from the current PATH
fn get_program(name: &Path) -> Option<PathBuf> {
    std::env::var_os("PATH").and_then(|paths| {
        std::env::split_paths(&paths)
            .filter_map(|dir| {
                let full_path = dir.join(name);
                if full_path.is_file() {
                    Some(full_path)
                } else {
                    None
                }
            })
            .next()
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    pub const EXAMPLE_CONFIG: &str = include_str!("../../lorry.example.toml");

    #[test]
    fn config_controller_parse() {
        let config = r#"
            statedb = "/db/go/here"
            hostname = "http://localhost:9999"
            configuration-directory = "/config/go/here"
            confgit-url = "/come/git/your/config"
            confgit-update-period = "PT30H"
            confgit-branch = "hi/im/a/branch"
            port = 20
            log-level = "debug"
            gitlab-private-token = "super-secret-token"
            username = "oauth2"
        "#;

        let config = load_str(config).unwrap();

        assert_eq!(config.state_db, "/db/go/here".to_string());
        assert_eq!(
            config.configuration_directory,
            Path::new("/config/go/here").to_path_buf()
        );
        assert_eq!(
            config.confgit_url.unwrap(),
            "/come/git/your/config".to_string()
        );
        assert_eq!(config.confgit_update_period, "PT30H".parse().unwrap());
        assert_eq!(config.confgit_branch, "hi/im/a/branch".to_string());
        assert_eq!(config.port, 20);
        assert_eq!(config.log_level, Level::DEBUG);
    }

    #[test]
    fn test_example_config() {
        // ensure that the example configuration shipped with Lorry is valid
        std::env::set_var("LORRY_GITLAB_PRIVATE_TOKEN", "supersecuretoken");
        load_str(EXAMPLE_CONFIG).unwrap();
    }
}

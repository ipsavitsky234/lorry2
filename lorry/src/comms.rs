use serde::{Deserialize, Serialize};
use url::Url;

/// The status of a given job; either it is running (or has never been
/// completed before), or it is completed, with some exit status.
#[derive(Serialize, Deserialize, PartialEq, Copy, Clone, Debug)]
pub enum JobExitStatus {
    FinishedExitCode(i64),
    Running,
}

impl From<Option<i64>> for JobExitStatus {
    fn from(value: Option<i64>) -> JobExitStatus {
        if let Some(exit_code) = value {
            JobExitStatus::FinishedExitCode(exit_code)
        } else {
            JobExitStatus::Running // or, technically, has not been run yet
        }
    }
}

/// Sent by the controller to the worker to specify the job the worker should work on
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Job {
    // The text of the job, this would be the contents of the .lorry file
    pub lorry_name: String,

    pub lorry_spec: workerlib::LorrySpec,
    pub id: JobId,
    pub path: LorryPath,

    // If the working copies were mirrored before this date, delete them
    pub purge_cutoff: TimeStamp,

    // It may even make sense to pre-calculate the location of the downstream git repo, rather than just pass along
    // the base URL. We have all the information to do so server-side, and would simplify the client.
    pub insecure: Option<bool>,
    pub mirror_server_base_url: Url,
}

// Represents the partial file path that identifies a lorry spec; i.e by the relative upstream path it will be mirrored to.
#[derive(
    Serialize, Deserialize, Debug, Clone, sqlx::Type, PartialEq, PartialOrd, Eq, Ord, Hash,
)]
#[sqlx(transparent)]
#[serde(transparent)]
pub struct LorryPath(String);

impl std::fmt::Display for LorryPath {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl LorryPath {
    pub fn new(s: String) -> Self {
        LorryPath(s)
    }

    pub fn components(&self) -> Vec<&str> {
        self.0.split('/').collect::<Vec<_>>()
    }
}

impl<'a> From<&'a LorryPath> for gitlab::api::common::NameOrId<'a> {
    fn from(value: &'a LorryPath) -> Self {
        value.0.as_str().into()
    }
}

#[derive(Serialize, Deserialize, Copy, Clone, sqlx::Type, Debug, PartialEq)]
#[serde(transparent)]
#[sqlx(transparent)]
pub struct JobId(pub i64);

#[derive(Serialize, Deserialize, Copy, Clone, sqlx::Type, Debug, sqlx::FromRow)]
#[sqlx(transparent)]
pub struct TimeStamp(pub i64);

// TODO might it make sense to replace this with inbuilt Duration type?
#[derive(Copy, Clone, sqlx::Type, Debug, sqlx::FromRow, PartialEq, PartialOrd)]
#[sqlx(transparent)]
pub struct Interval(pub i64);

// TODO: Delete everything below this line.

impl<'de> Deserialize<'de> for Interval {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let d: iso8601::Duration = serde::de::Deserialize::deserialize(deserializer)?;
        match d {
            iso8601::Duration::YMDHMS {
                year,
                month,
                day,
                hour,
                minute,
                second,
                millisecond,
            } => Ok(Interval(
                millisecond as i64 / 1000
                    + second as i64
                    + minute as i64* 60
                    + hour as i64* 60 * 60
                    // We ignoring leap years and real months because that would be a pain to implement and that degree of accuracy isn't so important
                    // Who is going months between mirrors anyway?
                    + (year as i64* 365 + month as i64* 30 + day as i64) * 60 * 60 * 24,
            )),
            iso8601::Duration::Weeks(num_weeks) => {
                Ok(Interval(num_weeks as i64 * 7 * 24 * 60 * 60))
            }
        }
    }
}

impl Serialize for Interval {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        // TODO could simplify the seconds into minutes and hours but that's complex and not strictly needed
        (iso8601::Duration::YMDHMS {
            year: 0,
            month: 0,
            day: 0,
            hour: 0,
            minute: 0,
            second: self.0 as u32,
            millisecond: 0,
        })
        .serialize(serializer)
    }
}

-- Add a table that stores logs of errors in a cenrtal location, both internal (i.e from the controller) and external (from workers)
-- This will be queries by monitoring systems to get an idea of system health
-- SQLite doesn't actually need the id field as it will use ROWID, but we would also like to use other databases :)

CREATE TABLE errors(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    error TEXT NOT NULL,
    time_occurred INT NOT NULL,
    in_endpoint TEXT, -- Null or empty here means we couldn't ascertain which endpoint it was in
    for_lorry TEXT --Null/empty can mean an error in getting the lorry name, multiple lorries (inspect `error to get them`), 
    --or an endpoint that doesn't handle a mirroring job
);

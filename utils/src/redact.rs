//! Helper utility that can redact sensitive strings common in Lorry
//!

use fancy_regex::Regex;
use std::fmt::Display;
use std::sync::OnceLock;

fn get_expressions() -> &'static Vec<Regex> {
    static EXPRESSIONS: OnceLock<Vec<Regex>> = OnceLock::new();
    EXPRESSIONS.get_or_init(|| vec![Regex::new(r##"glpat-[0-9a-zA-Z\-_]*"##).unwrap()])
}

/// Removes any known sensitive strings from the input and replaces them
/// with LORRY_REDACTED
pub fn redact(input: &impl Display) -> String {
    let mut copy = input.to_string();
    for expr in get_expressions() {
        copy = expr.replace_all(&copy, "LORRY_REDACTED").to_string();
    }
    copy
}

#[cfg(test)]
mod tests {
    use super::*;

    struct TestCase<'a> {
        pub input: &'a str,
        pub expected: &'a str,
    }

    impl TestCase<'_> {
        pub fn check(&self) {
            let result = redact(&self.input.to_string());
            assert!(result == self.expected);
        }
    }

    #[test]
    fn test_redact() {
        TestCase {
            input: "glpat-Y38-kU_3pviux6H1D9ec",
            expected: "LORRY_REDACTED",
        }
        .check();
        TestCase {
            input: "glpat-AdmAbJdT-FMYPa_2bQyy",
            expected: "LORRY_REDACTED",
        }
        .check();
        TestCase {
            input: "glpat-MjW2FqijnvVBs7yPEoDG",
            expected: "LORRY_REDACTED",
        }
        .check();
        TestCase {
            input: "Using git binary to push remote: http://oauth2:glpat-25HjHgcpRJsY_JQweJYa@localhost:9999/lorry-mirrors/test/lorry.git",
            expected: "Using git binary to push remote: http://oauth2:LORRY_REDACTED@localhost:9999/lorry-mirrors/test/lorry.git",
        }.check();
    }
}

//! Command builder that supplies deaults to reduce verbosity of git calls
//!

use std::{ffi::OsStr, path::Path};
use thiserror::Error;
use tokio::process::Command;
use tracing::debug;

#[derive(Error, Debug)]
pub enum CommandExecutionError {
    /// Program failed to open a subprocess.
    #[error("Unable to run subcommand {command}")]
    IO {
        command: String,
        source: std::io::Error,
    },

    /// The subprocess ran to completion but returned a failure code.
    #[error("Command {command:?} failed with STDERR: {stderr}")]
    CommandError {
        command: String,
        status: std::process::ExitStatus,
        stderr: String,
    },
}

impl CommandExecutionError {
    /// Return the underlying process status code if applicable
    pub fn status(&self) -> Option<i32> {
        if let CommandExecutionError::CommandError {
            command: _,
            status,
            stderr: _,
        } = self
        {
            status.code()
        } else {
            None
        }
    }
}

/// This is a wrapper around the `std::Command` struct that just allows us to
/// write commands less verbosely by supplying the defaults implicitly (i.e
/// setting up the STDOUT/STDERR) as well as debug information and handing out
/// and err in a more convenient tuple.
pub struct CommandBuilder {
    internal: Command,
}

impl CommandBuilder {
    /// Creates a new Command builder that will call the given program
    pub fn new<A: AsRef<OsStr>>(cmd: A) -> CommandBuilder {
        let mut cmd = Command::new(cmd);
        cmd.kill_on_drop(true);
        CommandBuilder { internal: cmd }
    }

    /// Appends an argument to the command
    pub fn arg<A: AsRef<OsStr>>(mut self, arg: A) -> CommandBuilder {
        self.internal.arg(arg);
        self
    }

    /// Appends asequence of arguments to the command
    pub fn args<A: AsRef<OsStr>>(mut self, args: &[A]) -> CommandBuilder {
        self.internal.args(args);
        self
    }

    /// Sets enviroment variables for the process the command will execute in.
    /// Each pair in the argument array is the name:value of each enviroment
    /// variable.
    pub fn env<A: AsRef<OsStr>>(mut self, env: &[(A, A)]) -> CommandBuilder {
        for (key, val) in env {
            self.internal.env(key, val);
        }
        self
    }

    /// Executes the built process in the given directory. Takes no STDIN and
    /// runs to completion. If the command succeeds, returns the programs
    /// STDOUT and STDERR.
    ///
    /// Also logs that the command was run and the results to debug.
    pub async fn execute<P: AsRef<Path>>(
        mut self,
        cwd: P,
    ) -> Result<(String, String), CommandExecutionError> {
        self.internal.current_dir(cwd);

        self.internal.stdin(std::process::Stdio::null());

        let child_process_output =
            self.internal
                .output()
                .await
                .map_err(|e| CommandExecutionError::IO {
                    command: format!("{:?}", self.internal),
                    source: e,
                })?;
        let succeeded = child_process_output.status.success();
        let out = String::from_utf8_lossy(&child_process_output.stdout);
        let err = String::from_utf8_lossy(&child_process_output.stderr);
        debug!(
            "Command:{:?} \n Exit code:{:?} \n Stdout: {}, Stderr: {} \n",
            &self.internal,
            child_process_output.status.code(),
            out,
            err,
        );
        if !succeeded {
            debug!(
                "Failed to run {:?}, Status was: {:?}, Stdout was:\n{:?},Stderr was:\n{:?}",
                &self.internal,
                child_process_output.status.code(),
                out,
                err
            );
            Err(CommandExecutionError::CommandError {
                status: child_process_output.status,
                stderr: err.to_string(),
                command: format!("{:?}", self.internal),
            })
        } else {
            Ok((out.to_string(), err.to_string()))
        }
    }
}

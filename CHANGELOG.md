# 1.2.0 (https://gitlab.com/CodethinkLabs/lorry/lorry2/compare/v1.1.0...v1.2.0) (2024-04-03)

### Features

* Add error metrics to the controller. ([276fd8ed](https://gitlab.com/CodethinkLabs/lorry/lorry2/commit/276fd8edc725139ded499c78ea247d67cab987ff))

# 1.1.0 (https://gitlab.com/CodethinkLabs/lorry/lorry2/compare/v1.0.1...v1.1.0) (2024-03-25)

### Bug Fixes

* Update log-level to properly configure lorry-controller ([171103d](https://gitlab.com/CodethinkLabs/lorry/lorry2/commit/171103db35ff4f13b9bcde7562fb04c0615ad25b))
* raw_file_import: improve error messaging ([88c3e1e](https://gitlab.com/CodethinkLabs/lorry/lorry2/commit/88c3e1eb6f345cedda3b6923db8ef335deb045c7))
* update the configs in the docker compose example ([d05132f](https://gitlab.com/CodethinkLabs/lorry/lorry2/commit/d05132fb72fe8c9949b0202a682d283247a8b977))

### Features

* add support for mirroring git repos with lfs objects ([38af94e](https://gitlab.com/CodethinkLabs/lorry/lorry2/commit/38af94e3f139b359f22689ffffd9215e7729e348)), closes #72 (https://gitlab.com/CodethinkLabs/lorry/lorry2/issues/72)

## 1.0.1 (https://gitlab.com/CodethinkLabs/lorry/lorry2/compare/v1.0.0...v1.0.1) (2024-03-19)

### Bug Fixes

* strip leading `v` from docker image tags ([1351cd6](https://gitlab.com/CodethinkLabs/lorry/lorry2/commit/1351cd669b3c58f7853cc6eab3bca3244803563a))
* improve helm chart naming ([4a25511](https://gitlab.com/CodethinkLabs/lorry/lorry2/commit/4a25511ec5fc3ecefd767f28fe334de0605551dc))

# 1.0.0 (2024-03-18)


### Summary

* This is the first release of Lorry after the rewrite to Rust
* Some Lorry features which had little to no uptake by users have been dropped:
    * Cascading configuration files
    * Bundling or tarball-ing of mirrors
    * Support for upstreams which are not git or raw files
* If you are moving from lorry 1, please see the migration guide
  at https://gitlab.com/CodethinkLabs/lorry/lorry2#migration-from-lorry-1

# 2.0.0 (2024-10-30)

This is the first release of Lorry 2.0 which has been mostly re-written from
the ground up. More details can be found at [lorry.software](https://lorry.software).

## Notable Features

* Replaced distributed architecture with a monolithic thread based system 
* Re-wrote the work directory to use a single on-disk copy of bare git repositories
* Re-wrote raw-file support entirely and added SHA256SUM verficiation
* Added individual ref monitoring
* Added a glob based "negative" refspec field for ignoring refs
* Made the git binary configurable
* Introduced limited support for using libgit2 for cloing operations
* Added 10s of new tests
* Added a standalone Git server for testing purposes
* Cleaned up Gitlab CI considerably
* Added linter support for Lorry
* Fixed leakage of sensitive Gitlab tokens
* Added a new UI interface
* Added various Prometheus alerting implementations
* Various database schema/design improvements

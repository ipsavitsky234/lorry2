*** Settings ***
Documentation       Lorry End to End Suite

Library             Process
Library             TemplatedData
Library             OperatingSystem
Library             String
Resource            default.resource

Suite Setup         Create Gitlab Cluster
Suite Teardown      Teardown Gitlab cluster

Test Tags           lorry2


*** Variables ***
${SUITE_TMP_DIR}                    ${None}
${SUITE_KIND_CLUSTER}               ${None}
${SKIP_CLUSTER_SETUP}               ${False}
${SKIP_CLUSTER_TEARDOWN}            ${False}
${kind_config_directory}            config/kind
${kind_config_filename}             kind-full-ingress.yaml

${kubernetes_config_directory}      config/kubernetes


*** Keywords ***
Create Gitlab Cluster
    Check Prerequisites
    IF    not ${SKIP_CLUSTER_SETUP}
        Create Temp Dir
        Create Kind Cluster
        Install Ingress Controller
        Install Gitlab Helm
    END

Teardown Gitlab Cluster
    IF    not ${SKIP_CLUSTER_TEARDOWN}
        Delete Kind Cluster
        Cleanup Temp Dir
    END

Check Prerequisites
    Check Program Installed    docker
    Check Program Installed    kind
    Check Program Installed    helm
    Check Program Installed    kubectl

Create Kind Cluster
    Log    Creating Kind Cluster
    Log    Using HTTP Port ${kind_port_external_http}
    Log    Using SSH Port ${kind_port_external_ssh}

    # Create kind config file
    ${kind_template}    Get Templated Data from Path    ${kind_config_directory}/${kind_config_filename}
    Create File    ${SUITE_TMP_DIR}/${kind_config_filename}    ${kind_template}

    # Create cluster name
    ${cluster_random_name}    Generate Random String    12    [LOWER][NUMBERS]
    Set Suite Variable    $SUITE_KIND_CLUSTER    lorry2-e2e-${cluster_random_name}    children=True

    # Spin up kind cluster
    ${kind_process_result}    Run Process
    ...    kind    create    cluster
    ...    --config    ${SUITE_TMP_DIR}/${kind_config_filename}
    ...    --name    ${SUITE_KIND_CLUSTER}
    ...    --kubeconfig    ${SUITE_TMP_DIR}/kubeconfig
    Should Be Equal As Integers
    ...    ${kind_process_result.rc}
    ...    0
    ...    Failed to create kind cluster [ ${SUITE_KIND_CLUSTER} ]. Error message: ${kind_process_result.stderr}

Delete Kind Cluster
    Log    Deleting Kind Cluster

    ${kind_process_result}    Run Process
    ...    kind    delete    cluster
    ...    --name    ${SUITE_KIND_CLUSTER}
    ...    --kubeconfig    ${SUITE_TMP_DIR}/kubeconfig
    Should Be Equal As Integers
    ...    ${kind_process_result.rc}
    ...    0
    ...    Failed to delete kind cluster [ ${SUITE_KIND_CLUSTER} ]

Install Ingress Controller
    Log    Installing Ingress Controller

    ${kubectl_result}    Run Process
    ...    kubectl    apply
    ...    -f    ${kubernetes_config_directory}/kind-ingress-nginx.yml
    ...    env:KUBECONFIG=${SUITE_TMP_DIR}/kubeconfig
    Should Be Equal As Integers
    ...    ${kubectl_result.rc}
    ...    0
    ...    Failed to install ingress controller [ ${SUITE_KIND_CLUSTER} ]

    Wait Until Keyword Succeeds    5m    1s    Check Ingress Controller

Check Ingress Controller
    ${kubectl_result}    Run Process
    ...    kubectl    wait
    ...    --namespace    ingress-nginx
    ...    --for\=condition\=ready    pod
    ...    --selector\=app.kubernetes.io/component\=controller
    ...    --timeout\=5m
    ...    env:KUBECONFIG=${SUITE_TMP_DIR}/kubeconfig
    Should Be Equal As Integers
    ...    ${kubectl_result.rc}
    ...    0
    ...    Failed to wait for ingress controller [ ${SUITE_KIND_CLUSTER} ] - ${kubectl_result.stderr}

Install Gitlab Helm
    Log    Installing Gitlab Helm

    ${helm_process_result}    Run Process
    ...    helm    repo    add
    ...    gitlab    https://charts.gitlab.io
    Should Be Equal As Integers
    ...    ${helm_process_result.rc}
    ...    0
    ...    Failed to add gitlab helm repo

    ${helm_process_result}    Run Process
    ...    helm    repo    update
    Should Be Equal As Integers
    ...    ${helm_process_result.rc}
    ...    0
    ...    Failed to update helm repos

    # create config files as needed
    ${kind_template}    Get Templated Data from Path    config/helm/gitlab-values-base.yaml
    Create File    ${SUITE_TMP_DIR}/gitlab-values-base.yaml    ${kind_template}
    ${kind_template}    Get Templated Data from Path    config/helm/gitlab-values-no-ssl.yaml
    Create File    ${SUITE_TMP_DIR}/gitlab-values-no-ssl.yaml    ${kind_template}

    # create namespace
    ${kubectl_result}    Run Process
    ...    kubectl    create
    ...    namespace    gitlab
    ...    env:KUBECONFIG=${SUITE_TMP_DIR}/kubeconfig

    # Add SSH Ingress Config
    ${kubectl_result}    Run Process
    ...    kubectl    apply
    ...    -f    ${kubernetes_config_directory}/gitlab-ssh-ingress-config.yml
    ...    env:KUBECONFIG=${SUITE_TMP_DIR}/kubeconfig

    ${helm_process_result}    Run Process
    ...    helm    upgrade
    ...    --install    gitlab    gitlab/gitlab
    ...    --version    ${helm_gitlab_chart_version}
    ...    --set    global.hosts.domain\=${SUITE_KIND_CLUSTER}.localhost
    ...    --values    ${SUITE_TMP_DIR}/gitlab-values-base.yaml
    ...    --values    ${SUITE_TMP_DIR}/gitlab-values-no-ssl.yaml
    ...    --namespace    gitlab
    ...    --timeout    ${helm_timeout}
    ...    --kubeconfig    ${SUITE_TMP_DIR}/kubeconfig
    ...    --wait
    Should Be Equal As Integers
    ...    ${helm_process_result.rc}
    ...    0
    ...    Failed to install gitlab helm: ${helm_process_result.stderr}

Check Program Installed
    [Arguments]    ${program}
    ${result}    Run Process    which    ${program}
    Should Be Equal As Integers    ${result.rc}    0    Progam ${program} not found!

Create Temp Dir
    ${dir_location}    Run Process    mktemp    --directory
    Log To Console    Using ${dir_location.stdout} as temp dir
    Should Be Equal As Integers    ${dir_location.rc}    0
    Set Suite Variable    $SUITE_TMP_DIR    ${dir_location.stdout}    children=True

Cleanup Temp Dir
    Variable Should Exist    $SUITE_TMP_DIR
    Remove Directory    ${SUITE_TMP_DIR}    recursive=True

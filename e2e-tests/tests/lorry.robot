*** Settings ***
Library             RequestsLibrary
Library             TemplatedData
Library             KubeLibrary    ${SUITE_TMP_DIR}/kubeconfig
Library             Collections
Library             DateTime
Library             OperatingSystem
Library             Process
Resource            default.resource

Suite Setup         Setup Lorry Test
Suite Teardown      Teardown Lorry Test


*** Variables ***
${SUITE_TMP_DIR}                    ${None}
${SUITE_KIND_CLUSTER}               ${None}
${LORRY_GITLAB_GROUP_ID}            ${None}
${LORRY_GITLAB_GROUP_PAT}           ${None}
${LORRY_GITLAB_CONFGIT_REPO_ID}     ${None}
${LORRY_GITLAB_WORKER_SSH_ID}       ${None}


*** Test Cases ***
Is Logged In
    ${auth_test_resp}    GET On Session    gitlab    /api/v4/version

    # can only get version if logged in
    Dictionary Should Contain Key    ${auth_test_resp.json()}    version

Check Mirror Exists
    Wait Until Keyword Succeeds
    ...    5m
    ...    10s
    ...    GET On Session
    ...    gitlab
    ...    /api/v4/projects/lorry-testing%2Fmirrors%2Florry%2Florry2

    Status Should Be    200

Check Mirror Update
    # Sha is current main at time of writing
    Wait Until Keyword Succeeds
    ...    5m
    ...    10s
    ...    Get On Session
    ...    gitlab
    ...    /api/v4/projects/lorry-testing%2Fmirrors%2Florry%2Florry2/repository/commits/19685b1fe3b6cc8f40e479067be56609b13156c0

    Status Should Be    200


*** Keywords ***
Setup Lorry Test
    Login As Root
    Setup Lorry Group
    Setup Mirror Config
    Deploy Lorry Helm
    Add SSH Key

Teardown Lorry Test
    Log    Tearing Down All The Things
    Remove SSH Key
    Delete Lorry Helm
    # Dont need to teardown Mirror Config as we delete the group
    Teardown Lorry Group

Login As Root
    Log    Logging in as Root
    Create Session    gitlab    http://gitlab.${SUITE_KIND_CLUSTER}.localhost:${kind_port_external_http}

    ${root_password_secret}    List Namespaced Secret By Pattern    gitlab-gitlab-initial-root-password    gitlab

    ${root_password}    Evaluate
    ...    base64.b64decode('${root_password_secret[0].data['password']}').decode('utf-8')
    ...    modules=base64

    ${login_req_data}    Create Dictionary    grant_type=password    username=root    password=${root_password}

    ${login_resp}    POST On Session    gitlab    /oauth/token    json=${login_req_data}
    Status Should Be    200

    ${session_headers}    Create Dictionary    authorization=Bearer ${login_resp.json()['access_token']}

    Update Session    gitlab    headers=${session_headers}

Setup Lorry Group
    Log    Setting up Lorry Gitlab Group

    ${group_req_data}    Create Dictionary    name=Lorry Testing    path=lorry-testing    visibility=public

    ${group_resp}    POST On Session    gitlab    /api/v4/groups    json=${group_req_data}
    Status Should Be    201    msg=${group_resp.text}

    Set Suite Variable    ${LORRY_GITLAB_GROUP_ID}    ${group_resp.json()['id']}

    ${datetime_now}    Get Current Date
    ${datetime_later}    Add Time To Date    ${datetime_now}    7 days    result_format=%Y-%m-%d

    ${group_pat_req_scopes}    Create List    api
    ${group_pat_req_data}    Create Dictionary
    ...    name=Lorry Token
    ...    access_level=50
    ...    scopes=${group_pat_req_scopes}
    ...    expires_at=${datetime_later}

    ${group_pat_resp}    POST On Session
    ...    gitlab
    ...    /api/v4/groups/${LORRY_GITLAB_GROUP_ID}/access_tokens
    ...    json=${group_pat_req_data}
    Status Should Be    201    msg=${group_pat_resp.text}

    Set Suite Variable    ${LORRY_GITLAB_GROUP_PAT}    ${group_pat_resp.json()['token']}

Teardown Lorry Group
    Log    Deleting Lorry Gitlab Group

    ${group_resp}    DELETE On Session    gitlab    /api/v4/groups/${LORRY_GITLAB_GROUP_ID}
    Status Should Be    202    msg=${group_resp.text}

Setup Mirror Config
    Log    Setup Mirror Config

    ${project_req_data}    Create Dictionary
    ...    name=Lorry Confgit
    ...    path=confgit
    ...    namespace_id=${LORRY_GITLAB_GROUP_ID}
    ...    visibility=public

    ${project_resp}    POST On Session    gitlab    /api/v4/projects    json=${project_req_data}
    Status Should Be    201    msg=${project_resp.text}

    Set Suite Variable    ${LORRY_GITLAB_CONFGIT_REPO_ID}    ${project_resp.json()['id']}

    ${lorry_config_file_content}    Get File    config/lorry/lorry-controller.conf
    ${lorry_config_file_action}    Create Dictionary
    ...    action=create
    ...    file_path=lorry-controller.conf
    ...    content=${lorry_config_file_content}
    ${lorry_lorry_file_content}    Get File    config/lorry/lorry.lorry
    ${lorry_lorry_file_action}    Create Dictionary
    ...    action=create
    ...    file_path=lorry.lorry
    ...    content=${lorry_lorry_file_content}
    ${project_actions}    Create List    ${lorry_config_file_action}    ${lorry_lorry_file_action}
    ${project_payload}    Create Dictionary
    ...    branch=main
    ...    commit_message=Create Config
    ...    actions=${project_actions}

    ${project_commit_resp}    POST On Session
    ...    gitlab
    ...    /api/v4/projects/${LORRY_GITLAB_CONFGIT_REPO_ID}/repository/commits
    ...    json=${project_payload}
    Status Should Be    201    msg=${project_commit_resp.text}

Deploy Lorry Helm
    Log    Deploying Lorry using Helm

    ${lorry_template}    Get Templated Data from Path    config/helm/lorry-values.yaml
    Create File    ${SUITE_TMP_DIR}/lorry-values.yaml    ${lorry_template}

    ${helm_add_repo}      Run Process
    ...    helm    repo    add
    ...    lorry2_repo    https://gitlab.com/api/v4/projects/CodethinkLabs%2Florry%2Florry2/packages/helm/stable
    Should Be Equal As Integers
    ...    ${helm_add_repo.rc}
    ...    0
    ...    Failed to install lorry2 helm: ${helm_add_repo.stderr}

    ${helm_refresh_repo}    Run Process
    ...    helm    repo    update
    Should Be Equal As Integers
    ...    ${helm_refresh_repo.rc}
    ...    0
    ...    Failed to install lorry2 helm: ${helm_add_repo.stderr}

    ${chart_version}    Get Environment Variable    CHART_VERSION

    ${helm_process_result}    Run Process
    ...    helm    upgrade
    ...    --install    lorry2    lorry2_repo/lorry2
    ...    --version    ${chart_version}
    ...    --values    ${SUITE_TMP_DIR}/lorry-values.yaml
    ...    --namespace    lorry2
    ...    --create-namespace
    ...    --timeout    ${helm_timeout}
    ...    --kubeconfig    ${SUITE_TMP_DIR}/kubeconfig
    ...    --wait
    Should Be Equal As Integers
    ...    ${helm_process_result.rc}
    ...    0
    ...    Failed to install gitlab helm: ${helm_process_result.stderr}

Delete Lorry Helm
    Log    Deleting Lorry using Helm

    ${helm_process_result}    Run Process
    ...    helm    delete    lorry2
    ...    --namespace    lorry2
    ...    --kubeconfig    ${SUITE_TMP_DIR}/kubeconfig
    Should Be Equal As Integers
    ...    ${helm_process_result.rc}
    ...    0
    ...    Failed to install gitlab helm: ${helm_process_result.stderr}

Add SSH Key
    Log    Adding SSH Key

    ${ssh_private_secret}    List Namespaced Secret By Pattern    lorry2-ssh-key    lorry2

    ${ssh_private_key}    Evaluate
    ...    base64.b64decode('${ssh_private_secret[0].data['ssh-privatekey']}').decode('utf-8')
    ...    modules=base64

    ${ssh_public_key}    Run Process    ssh-keygen    -f    /dev/stdin    -y    stdin=${ssh_private_key}
    Should Be Equal As Integers
    ...    ${ssh_public_key.rc}
    ...    0
    ...    Failed to get public key - ${ssh_public_key.stderr}

    ${ssh_req_data}    Create Dictionary
    ...    title=Lorry Worker
    ...    key=${ssh_public_key.stdout}

    ${ssh_resp}    POST On Session    gitlab    /api/v4/user/keys    json=${ssh_req_data}
    Status Should Be    201    msg=${ssh_resp.text}

    Set Suite Variable    ${LORRY_GITLAB_WORKER_SSH_ID}    ${ssh_resp.json()['id']}

Remove SSH Key
    Log    Removing SSH Key

    ${ssh_resp}    DELETE On Session    gitlab    /api/v4/user/keys/${LORRY_GITLAB_WORKER_SSH_ID}
    Status Should Be    204    msg=${ssh_resp.text}

#!/usr/bin/env sh
# script to run lorry components and reload them when code changes
set -e

BINARY_NAME="lorry"
CONFIG_FILE="lorry.example.toml"

# cargo watch crashes due to not being able to access .gitlab-container
# so you have to explicitly tell it what to watch instead of using --ignore
cargo watch -w lorry -w workerlib -w utils \
	-x "run --bin $BINARY_NAME -- --config $CONFIG_FILE"

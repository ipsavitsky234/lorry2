#!/usr/bin/env sh
# Simple script to launch a local instance of Gitlab via podman
set -e

GITLAB_VERSION="17.8.0-ce.0"
GITLAB_HTTP_PORT=9999
# GITLAB_SSH_PORT=2022
GITLAB_INSECURE_PASSWORD=insecure1111

DOCKER="podman"
IMAGE_NAME="docker.io/gitlab/gitlab-ce:$GITLAB_VERSION"
GITLAB_HOME="$PWD/.gitlab-container"

mkdir -p "$GITLAB_HOME/config"
mkdir -p "$GITLAB_HOME/logs"
mkdir -p "$GITLAB_HOME/data"

make_config() {
  printf "external_url \"http://127.0.0.1:%s\";" "$GITLAB_HTTP_PORT"
  printf "gitlab_rails[\"lfs_enabled\"] = true;"
  printf "nginx[\"listen_port\"] = %s" "$GITLAB_HTTP_PORT"
}

$DOCKER run --rm -ti \
  --hostname gitlab.example.com \
  --env GITLAB_ROOT_PASSWORD=$GITLAB_INSECURE_PASSWORD \
  --env GITLAB_OMNIBUS_CONFIG="$(make_config)" \
  --publish $GITLAB_HTTP_PORT:$GITLAB_HTTP_PORT \
  --name lorry-gitlab-integration-test \
  --volume "$GITLAB_HOME/config:/etc/gitlab:Z" \
  --volume "$GITLAB_HOME/logs:/var/log/gitlab:Z" \
  --volume "$GITLAB_HOME/data:/var/opt/gitlab:Z" \
  --shm-size 256m \
  $IMAGE_NAME

#!/usr/bin/env bash
# Mirror various container images used in the build process of Lorry into
# Gitlab.
#
# DEBIAN_TARGET_IMAGE - Runtime container for Lorry CLI tool
# RUST_TARGET_IMAGE   - Build image for Lorry CLI and use in CI
# PODMAN_TARGET_IMAGE - Podman binary used for uploading Lorry container.
set -ex

# Change this value to update the version of Rust used in builds.
RUST_IMAGE="rust:1.84-bookworm"

REGISTRY="registry.gitlab.com"
NAMESPACE="codethinklabs/lorry/lorry2"

MIRRORS=(
  "docker://docker.io/library/alpine:3 docker://$REGISTRY/$NAMESPACE/alpine:3"
  "docker://docker.io/library/debian:bookworm docker://$REGISTRY/$NAMESPACE/debian:bookworm"
  "docker://quay.io/podman/stable:v5 docker://$REGISTRY/$NAMESPACE/podman:v5"
)

BASE_BUILD_IMAGE="docker.io/library/$RUST_IMAGE"
TARGET_BUILD_IMAGE="$REGISTRY/$NAMESPACE/$RUST_IMAGE"

podman login "$REGISTRY"

echo "Setting up build image"

podman build \
  --build-arg BASE_BUILD_IMAGE="$BASE_BUILD_IMAGE" \
  -t $TARGET_BUILD_IMAGE \
  -f Containerfile.build
podman push "$TARGET_BUILD_IMAGE"

echo "Mirroring other images"

for mirror in "${MIRRORS[@]}"; do
  src="$(echo "$mirror" | cut -d ' ' -f 1)"
  dst="$(echo "$mirror" | cut -d ' ' -f 2)"
  echo "Mirroring $src -> $dst"
  skopeo copy "$src" "$dst"
done
